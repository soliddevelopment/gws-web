<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

use Carbon\Carbon;

class ProductsExport implements FromCollection, WithHeadings, WithMapping, /*WithColumnWidths,*/ WithStyles
{
    private $elements;

    public function __construct($elements)
    {
        $this->elements = $elements;
    }

    public function collection()
    {
        return $this->elements;
    }

    public function map($item): array
    {
        //$createdObj = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($item->created_at));
        //$created = $createdObj->format('Y-m-d');

        //$created = \PhpOffice\PhpSpreadsheet\Shared\Date::dateTimeToExcel($item->created_at);
        $created = Carbon::parse($item->created_at)->format('Y-m-d h:i a');

        return [
            $item->id,
            $item->categoria_nombre,
            $item->img,
            $item->titulo,
            $item->titulo_corto,
            $item->bajada,
            $item->subida,
            $item->destacado,
        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'CATEGORIA',
            'IMG',
            'TITULO',
            'TITULO_CORTO',
            'BAJADA',
            'SUBIDA',
            'DESTACADO',
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 20,
            'C' => 15,
            'D' => 15,
            'E' => 20,
        ];
    }

    public function styles($sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function columnFormats(): array
    {
        return [
            //'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            //'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
        ];
    }
}
