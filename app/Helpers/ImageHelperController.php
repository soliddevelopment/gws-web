<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

class ImageHelperController extends \App\Http\Controllers\Controller
{

    /*public function getImg($img, $w=200, $h=200)
    {
        return Image::make(public_path("/storage/".$img))->resize($w, $h)->response('jpg');
    }*/
    public function getImg()
    {
        $img = Input::get('img', null);
        $w = Input::get('w', false);
        $h = Input::get('h', false);
        return Image::make(public_path($img))->resize($w, $h)->response('jpg');
    }
}
