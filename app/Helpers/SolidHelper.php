<?php


// quita todo
function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function configVal($configs, $slug='')
{
    if(strlen($slug)>0 && count($configs)>0){
        foreach($configs as $item){
            if($item->slug == $slug){
                return $item->getVal();
            }
        }
    }
}

// Funcion para ponerle background image facilmente a divs
function bgImgStyle($img, $w=0, $h=0){

    if(strlen($img)==0){
        $img = 'a/img/no-disponible.png';
    } else {
        $img = 'storage/'.$img;
    }

    if($w > 0 && $h > 0){
        $hayMedidas = true;
    } else {
        $hayMedidas = false;
    }

    if($hayMedidas){
        $str = '&w='.$w.'&h='.$h;
        $url = url('img/thumb/?img='.$img.$str);
    } else {
        $url = url($img);
    }

    $styletxt = "background-image:url('".$url."')";
    return $styletxt;
    // lo demas esta seteado en el controller ImageController
}

// Funcion para scale images
function thumb($img, $w=0, $h=0){
    if(strlen($img)==0){
        $img = 'a/img/no-disponible.png';
    } else {
        $img = 'storage/'.$img;
    }

    if($w > 0 && $h > 0){
        $hayMedidas = true;
    } else {
        $hayMedidas = false;
    }

    if($hayMedidas){
        $pathmedidas = '&w='.$w.'&h='.$h;
    } else {
        $pathmedidas = '';
    }

    $path = 'img/thumb/?img='.$img;

    return url($path.$pathmedidas);
    // lo demas esta seteado en el controller ImageController
}
//<img src="{{ scale($item->img, 100, 100) }}" alt="">

function pr($array) {
	if($array) {
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
}

function pr2($array) { // muestra un array en un cuadrito con estilo
	if($array) {
		echo "<div style='overflow-y: scroll; opacity: 0.36;position:fixed;bottom:0;right:0;width:500px;height:344px;z-index:9999; padding:0 10px; color:#111;background-color:#fff;border: 10px solid red;'>";
		echo "<pre style='font-weight:bold;font-size: 14px;line-height:1em;'>";
		print_r($array);
		echo "</pre>";
		echo "</div>";
	}
}

function nombre_mes($num, $lang=0) {
	if ($num == 1) {
		if($lang==0) $mes = "enero"; else $mes = "january";
	} elseif ($num == 2) {
		if($lang==0) $mes = "febrero"; else $mes = "february";
	} elseif ($num == 3) {
		if($lang==0) $mes = "marzo"; else $mes = "march";
	} elseif ($num == 4) {
		if($lang==0) $mes = "abril"; else $mes = "april";
	} elseif ($num == 5) {
		if($lang==0) $mes = "mayo"; else $mes = "may";
	} elseif ($num == 6) {
		if($lang==0) $mes = "junio"; else $mes = "june";
	} elseif ($num == 7) {
		if($lang==0) $mes = "julio"; else $mes = "july";
	} elseif ($num == 8) {
		if($lang==0) $mes = "agosto"; else $mes = "august";
	} elseif ($num == 9) {
		if($lang==0) $mes = "septiembre"; else $mes = "september";
	} elseif ($num == 10) {
		if($lang==0) $mes = "octubre"; else $mes = "october";
	} elseif ($num == 11) {
		if($lang==0) $mes = "noviembre"; else $mes = "november";
	} elseif ($num == 12) {
		if($lang==0) $mes = "diciembre"; else $mes = "december";
	} 
	
	if ($mes) return $mes;
	else return false;
	
}

function shuffle_assoc($array){
	$ary_keys = array_keys($array);
	$ary_values = array_values($array);
	shuffle($ary_values);
	foreach($ary_keys as $key => $value) {
		if (is_array($ary_values[$key]) AND $ary_values[$key] != NULL) {
			$ary_values[$key] = rec_assoc_shuffle($ary_values[$key]);
		}
		$new[$value] = $ary_values[$key];
	}
	return $new;
}

function file_extension($filename){
	return end(explode(".", $filename));
}


function convertirTildes($entra){
	$traduce=array( '�' => '&aacute;' , '�' => '&eacute;' , '�' => '&iacute;' , '�' => '&oacute;' , '�' => '&uacute;' , '�' => '&ntilde');
	$sale=strtr( $entra , $traduce );
	return $sale;
}

function getRealIpAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])) { // check ip from share internet
    	$ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { // to check ip is pass from proxy
    	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function remove_accents($string) {
    if ( !preg_match('/[\x80-\xff]/', $string) )
        return $string;

    $chars = array(
    // Decompositions for Latin-1 Supplement
    chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
    chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
    chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
    chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
    chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
    chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
    chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
    chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
    chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
    chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
    chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
    chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
    chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
    chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
    chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
    chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
    chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
    chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
    chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
    chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
    chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
    chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
    chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
    chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
    chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
    chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
    chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
    chr(195).chr(191) => 'y',
    // Decompositions for Latin Extended-A
    chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
    chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
    chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
    chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
    chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
    chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
    chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
    chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
    chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
    chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
    chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
    chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
    chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
    chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
    chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
    chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
    chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
    chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
    chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
    chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
    chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
    chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
    chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
    chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
    chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
    chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
    chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
    chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
    chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
    chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
    chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
    chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
    chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
    chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
    chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
    chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
    chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
    chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
    chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
    chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
    chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
    chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
    chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
    chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
    chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
    chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
    chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
    chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
    chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
    chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
    chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
    chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
    chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
    chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
    chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
    chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
    chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
    chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
    chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
    chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
    chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
    chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
    chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
    chr(197).chr(190) => 'z', chr(197).chr(191) => 's'
    );

    $string = strtr($string, $chars);

    return $string;
}

function cleanURL($url){
    if(isset($url) && strlen($url)>0){
        $parsed = parse_url($url);

        if(isset($parsed['host']) && strlen($parsed['host'])>0){
            if($parsed['host'] == "www.youtube.com" || $parsed['host'] == "youtube.com"){
                $tipo = "youtube";

            } else if($parsed['host'] == "youtu.be"){
                $tipo = "youtube";
                $yt_id = explode("/",$parsed['path']);
                $yt_id = $yt_id['1'];
                $url = "https://www.youtube.com/watch?v=".$yt_id;

            } else if($parsed['host'] == "vimeo.com"){
                $tipo = "vimeo";
                $vim_id = explode("/",$parsed['path']);
                $vim_id = end($vim_id);
                $url = "https://vimeo.com/".$vim_id;
            }

            if(isset($tipo) && isset($url)){
        		$obj = array("url"=>$url, "tipo"=>$tipo);
        		return $obj;
            } else {
            	return false;
            }

        } else {
            return false;
        }
    }
}

// lo convierte a iframe (nuevo)
function youtubeConvert($url, $width="750", $height="440"){
	return preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"".$width."\" height=\"".$height."\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$url);
}

function vimeoConvert($url, $width="750", $height="440"){
	preg_match("/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/", $url, $matches);
	$id = $matches[2];
	$html = '<iframe src="http://player.vimeo.com/video/'.$id.'?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
	return $html;
}

function valid_url($url) {
    if(filter_var($url, FILTER_VALIDATE_URL) === FALSE){
        return false;
    } else {
        return true;
    }
}

function validate_email($email){
    return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email);
}

function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

function escape( $value ) {
    
	$magic_quotes_active = get_magic_quotes_gpc(); 
    //$this->magic_quotes_active = get_magic_quotes_gpc();
    //$this->real_escape_string_exists = function_exists("mysql_real_escape_string");
    
    if(function_exists("mysql_real_escape_string")) {
        // if condition true, undo any magic quote effects so mysql_real_escape_string can do the work
        if($magic_quotes_active == TRUE) { $value = stripslashes($value); }
        $value = mysql_real_escape_string($value);
    } else {
        // if magic quotes aren't already on, then add slashes manually
        if ($magic_quotes_active == FALSE) {
            $value = addslashes($value);
        }
    }
    return $value;
}

function fecha($fecha, $lang=0){
	
	
	$fecha_array = explode("-",$fecha);
	$ano = $fecha_array['0'];
	$mes = nombre_mes($fecha_array['1'], $lang);
	$dia = $fecha_array['2'];
	if($dia[0] == "0") $dia = substr($dia, 1);
	
	if($lang==0){
		$fecha = $dia." de ".$mes.", ".$ano;
	} else {
		$fecha = $mes." ".$dia.", ".$ano;
	}
	
	
	return $fecha;
}

function resumen($str, $limit){
	if(strlen($str)<=$limit){
		return $str;
	} else {
		$resumen = substr($str, 0, $limit)."...";
		return $resumen;
	}
}

?>