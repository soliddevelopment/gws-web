<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Categoria;
use App\Models\Grupo;
use Image;
use Session;

class CategoriasController extends BaseController
{
    
    public function index($grupo_id)
    {
        $grupo = Grupo::find($grupo_id);
        $categorias = $grupo->getCategorias();
        // $categorias = Categoria::all()->sortByDesc('created_at');
        return view('admin.categorias.index')->with('grupo', $grupo)->with('categorias', $categorias);
    }


    public function create($grupo)
    {
        $grupo = Grupo::find($grupo);
        return view('admin.categorias.create')->with('grupo', $grupo);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'nombre' => 'required|string|max:255',
            'img' => 'max:3000|mimes:jpeg,jpg,bmp,png',
            'grupo_id' => 'required|integer|min:1',
        ]);

        $categoria = new Categoria;
        $categoria->grupo_id = $request->grupo_id;
        $categoria->nombre = $request->nombre;
        $categoria->titulo = $request->titulo;

        // img principal
        if($request->hasFile('img')){
            $file = $request->file('img');
            $imgpath = 'cat-'.time().'-'.$file->getClientOriginalName();
            // $img = Image::make($file)->resize(320, 240)->save($imgpath);
            $img = Image::make($file)->save('storage/'.$imgpath);
            $categoria->img = $imgpath;
        }
        
        // Guardar cat
        $categoria->save();

        Session::flash('success', 'La categoría fue guardado con éxito.');
        return redirect()->route('admin.categorias.index', ['grupo' => $categoria->grupo_id]);
        dd($request->all());
    }
    

    public function edit($id)
    {
        $categoria = Categoria::find($id);
        $grupo = Grupo::find($categoria->grupo_id);
        return view('admin.categorias.edit')->with('categoria', $categoria)->with('grupo', $grupo);
    }

    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'nombre' => 'required|string|max:255',
            'grupo_id' => 'required|integer|min:1',
            'img' => 'max:3000|mimes:jpeg,jpg,bmp,png',
        ]);

        // Guardo info del usuario
        $categoria = Categoria::find($id);
        $categoria->titulo = $request->input('titulo');
        $categoria->nombre = $request->input('nombre');
        
        // img principal
        if($request->hasFile('img')){
            if(Storage::disk('public')->has($categoria->img)){
                Storage::disk('public')->delete($categoria->img);
            }
            $file = $request->file('img');
            $imgpath = 'cat-'.time().'-'.$file->getClientOriginalName();
            // $img = Image::make($file)->resize(320, 240)->save($imgpath);
            $img = Image::make($file)->save('storage/'.$imgpath);
            $categoria->img = $imgpath;
        }

        $categoria->save();

        Session::flash('success', 'La categoría se modificó exitosamente');
        return redirect()->route('admin.categorias.edit', $categoria->id);
    }

    public function destroy($id)
    {
        $categoria = Categoria::find($id);
        $categoria->st2 = 0;
        $categoria->save(); // soft delete

        Session::flash('success', 'La categoría fue borrada!');
        return redirect()->route('admin.categorias.index', ['grupo'=>$categoria->grupo_id]);
    }

    public function st(Request $request, $id)
    {
        $categoria = Categoria::find($id);
        $categoria->st = $request->input('st');
        $categoria->save();

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
}
