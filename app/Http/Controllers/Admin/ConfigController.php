<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use View;
use Session;
use Image;
use App\Models\Config;

class ConfigController extends BaseController
{
    public function index(Request $request){
        $configuraciones = array();
        $usr = $request->user();
        if($usr->hasRole('Global')){
            $configuraciones = Config::all();
        } else {
            $configuraciones = Config::all()->where('only_global', '=', 0);
        }

        $view = View::make('admin.config.index', [
            'configuraciones' => $configuraciones
        ]);
        return $view;
    }

    public function edit(Request $request, $id=null)
    {
        if($id!=null){
            $config = Config::find($id);
            $title = $config->titulo;
        } else {
            $config = null;
            $title = 'Nuevo item';
        }  

        $view = View::make('admin.config.edit', [
            'title' => $title,
            'config' => $config,
        ]);
        return $view;
    }


    public function store(Request $request, $id=null)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'slug' => [
                'required', 'string', 'max:255',
                Rule::unique('configs')->ignore($id),
            ],
            'tipo' => 'required|string|max:255'
        ]);

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0) {
            $edit = true;
            $config = Config::find($id);
        } else {
            $edit = false;
            $config = new Config;
        }

        $usr = $request->user();

        if($usr->hasRole('Global')){
            $config->only_global = $request->input('only_global');
            $config->tipo = $request->input('tipo');
            $config->slug = $request->input('slug');
            $config->titulo = $request->input('titulo');
            $config->descripcion = $request->input('descripcion');
        }

        $proceed = false;
        if($edit && $config->only_global == 0){
            $proceed = true;
        }

        if($usr->hasRole('Global') || ( !$usr->hasRole('Global') && $edit && $config->only_global == 0 )){
            $config->valor_str = $request->input('valor_str');
            $config->valor_int = $request->input('valor_int');
            $config->valor_bool = $request->input('valor_bool');
            // $config->valor_img = $request->input('valor_img');

            if($request->hasFile('valor_img')){
                $file = $request->file('valor_img');
                $oldfilename = $file->getClientOriginalName();
                $newfilename = str_replace(' ', '', $oldfilename);
                $imgpath = time().'-'.$newfilename;
                // $img = Image::make($file)->resizeCanvas(400, 252)->save('storage/'.$imgpath);
                $img = Image::make($file)->save('storage/'.$imgpath);
                $config->valor_img = $imgpath;
            }
        }

        $config->save();

        Session::flash('success', 'La configuración fue guardada con éxito.');
        return redirect()->route('admin.config.index');
        dd($request->all());
    }
    
    public function destroy($id)
    {
        $config = Config::find($id);
        $config->delete();
        Session::flash('success', 'La configuración fue borrada!');
        return redirect()->route('admin.config.index');
    }
    
    public function st(Request $request, $id)
    {
        $config = Config::find($id);
        $config->valor_bool = $request->input('val');
        $config->save();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
}
