<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use Session;

use App\Models\Contacto;

class ContactosController extends BaseController
{
    public function index(Request $request){
        $contactos = Contacto::all();
        $view = View::make('admin.contactos.index', [
            'contactos' => $contactos
        ]);
        return $view;
    }

    public function detalle(Request $request, $id=null)
    {
        $contacto = Contacto::find($id);
        $view = View::make('admin.contactos.detalle', [
            'contacto' => $contacto
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }

    public function destroy($id)
    {
        $contacto = Contacto::find($id);
        if($contacto != null) $contacto->delete();
        Session::flash('success', 'El contacto fue borrado!');
        return redirect()->route('admin.contactos.index');
    }
}
