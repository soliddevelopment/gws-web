<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Session;
use Storage;
use View;
use App\Models\Grupo;
use App\Models\Post;
use App\Models\TipoGrupo;

class GruposController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupos = Grupo::where('st', 1)->where('st2', 1)->orderBy('created_at', 'desc')->get();

        $view = \View::make('admin.grupos.index', [
            'grupos' => $grupos,
        ]);
        return $view;
    }

    /*public function create()
    {
        $tipos = TipoGrupo::all()->where('st', 1);
        // pr($tipos);
        return view('admin.grupos.create')->with('tipos', $tipos);
    }*/

    public function edit($id=null)
    {
        if($id!=null){
            $grupo = Grupo::find($id);
            $title = 'Edigar grupo: '.$grupo->titulo;
        } else {
            $grupo = null;
            $title = 'Nuevo grupo';
        }
        $tipos = TipoGrupo::all()->where('st', 1);
        $view = View::make('admin.grupos.edit', [
            'title' => $title,
            'grupo' => $grupo,
            'tipos' => $tipos,
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        // validate data
        $this->validate($request, [
            'nombre' => 'required|string|max:255',
            'titulo' => 'required|string|max:255',
            'tipo_id' => 'required|string',
        ]);

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0) {
            $edit = true;
            $grupo = Grupo::find($id);
            $route = 'admin.grupos.index';
        } else {
            $edit = false;
            $grupo = new Grupo;
            $grupo->st = 1;
            $route = 'admin.grupos.index';
        }

        // Guardo info
        $grupo->nombre = $request->input('nombre');
        $grupo->titulo = $request->input('titulo');
        $grupo->tipo_id = $request->input('tipo_id');
        $grupo->save();

        Session::flash('success', 'El grupo fue guardado con éxito');
        return redirect()->route($route, $grupo->id);
    }
    
    public function destroy($id)
    {
        $grupo = Grupo::find($id);
        $grupo->st2 = 0;
        $grupo->save();

        Session::flash('success', 'El grupo fue borrado');
        return redirect()->route('admin.grupos.index');
    }

    public function st(Request $request, $id)
    {
        $grupo = Grupo::find($id);
        $grupo->st = $request->input('st');
        $grupo->save();

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

    /*
    public function destroy($id)
    {
        $grupo = Grupo::find($id);
        $extras = $grupo->extras();

        // borro los post_extras
        foreach($extras as $item){
            $post_extras = DB::table('post_extras')->where('extra_id', $item->id)->get();
            foreach($post_extras as $item2){
                $item2->delete();
            }
        }

        // Borro las imgs de los posts
        $posts = $grupo->getPosts();
        foreach($posts as $post){
            if(strlen($post->img)>0){
                if(Storage::disk('public')->has($post->img)){
                    Storage::disk('public')->delete($post->img);
                }
            }
            // Borrar las imagenes relacionadas o todo lo relacionado a posts como tags o eso
        }

        // borro extras
        $extras->delete();

        // borro posts
        $grupo->posts()->delete();

        // borro grupo
        $grupo->delete();

        Session::flash('success', 'El grupo y toda su información fue borrada');
        return redirect()->route('admin.grupos.index');
    }*/
}
