<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Image;
use Storage;
use File;
use App\Models\Post;
use App\Models\Producto;
use App\Models\Img;

class ImgsController extends BaseController
{
    public function postIndex($postId)
    {
        $post = Post::find($postId);
        $grupo = $post->grupo;
        $imgs = $post->imgs->sortBy('orden');
        $imgUploadAction = action('Admin\ImgsController@postUpload', ['postId'=>$post->id]);
        $breadcrumbAction = action('Admin\PostsController@index', ['grupo'=>$grupo->id]);

        return view('admin.imgs.index')
            ->with('imgs', $imgs)
            ->with('grupo', $grupo)
            ->with('breadcrumbAction', $breadcrumbAction)
            ->with('imgUploadAction', $imgUploadAction);
    }
    public function productoIndex($productoId)
    {
        $producto = Producto::find($productoId);
        $grupo = $producto->grupo;
        $imgs = $producto->imgs->sortBy('orden');
        $imgUploadAction = action('Admin\ImgsController@productoUpload', ['productoId'=>$producto->id]);
        $breadcrumbAction = action('Admin\ProductosController@index', ['grupo'=>$grupo->id]);

        return view('admin.imgs.index')
            ->with('imgs', $imgs)
            ->with('grupo', $grupo)
            ->with('breadcrumbAction', $breadcrumbAction)
            ->with('imgUploadAction', $imgUploadAction);
    }

    public function postCreate($postId)
    {
        $post = Post::find($postId);
        $grupo = $post->grupo;
        return view('admin.imgs.post_create')
            ->with('grupo', $grupo)
            ->with('post', $post);
    }
    
    public function productoCreate($productoId)
    {
        $producto = Producto::find($productoId);
        $grupo = $producto->grupo;
        return view('admin.imgs.producto_create')
            ->with('grupo', $grupo)
            ->with('producto', $producto);
    }
    
    public function postUpload(Request $request, $postId){
        $post = Post::find($postId);
        $img = new Img;
        $img->post_id = $post->id;
        $img->producto_id = null;
        return $this->upload($request, $img, 'codisa-gallery-');
    }

    public function productoUpload(Request $request, $productoId){

        $this->validate($request, [
            'file' => 'required|max:3000|mimes:jpeg,jpg,bmp,png',
        ]);

        $producto = Producto::find($productoId);
        $img = new Img;
        $img->producto_id = $producto->id;
        $img->post_id = null;


        return $this->upload($request, $img, 'productos/imgs/');
    }
    
    public function upload($request, $img, $prefix){
        // validar archivos

        $img->titulo = '';
        $img->descripcion = '';
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filepath = $prefix.time().'-'.$file->getClientOriginalName();
            $file = Image::make($file)->save('storage/'.$filepath);
            $img->img = $filepath;
        }
        $img->save();
        return response()->json([
            'success' => true,
            'error' => null,
            'img_obj' => array(
                'id' => $img->id,
                'imgpath_small' => thumb($img->img, 84, 84),
                'imgpath_big' => thumb($img->img, 1200, 900)
            )
        ]);
    }
    
    public function destroy(Request $request){
        $img = Img::find($request->input('id'));
        if(strlen($img->img)>0){
            if(Storage::disk('public')->has($img->img)){
                Storage::disk('public')->delete($img->img);
            }
        }
        $img->delete();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
    
    public function sort(Request $request){
        $array = $request->input('img');
        $cambiados = 0;
        for($x=0; $x<count($array); $x++){
            $idActual = $array[$x];
            $img = Img::find($idActual);
            $img->orden = $x+1;
            $img->save();
        }
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }










    
    /*public function store(Request $request)
    {
        $this->validate($request, [
            'llave' => 'required|string|max:255',
            'titulo' => 'required|string|max:255',
            'tipo' => 'required|integer|min:0',
        ]);

        $grupo = Grupo::find($request->grupo_id);

        $extra = new Extra;
        $extra->llave = $request->llave;
        $extra->titulo = $request->titulo;
        $extra->tipo = $request->tipo;
        $extra->grupo_id = $grupo->id;
        $extra->save();

        // busco posts del grupo que ya estaban creados para generarles campos en la tabla post_extras, asi no tengo pedos con consultas
        $posts = Post::all()->where('grupo_id', $grupo->id);
        foreach($posts as $item){
            $extras = $grupo->getExtrasNuevasPosts();
            foreach($extras as $ex){
                $found = PostExtra::all()->where('extra_id', $ex->id)->where('post_id', $item->id)->first();
                if($found){
                    // ya existe, no hacer nada
                } else {
                    $rel = new PostExtra;
                    $rel->post_id = $item->id;
                    $rel->extra_id = $ex->id;
                    $rel->value = '';
                    $rel->save();
                }
            }
        }

        // busco posts del grupo que ya estaban creados para generarles campos en la tabla post_extras, asi no tengo pedos con consultas
        $productos = Producto::all()->where('grupo_id', $grupo->id);
        foreach($productos as $item){
            $extras = $grupo->getExtrasNuevasProductos();
            foreach($extras as $ex){
                $found = ProductoExtra::all()->where('extra_id', $ex->id)->where('producto_id', $item->id)->first();
                if($found){
                    // ya existe, no hacer nada
                } else {
                    $rel = new ProductoExtra;
                    $rel->producto_id = $item->id;
                    $rel->extra_id = $ex->id;
                    $rel->value = '';
                    $rel->save();
                }
            }
        }

        Session::flash('success', 'El extra fue guardado con éxito.');
        // return redirect()->action('ExtrasController@index', ['grupo' => $extra->grupo_id]);
        return redirect()->route('admin.extras.index', ['grupo' => $grupo]);
        dd($request->all());
    }

    public function edit($id)
    {
        $extra = Extra::find($id);
        $grupo = Grupo::find($extra->grupo_id);
        return view('admin.extras.edit')->with('extra', $extra)->with('grupo', $grupo);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'llave' => 'required|string|max:255',
            'titulo' => 'required|string|max:255',
            'tipo' => 'required|integer|min:0',
        ]);

        $extra = Extra::find($id);
        $extra->llave = $request->input('llave');
        $extra->titulo = $request->input('titulo');
        $extra->tipo = $request->input('tipo');
        // $extra->grupo_id = $request->grupo_id;

        $extra->save();

        Session::flash('success', 'El extra se modificó exitosamente');
        return redirect()->route('admin.extras.edit', $extra->id);
    }

    public function destroy($id)
    {
        $extra = Extra::find($id);
        $extra->posts()->detach();

        $post_extras = PostExtra::all()->where('extra_id', $id);
        foreach($post_extras as $item){
            $item->delete();
        }
        
        $producto_extras = ProductoExtra::all()->where('extra_id', $id);
        foreach($producto_extras as $item){
            $item->delete();
        }

        $extra->delete();
        Session::flash('success', 'El extra fue borrado!');
        return redirect()->route('admin.extras.index', ['grupo'=>$extra->grupo_id]);
    }*/
}
