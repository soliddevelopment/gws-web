<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use View;
use Session;

use App\Models\Proyecto;
use App\Models\Link;

class LinksController extends BaseController
{
    public function index(Request $request, $id){
        $proyecto = Proyecto::find($id);
        $links = Link::where('proyecto_id', $id)->orderBy('orden')->get();
        $view = View::make('admin.proyectos.links.index', [
            'links' => $links,
            'proyecto' => $proyecto,
        ]);
        return $view;
    }

    public function create($proyectoId=null)
    {
        $link = null;
        $title = 'Nuevo item';
        $proyecto = Proyecto::find($proyectoId);

        $view = View::make('admin.proyectos.links.edit', [
            'title' => $title,
            'link' => $link,
            'proyecto' => $proyecto,
        ]);
        return $view;
    }

    public function edit($linkId=null)
    {
        $link = Link::find($linkId);
        $title = $link->titulo;
        $proyecto = Proyecto::find($link->proyecto_id);

        $view = View::make('admin.proyectos.links.edit', [
            'title' => $title,
            'link' => $link,
            'proyecto' => $proyecto,
        ]);
        return $view;
    }

    public function store(Request $request, $linkId=null)
    {
        $array_validations = array(
            'proyecto_id' => 'required|integer',
            'tipo' => 'required|string|max:255',
            'titulo' => 'required|string|max:255',
            'url' => 'required|string|max:255',
        );
        $this->validate($request, $array_validations);
        
        if(isset($linkId) && $linkId!=null && is_numeric($linkId) && $linkId > 0){
            $edit = true;
            $link = Link::find($linkId);
        } else {
            $edit = false;
            $link = new Link;
            $link->st2 = 1;
        }

        $link->proyecto_id = $request->input('proyecto_id');
        $link->titulo = $request->input('titulo');
        $link->tipo = $request->input('tipo');
        $link->url = $request->input('url');
        $link->descripcion = $request->input('descripcion');

        $link->save();

        
        $proyecto = Proyecto::find($link->proyecto_id);

        Session::flash('success', 'El link fue guardado con éxito.');
        return redirect()->route('admin.proyectos.links.index', ['id'=>$proyecto->id]);
    }
    
    public function destroy($id)
    {
        $link = Link::find($id);
        $proyecto_id = $link->proyecto_id;
        $link->delete();
        Session::flash('success', 'El link fue borrado!');
        return redirect()->route('admin.proyectos.links.index', ['id'=>$proyecto_id]);
    }

    public function sort(Request $request)
    {
        $array = $request->input('elem');
        // dd($request->input('elem'));
        
        for($x=0; $x<count($array); $x++){
            $actual = $array[$x];
            $link = Link::find($actual);
            $link->orden = $x+1;
            $link->update();
        }
        
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

}
