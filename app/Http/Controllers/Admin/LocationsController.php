<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Http\File;
// use Illuminate\Http\Support\Facades\Storage;
use Session;
use Storage;
use DB;
use Image;
// use File;
use View;

use App\Models\Custom\Location;

class LocationsController extends BaseController
{
    public function index(Request $request){
        $locations = Location::all();
        $view = View::make('admin.locations.index', [
            'locations' => $locations
        ]);
        return $view;
    }

    public function edit($id=null)
    {
        if($id!=null){
            $location = Location::find($id);
            $title = $location->nombre;
        } else {
            $location = null;
            $title = 'Nuevo item';
        }  

        $view = View::make('admin.locations.edit', [
            'title' => $title,
            'location' => $location,
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        $array_validations = array(
            // 'nombre' => 'required|string|max:255',
            'latitude' => 'required|string|max:255',
            'longitude' => 'required|string|max:255',
        );
        $this->validate($request, $array_validations);
        
        if(isset($id) && $id!=null && is_numeric($id) && $id > 0){
            $edit = true;
            $location = Location::find($id);
        } else {
            $edit = false;
            $location = new Location;
            $location->ingresado_bulk = 0;
        }

        $location->nombre = $request->input('nombre');
        $location->descripcion = $request->input('descripcion');
        $location->latitude = $request->input('latitude');
        $location->longitude = $request->input('longitude');

        $location->save();

        Session::flash('success', 'El ítem fue guardado con éxito.');

        if($edit) return redirect()->route('admin.locations.index');
        else return redirect()->route('admin.locations.create');
    }
    
    public function destroy($id)
    {
        $location = Location::find($id);
        // $location->st2 = 0;
        // $location->save(); // soft delete
        $location->delete();
        Session::flash('success', 'El ítem fue borrado!');
        return redirect()->route('admin.locations.index');
    }

    public function scriptsIndex(){
        
    }

    public function scriptsEdit(){
        $view = View::make('admin.locations.script', [
        ]);
        return $view;
    }

    public function scriptsStore(Request $request){
        $array_validations = array(
            // 'nombre' => 'required|string|max:255',
            // 'latitude' => 'required|string|max:255',
            // 'longitude' => 'required|string|max:255',
            // 'json' => 'required|max:8000|mimes:json,geojson',
        );
        $this->validate($request, $array_validations);
        
        $locations = Location::all();

        if($request->input('borrar_anteriores')!== null){
            $borrar_anteriores = $request->input('borrar_anteriores');
            if($borrar_anteriores === 0){
                // borrar todas
                foreach($locations as $loc){
                    array_push($id_array, $loc->id);
                }
                Location::destroy($id_array);
            } else if($borrar_anteriores === 1){
                // borrar solo las bulk
                foreach($locations as $loc){
                    if($loc->ingresado_bulk === 1) array_push($id_array, $loc->id);
                }
                Location::destroy($id_array);
            } else {
                // no borrar nada
            }
        }
        
        if($request->hasFile('json')){
            $file = $request->file('json');
            $extension = $file->extension();
            // $basicname = 'script-json.'.$extension;
            $basicname = 'script-json.json';
            $newname = time().'-'.$basicname;

            $path = Storage::putFileAs('', $file, $newname);
            // Storage::putFileAs('photos', new File('/path/to/photo'), 'photo.jpg');

            $jsonfile = storage_path().'/app/'.$path;

            $json = json_decode(file_get_contents($jsonfile), true); 

            if($json !== null){
                $cantidad = 0;
                foreach($json['features'] as $feat){
                    $nombre = $feat['properties']['Name'];
                    $lat = $feat['geometry']['coordinates'][1];
                    $lng = $feat['geometry']['coordinates'][0];

                    $location = new Location;
                    $location->ingresado_bulk = 1;
                    $location->nombre = $nombre;
                    $location->descripcion = '';
                    $location->latitude = $lat;
                    $location->longitude = $lng;
                    $location->save();

                    $cantidad++;
                }
                Session::flash('success', 'El json fue procesado con éxito y se guardaron '.$cantidad.' locations.');
                return redirect()->route('admin.locations.index');
            } else {
                
                Session::flash('error', 'Ocurrió un error');
                return redirect()->route('admin.locations.script');
            }

        }

    }


}
