<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Session;
use Storage;
use View;
use App\Models\GrupoMapping;
use App\Models\Grupo;
use App\Models\Post;
use App\Models\TipoGrupo;

class MappingsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($grupo_id)
    {
        //$grupos = Grupo::where('st', 1)->where('st2', 1)->sortByDesc('created_at')->get();
        $mappings = GrupoMapping::all()->where('grupo_id', $grupo_id)->sortByDesc('created_at');
        $grupo = Grupo::find($grupo_id);
        $view = \View::make('admin.grupos.index', [
            'mappings' => $mappings,
            'grupo' => $grupo,
            'grupos' => [],
        ]);
        return $view;
    }

    public function edit(Request $request, $grupo_id, $id=null)
    {
        if($id!=null){
            // $mappings = GrupoMapping::where('grupo_id', $grupo_id);
            $title = 'Editar mapping: '.$mapping->campo;
        } else {
            $mappings = array();
            $title = 'Nuevo mapping';
        }

        $grupo = Grupo::find($grupo_id);
        
        $columns = Schema::getColumnListing('posts'); // get the column names for the table
        $columns2 = array();
        
        $excepciones = [ 'id', 'st2', 'created_at', 'updated_at', 'grupo_id' ];
        foreach($columns as $item){
            if(!in_array($item, $excepciones)){
                $mapping = GrupoMapping::where('grupo_id', $grupo->id)->where('campo', $item)->first();
                
                if($mapping == null){
                    $obj = array(
                        'campo' => $item,
                        'titulo' => '',
                        'placeholder'=> '',
                        'helptext'=> '',
                        'requerido'=> 0,
                        'max_char_length'=> null,
                        'existe'=> false
                    );
                    array_push($columns2, $obj);
                } else {
                    $obj = array(
                        'campo' => $item,
                        'titulo' => $mapping->titulo,
                        'placeholder'=> $mapping->placeholder,
                        'helptext'=> $mapping->helptext,
                        'requerido'=> $mapping->requerido,
                        'max_char_length'=> $mapping->max_char_length,
                        'existe'=> true
                    );
                    array_push($columns2, $obj);
                }
            }
        }

        // dd($columns2);
        $view = View::make('admin.grupos.mappings.edit', [
            'title' => $title,
            'grupo' => $grupo,
            'columns' => $columns2,
        ]);
        return $view;
    }

    
    public function store(Request $request, $grupo_id, $id=null)
    {
        // validate data
        $this->validate($request, [
            // 'grupo_id' => 'required|integer',
            // 'campo' => 'required|string|max:255',
        ]);
        
        $columns = Schema::getColumnListing('posts');
        $grupo = Grupo::find($grupo_id);
        $mappings = GrupoMapping::where('grupo_id', $grupo->id);
        $mappings->delete();
        $mapping_save_count = 0;

        $excepciones = [ 'id', 'st2', 'created_at', 'updated_at', 'grupo_id' ];
        foreach($columns as $item){
            if(!in_array($item, $excepciones)){
                $name = 'map_'.$item;
                if($request->input($name) != null || $request->input($name) == true){
                    $newcol = new GrupoMapping;
                    $newcol->grupo_id = $grupo->id;
                    $newcol->campo = $item;
                    $newcol->placeholder = $request->input('map_'.$item.'_placeholder');
                    $newcol->helptext = $request->input('map_'.$item.'_helptext');
                    $newcol->requerido = $request->input('map_'.$item.'_requerido');
                    $newcol->max_char_length = $request->input('map_'.$item.'_max_char_length');
                    
                    $titulo = $request->input('map_'.$item.'_titulo');
                    if($titulo == null || strlen($titulo)==0) $titulo = ucfirst($item);
                    $newcol->titulo = $titulo;
                    
                    $newcol->save();
                    $mapping_save_count++;
                }
            }
        }

        /*$mapping->grupo_id = $grupo->id;
        $mapping->campo = $request->input('campo');
        $mapping->helptext = $request->input('helptext');
        $mapping->titulo = $request->input('titulo');
        $mapping->placeholder = $request->input('placeholder');
        $mapping->save();*/

        Session::flash('success', $mapping_save_count.' mappings fueron guardados con éxito');
        // return redirect()->route('admin.grupos.index', $grupo->id);
        return redirect()->route('admin.grupos.index');
    }
    
    public function destroy($id)
    {   
        $mapping = GrupoMapping::find($id);
        if($mapping != null) {
            $grupo = Grupo::find($mapping->grupo_id);
            $mapping->delete();
            $grupo_id = $grupo->id;
        } else {
            $grupo_id = null;
        }
        Session::flash('success', 'Este mapping fue borrado');
        return redirect()->route('admin.grupos.index', $grupo_id);
    }
}
