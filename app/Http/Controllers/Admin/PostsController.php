<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
// use Illuminate\Http\Support\Facades\Storage;
use Session;
use Storage;
use DB;
use Image;
use File;
use View;

use App\Models\Post;
use App\Models\Grupo;
use App\Models\GrupoMapping;
use App\Models\Categoria;
use App\Models\Subcategoria;

use App\Rules\NoSpaces;

class PostsController extends BaseController
{
    public function index(Request $request, $grupo_id){
        $items = $request->input('items'); if($items == null) $items = 15;
        $grupo = Grupo::find($grupo_id);
        $categorias = Categoria::where('grupo_id', $grupo->id)->where('st', 1)->where('st2', 1)->orderBy('order', 'asc')->get();
        $selected_cat = Categoria::find($request->query('cat'));

        // Obtengo posts
        $posts = $grupo->posts()->where('st2', 1);
        if($selected_cat !== null) $posts = $posts->where('categoria_id', $selected_cat->id);
        $posts = $posts
            ->orderBy('order', 'asc')
            ->get();
        
        // $posts = $grupo->posts()->where('st2', 1)->latest()->paginate($items);

        $view = View::make('admin.posts.index', [
            'posts' => $posts,
            'grupo' => $grupo,
            'categorias' => $categorias,
            'selected_cat' => $selected_cat
        ]);
        return $view;
    }

    public function edit($grupo_id=null, $id=null)
    {
        if($id!=null){
            $post = Post::find($id);
            $title = $post->titulo;
            $grupo = Grupo::find($post->grupo_id);
        } else {
            $post = null;
            $title = 'Nuevo item';
            $grupo = Grupo::find($grupo_id);
        }
        
        $categorias = Categoria::all()->where('grupo_id', $grupo->id)->where('st2', 1)->where('st', 1)->sortBy('order');
        $subcategorias = Subcategoria::all()->where('st2', 1)->where('st', 1);
        
        $mappings = GrupoMapping::where('grupo_id', $grupo->id)->orderBy('id', 'ASC')->get();
        $mappings_arr = array();
        foreach($mappings as $item){
            $mappings_arr[] = $item->nombre;
        }
        
        if($post != null && $post->subcategoria_id != null){
            $subcategoria = Subcategoria::find($post->subcategoria_id);
        } else {
            $subcategoria = null;
        }

        if($post != null && $post->categoria_id != null){
            $categoria = Categoria::find($post->categoria_id);
        } else {
            $categoria = null;
        }

        $view = View::make('admin.posts.edit', [
            'title' => $title,
            'post' => $post,
            'categorias' => $categorias,
            'subcategorias' => $subcategorias,
            'mappings' => $mappings,
            'mappings_arr' => $mappings_arr,
            'subcategoria' => $subcategoria,
            'categoria' => $categoria,
            'grupo' => $grupo,
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        if(isset($id) && $id!=null && is_numeric($id) && $id > 0) $edit = true; else $edit = false;
        
        if($request->input('grupo_id')!== null){

            // VALIDACION EN BASE A MAPPINGS
            $mappings = GrupoMapping::where('grupo_id', $request->input('grupo_id'))->orderBy('id', 'ASC')->get();
            $validationRules = array();
            foreach($mappings as $item){
                if($item->requerido == 1){
                    // Aca verifico si hay requeridos q tengan reglas custom
                    if($item->campo == 'img' || $item->campo == 'img2' || $item->campo == 'img3'){
                        $validationRules[$item->campo] = 'sometimes|required|max:3000|mimes:jpeg,jpg,bmp,png';
                    } else if($item->campo == 'slug'){
                        $arr = array(
                            'required', Rule::unique('posts')->ignore($id), new NoSpaces
                        );
                        $validationRules[$item->campo] = $arr;
                    } else { // default required validation
                        $validationRules[$item->campo] = 'required';
                    }
                }
            }

            $this->validate($request, $validationRules);

            if($edit) {
                $post = Post::find($id);
            } else {
                $post = new Post;
            }

            $post->titulo = $request->input('titulo');
            $post->subida = $request->input('subida');
            $post->slug = $request->input('slug');
            $post->titulo_corto = $request->input('titulo_corto');
            $post->bajada = $request->input('bajada');
            $post->body = $request->input('body');
            $post->url_externo = $request->input('url_externo');
            $post->fecha_custom = $request->input('fecha_custom');
            $post->simple_tags = $request->input('simple_tags');
            $post->grupo_id = $request->input('grupo_id');

            if(!$edit){
                $post->order = 0;
            }
            
            if($request->input('destacado') == 1 || $request->input('destacado') == true){
                $post->destacado = true;
            } else {
                $post->destacado = false;
            }

            if(isset($request->categoria_id) && is_numeric($request->categoria_id) && $request->categoria_id > 0){
                $post->categoria_id = $request->input('categoria_id');
            } else {
                $post->categoria_id = null;
            }

            if(isset($request->subcategoria_id) && is_numeric($request->subcategoria_id) && $request->subcategoria_id > 0){
                $post->subcategoria_id = $request->subcategoria_id;
            } else {
                $post->subcategoria_id = null;
            }

            if($request->hasFile('img')){
                $file = $request->file('img');
                $extension = $file->extension();
                $newfilename = 'post-img.'.$extension;
                $imgpath = time().'-'.$newfilename;
                // $img = Image::make($file)->resizeCanvas(1920, 1080)->save('storage/'.$imgpath);
                $img = Image::make($file)->save('storage/'.$imgpath);
                $post->img = $imgpath;
            }
            if($request->hasFile('img2')){
                $file = $request->file('img2');
                $newfilename = 'post-img2.'.$extension;
                $imgpath = time().'-'.$newfilename;
                // $img = Image::make($file)->resizeCanvas(600, 480)->encode('jpg', 80)->save('storage/'.$imgpath);
                $img = Image::make($file)->save('storage/'.$imgpath);
                $post->img2 = $imgpath;
            }
            if($request->hasFile('img3')){
                $file = $request->file('img3');
                $newfilename = 'post-img3.'.$extension;
                $imgpath = time().'-'.$newfilename;
                $img = Image::make($file)->resizeCanvas(1920, 1080)->encode('jpg', 80)->save('storage/'.$imgpath);
                $post->img3 = $imgpath;
            }
            
            $post->save();

            Session::flash('success', 'El post fue guardado con éxito.');
            return redirect()->route('admin.posts.index', ['grupo_id' => $post->grupo_id]);

        } else {
            return redirect()->route('admin.posts.index', ['grupo_id' => $request->input('grupo_id')]);
        }
    }
    

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->st2 = 0;
        $post->save();
        Session::flash('success', 'El post fue borrado!');
        return redirect()->route('admin.posts.index', ['grupo_id'=>$post->grupo_id]);
    }

    public function st(Request $request, $id)
    {
        $post = Post::find($id);
        $post->st = $request->input('st');
        $post->save();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
    
    public function sort(Request $request, $grupo_id=null)
    {
        if($grupo_id !== null){
            $elemsArr = $request->input('elem');
            $x=1;
            foreach($elemsArr as $item){
                $obj = Post::find($item);
                if($obj !== null){
                    $obj->order = $x;
                    $obj->save();
                    $x++;
                }
            }
            return response()->json([
                'success' => true,
                'error' => null
            ]);
        }
    }

}
