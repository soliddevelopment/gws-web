<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Session;
use Storage;
use DB;
use Image;
use File;
use App\Models\Producto;
use App\Models\Grupo;
use App\Models\Categoria;
use App\Models\Subcategoria;

class ProductosController extends BaseController
{
    public function index($grupo)
    {
        $limit = 3;
        // $productos = User::all()->where('st', 1);
        $grupo = Grupo::find($grupo);
        // $productos = $grupo->productos()->where('st2', 1)->latest()->get();
        $productos = $grupo->productos()->where('st2', 1)->latest()->paginate($limit);
        return view('admin.productos.index')->with('productos', $productos)->with('grupo', $grupo);
    }

    public function create($grupo)
    {
        $grupo = Grupo::find($grupo);

        $categorias = Categoria::all()->where('grupo_id', $grupo->id)->where('st2', 1)->where('st', 1);
        $subcategorias = Subcategoria::all()->where('st2', 1)->where('st', 1);

        return view('admin.productos.create')
                ->with('grupo', $grupo)
                ->with('categorias', $categorias)
                ->with('subcategorias', $subcategorias);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'bajada' => 'required|string|max:255',
            'precio' => 'numeric|min:0',
            'img' => 'max:3000|mimes:jpeg,jpg,bmp,png', // 10240 10 mb // 'required|mimes:jpeg,bmp,png|size:5000',
            'grupo_id' => 'required|integer|min:1',
        ]);

        $producto = new Producto;
        $producto->titulo = $request->titulo;
        $producto->bajada = $request->bajada;
        $producto->precio = $request->precio;
        $producto->body = $request->body;
        $producto->simple_tags = $request->simple_tags;
        $producto->grupo_id = $request->grupo_id;
        $producto->order = $request->order;

        // subcategoria
        if(isset($request->subcategoria_id) && is_numeric($request->subcategoria_id) && $request->subcategoria_id > 0){
            $producto->subcategoria_id = $request->subcategoria_id;
        } else {
            $producto->subcategoria_id = null;
        }

        // img principal
        if($request->hasFile('img')){
            $file = $request->file('img');
            $imgpath = time().'-'.$file->getClientOriginalName();
            $img = Image::make($file)->save('storage/'.$imgpath);
            $producto->img = $imgpath;
        }
        
        // Guardar producto
        $producto->save();

        Session::flash('success', 'El producto fue guardado con éxito.');
        return redirect()->route('admin.productos.index', ['grupo' => $producto->grupo_id]);
        dd($request->all());
    }
    

    public function edit($id)
    {
        $producto = Producto::find($id);
        $grupo = Grupo::find($producto->grupo_id);

        $categorias = Categoria::all()->where('grupo_id', $grupo->id)->where('st2', 1)->where('st', 1);
        $subcategorias = Subcategoria::all()->where('st2', 1)->where('st', 1);
        
        if($producto->subcategoria_id != null){
            $subcategoria = Subcategoria::find($producto->subcategoria_id);
        } else {
            $subcategoria = null;
        }

        return view('admin.productos.edit')
                ->with('producto', $producto)
                ->with('categorias', $categorias)
                ->with('subcategorias', $subcategorias)
                ->with('subcategoria', $subcategoria)
                ->with('grupo', $grupo);
    }

    public function update(Request $request, $id)
    {
        // validate data
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'bajada' => 'required|string|max:255',
            'grupo_id' => 'required|integer|min:1',
            'precio' => 'numeric|min:0',
            'img' => 'max:3000|mimes:jpeg,jpg,bmp,png',
        ]);

        // Guardo info del usuario
        $producto = Producto::find($id);
        $producto->titulo = $request->input('titulo');
        $producto->bajada = $request->input('bajada');
        $producto->precio = $request->input('precio');
        $producto->body = $request->input('body');
        $producto->simple_tags = $request->input('simple_tags');
        // $producto->grupo_id = $request->grupo_id;

        // subcategoria
        if(
            $request->input('subcategoria_id') 
            && is_numeric($request->input('subcategoria_id')) 
            && $request->input('subcategoria_id') > 0)
        {
            $producto->subcategoria_id = $request->input('subcategoria_id');
        } else {
            $producto->subcategoria_id = null;
        }

        // img principal
        if($request->hasFile('img')){
            if(Storage::disk('public')->has($producto->img)){
                Storage::disk('public')->delete($producto->img);
            }
            $file = $request->file('img');
            $imgpath = time().'-'.$file->getClientOriginalName();
            $img = Image::make($file)->save('storage/'.$imgpath);
            $producto->img = $imgpath;
        }

        $producto->save();

        Session::flash('success', 'El producto se modificó exitosamente');
        return redirect()->route('admin.productos.edit', $producto->id);
    }

    public function destroy($id)
    {
        $producto = Producto::find($id);
        $producto->st2 = 0;
        $producto->save(); // soft delete
        Session::flash('success', 'El producto fue borrado!');
        return redirect()->route('admin.productos.index', ['grupo'=>$producto->grupo_id]);
    }

    public function st(Request $request, $id)
    {
        $producto = Producto::find($id);
        $producto->st = $request->input('st');
        $producto->save();

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

}
