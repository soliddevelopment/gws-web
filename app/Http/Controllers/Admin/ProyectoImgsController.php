<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Storage;
use File;
use App\Models\Proyecto;
use App\Models\ProyectoImg;

class ProyectoImgsController extends BaseController
{
    public function index($proyectoId)
    {
        $proyecto = Proyecto::find($proyectoId);
        $imgs = $proyecto->imgs->sortBy('orden');
        $imgUploadAction = action('Admin\ProyectoImgsController@upload', ['proyectoId'=>$proyecto->id]);

        return view('admin.proyectos.imgs.index')
            ->with('imgs', $imgs)
            ->with('imgUploadAction', $imgUploadAction);
    }

    public function create($proyectoId)
    {
        $proyecto = Proyecto::find($proyectoId);
        $grupo = $proyecto->grupo;
        return view('admin.proyectos.imgs.post_create')
            ->with('grupo', $grupo)
            ->with('post', $proyecto);
    }
    
    
    public function upload(Request $request, $proyectoId){
        $proyecto = Proyecto::find($proyectoId);
        $img = new ProyectoImg;
        $img->proyecto_id = $proyecto->id;
        $prefix = 'solid-work-';

        $img->titulo = '';
        $img->descripcion = '';
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filepath = $prefix.time().'-'.$file->getClientOriginalName();
            $file = Image::make($file)->save('storage/'.$filepath);
            $img->img = $filepath;
        }
        $img->save();
        return response()->json([
            'success' => true,
            'error' => null,
            'img_obj' => array(
                'id' => $img->id,
                'imgpath_small' => thumb($img->img, 84, 84),
                'imgpath_big' => thumb($img->img, 1200, 900)
            )
        ]);
    }

    public function destroy(Request $request){
        $img = ProyectoImg::find($request->input('id'));
        if(strlen($img->img)>0){
            if(Storage::disk('public')->has($img->img)){
                Storage::disk('public')->delete($img->img);
            }
        }
        $img->delete();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
    
    public function sort(Request $request){
        $array = $request->input('img');
        $cambiados = 0;
        for($x=0; $x<count($array); $x++){
            $idActual = $array[$x];
            $img = ProyectoImg::find($idActual);
            $img->orden = $x+1;
            $img->save();
        }
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
}
