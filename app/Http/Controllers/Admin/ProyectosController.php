<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
// use Illuminate\Http\Support\Facades\Storage;
use Session;
use Storage;
use DB;
use Image;
use File;
use View;

use App\Models\Proyecto;
use App\Models\Servicio;

class ProyectosController extends BaseController
{
    public function index(Request $request){
        $proyectos = Proyecto::getLista(false, false);
        //$proyectos = Proyecto::all();
        $view = View::make('admin.proyectos.index', [
            'proyectos' => $proyectos
        ]);
        return $view;
    }

    public function edit($id=null)
    {
        if($id!=null){
            $proyecto = Proyecto::find($id);
            $title = $proyecto->titulo;
        } else {
            $proyecto = null;
            $title = 'Nuevo item';
        }  

        $lista_servicios = array();
        $lista_general_servicios = Servicio::where('st2', 1)->get();
        $array_seleccionados_editar = array();

        if($proyecto != null){
            foreach($proyecto->servicios as $item){
                array_push($array_seleccionados_editar, $item->id);
            }
        }

        foreach($lista_general_servicios as $item){
            if(in_array($item->id, $array_seleccionados_editar)){
                $item->checked = true;
            } else {
                $item->checked = false;
            }
            array_push($lista_servicios, $item);
        }
        // dd($lista_servicios);
        $view = View::make('admin.proyectos.edit', [
            'title' => $title,
            'proyecto' => $proyecto,
            'lista_servicios' => $lista_servicios
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        $array_validations = array(
            'titulo' => 'required|string|max:255',
            'slug' => [
                'required', Rule::unique('proyectos')->ignore($id),
            ],
            'img_logo' => 'sometimes|required|max:3000|mimes:png', // 10240 10 mb // 'required|mimes:jpeg,bmp,png|size:5000',
            'img_thumb' => 'sometimes|required|max:3000|mimes:jpeg,jpg,bmp,png', // 10240 10 mb // 'required|mimes:jpeg,bmp,png|size:5000',
            'img_intro' => 'sometimes|required|max:3000|mimes:jpeg,jpg,bmp,png', // 10240 10 mb // 'required|mimes:jpeg,bmp,png|size:5000',
        );
        $this->validate($request, $array_validations);
        
        if(isset($id) && $id!=null && is_numeric($id) && $id > 0){
            $edit = true;
            $proyecto = Proyecto::find($id);
        } else {
            $edit = false;
            $proyecto = new Proyecto;
            
            $proyecto->orden = 0;
            $proyecto->st = 1;
            $proyecto->st2 = 1;
        }

        $proyecto->cliente = $request->input('cliente');
        $proyecto->titulo = $request->input('titulo');
        $proyecto->slug = $request->input('slug');
        $proyecto->bajada = $request->input('bajada');
        $proyecto->descripcion = $request->input('descripcion');
        $proyecto->fecha = $request->input('fecha');

        if($request->destacado == 1 || $request->destacado == true){
            $proyecto->destacado = true;
        } else {
            $proyecto->destacado = false;
        }

        if($request->hasFile('img_logo')){
            $file = $request->file('img_logo');
            $oldfilename = $file->getClientOriginalName();
            $newfilename = str_replace(' ', '', $oldfilename);
            $imgpath = time().'-'.$newfilename;
            $img = Image::make($file)->resizeCanvas(400, 252)->save('storage/'.$imgpath);
            $proyecto->img_logo = $imgpath;
        }
        if($request->hasFile('img_thumb')){
            $file = $request->file('img_thumb');
            $oldfilename = $file->getClientOriginalName();
            $newfilename = str_replace(' ', '', $oldfilename);
            $imgpath = time().'-'.$newfilename;
            $img = Image::make($file)->resizeCanvas(600, 480)->encode('jpg', 80)->save('storage/'.$imgpath);
            $proyecto->img_thumb = $imgpath;
        }
        if($request->hasFile('img_intro')){
            $file = $request->file('img_intro');
            $oldfilename = $file->getClientOriginalName();
            $newfilename = str_replace(' ', '', $oldfilename);
            $imgpath = time().'-'.$newfilename;
            $img = Image::make($file)->resizeCanvas(1920, 1080)->encode('jpg', 80)->save('storage/'.$imgpath);
            $proyecto->img_intro = $imgpath;
        }
        
        // Guardar post
        $proyecto->save();

        $proyecto->servicios()->detach();
        $lista_servicios = Servicio::all()->where('st2', 1);
        //dd($request->all());
        foreach($lista_servicios as $item){
            if($request->input('serv_'.$item->id) != null && $request->input('serv_'.$item->id) == "1" ){
                $proyecto->servicios()->attach($item->id);
            }
        }

        Session::flash('success', 'El proyecto fue guardado con éxito.');
        return redirect()->route('admin.proyectos.index');
    }
    
    public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->st2 = 0;
        $proyecto->save(); // soft delete
        Session::flash('success', 'El proyecto fue borrado!');
        return redirect()->route('admin.proyectos.index');
    }

    public function st(Request $request, $id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->st = $request->input('val');
        $proyecto->save();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

    public function destacado(Request $request, $id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->destacado = $request->input('val');
        $proyecto->save();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

    public function sort(Request $request)
    {
        $array = $request->input('elem');
        // dd($request->input('elem'));
        
        for($x=0; $x<count($array); $x++){
            $actual = $array[$x];
            $proyecto = Proyecto::find($actual);
            $proyecto->orden = $x+1;
            $proyecto->update();
        }
        
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
}
