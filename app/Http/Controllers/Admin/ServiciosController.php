<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Session;
use Storage;
use DB;
use Image;
use File;
use View;

use App\Models\Proyecto;
use App\Models\Servicio;

class ServiciosController extends BaseController
{
    public function index(Request $request){
        $servicios = Servicio::all()->where('st2', 1);
        $view = View::make('admin.servicios.index', [
            'servicios' => $servicios
        ]);
        return $view;
    }

    public function edit($id=null)
    {
        if($id!=null){
            $servicio = Servicio::find($id);
            $title = $servicio->titulo;
        } else {
            $servicio = null;
            $title = 'Nuevo item';
        }  

        // lista general de servicios
        // servicios chequeados, links
        
        /*$servicios = GrupoMapping::where('grupo_id', $grupo->id)->orderBy('id', 'ASC')->get();
        $servicios_arr = array();
        foreach($servicios as $item){
            $servicios_arr[] = $item->nombre;
        }*/

        $lista_servicios = Servicio::all()->where('st2', 1);

        $view = View::make('admin.servicios.edit', [
            'title' => $title,
            'servicio' => $servicio,
            'lista_servicios' => $lista_servicios
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
        ]);

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0) {
            $edit = true;
            $servicio = Servicio::find($id);
        } else {
            $edit = false;
            $servicio = new Servicio;
        }

        $servicio->st2 = 1;
        $servicio->titulo = $request->input('titulo');
        $servicio->save();

        Session::flash('success', 'El servicio fue guardado con éxito.');
        return redirect()->route('admin.servicios.index');
        dd($request->all());
    }
    
    public function destroy($id)
    {
        $servicio = Servicio::find($id);
        $servicio->st2 = 0;
        $servicio->save(); // soft delete
        Session::flash('success', 'El servicio fue borrado!');
        return redirect()->route('admin.servicios.index');
    }

    public function st(Request $request, $id)
    {
        $servicio = Servicio::find($id);
        $servicio->st = $request->input('val');
        $servicio->save();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
}
