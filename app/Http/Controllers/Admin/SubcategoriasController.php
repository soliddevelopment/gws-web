<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Categoria;
use App\Models\Subcategoria;
use App\Models\Grupo;
use Image;
use Session;

class SubcategoriasController extends BaseController
{
    public function index($categoria)
    {
        $cat = Categoria::find($categoria);
        $subcategorias = Subcategoria::all()->where('categoria_id', $cat->id);
        $grupo = Grupo::find($cat->grupo_id);
        // $subcategorias = Subcategoria::all()->sortByDesc('created_at');
        return view('admin.subcategorias.index')
                ->with('grupo', $grupo)
                ->with('categoria', $cat)
                ->with('subcategorias', $subcategorias);
    }


    public function create($categoria)
    {
        $categoria = Categoria::find($categoria);
        $subcategorias = $categoria->subcategorias();
        $grupo = Grupo::find($categoria->grupo_id);

        return view('admin.subcategorias.create')
                ->with('grupo', $grupo)
                ->with('categoria', $categoria)
                ->with('subcategorias', $subcategorias);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'nombre' => 'required|string|max:255',
            'img' => 'max:3000|mimes:jpeg,jpg,bmp,png', // 10240 10 mb // 'required|mimes:jpeg,bmp,png|size:5000',
            'categoria_id' => 'required|integer|min:1',
        ]);

        $subcategoria = new Subcategoria;
        $subcategoria->categoria_id = $request->categoria_id;
        $subcategoria->nombre = $request->nombre;
        $subcategoria->titulo = $request->titulo;

        // img principal
        if($request->hasFile('img')){
            $file = $request->file('img');
            $imgpath = 'subcat-'.time().'-'.$file->getClientOriginalName();
            // $img = Image::make($file)->resize(320, 240)->save($imgpath);
            $img = Image::make($file)->save('storage/'.$imgpath);
            $subcategoria->img = $imgpath;
        }

        // categoria
        if(isset($request->categoria_id) && is_numeric($request->categoria_id) && $request->categoria_id > 0){
            $subcategoria->categoria_id = $request->categoria_id;
        } else {
            $subcategoria->categoria_id = null;
        }
        
        // Guardar cat
        $subcategoria->save();

        Session::flash('success', 'La subcategoría fue guardado con éxito.');
        return redirect()->route('admin.subcategorias.index', ['categoria' => $subcategoria->categoria_id]);
        dd($request->all());
    }
    

    public function edit($id)
    {
        $subcategoria = Subcategoria::find($id);
        $categoria = Categoria::find($subcategoria->categoria_id);
        $grupo = Grupo::find($categoria->grupo_id);
        return view('admin.subcategorias.edit')
                ->with('grupo', $grupo)
                ->with('subcategoria', $subcategoria)
                ->with('categoria', $categoria);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'nombre' => 'required|string|max:255',
            'img' => 'max:3000|mimes:jpeg,jpg,bmp,png', 
            'categoria_id' => 'required|integer|min:1',
        ]);

        $subcategoria = Subcategoria::find($id);
        $subcategoria->titulo = $request->input('titulo');
        $subcategoria->nombre = $request->input('nombre');
        
        // img principal
        if($request->hasFile('img')){
            if(Storage::disk('public')->has($subcategoria->img)){
                Storage::disk('public')->delete($subcategoria->img);
            }
            $file = $request->file('img');
            $imgpath = 'subcat-'.time().'-'.$file->getClientOriginalName();
            // $img = Image::make($file)->resize(320, 240)->save($imgpath);
            $img = Image::make($file)->save('storage/'.$imgpath);
            $subcategoria->img = $imgpath;
        }

        $subcategoria->save();

        Session::flash('success', 'La subcategoría se modificó exitosamente');
        return redirect()->route('admin.subcategorias.edit', $subcategoria->id);
    }

    public function destroy($id)
    {
        $subcategoria = Subcategoria::find($id);
        $subcategoria->st2 = 0;
        $subcategoria->save(); // soft delete

        Session::flash('success', 'La subcategoría fue borrada!');
        return redirect()->route('admin.subcategorias.index', ['categoria'=>$subcategoria->categoria_id]);
    }

    public function st(Request $request, $id)
    {
        $subcategoria = Subcategoria::find($id);
        $subcategoria->st = $request->input('st');
        $subcategoria->save();

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
}
