<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Session;
use View;

use App\Models\User;
use App\Models\Role;

class UsuariosController extends BaseController
{
    public function index()
    {
        $user = Auth::user();
        $usuarios = $user->getUsers();
        return view('admin.usuarios.index')->with('usuarios', $usuarios);
    }

    public function create()
    {
        $roles = Role::all();
        return view('admin.usuarios.create')->with('roles', $roles);
    }

    public function store(Request $request, $id=null)
    {
        // validate data
        $array_validations = array(
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'usuario' => [
                'required', 'string', 'max:255',
                Rule::unique('users')->ignore($id),
            ],
            'email' => [
                'required', 'string', 'max:255', 'email',
                Rule::unique('users')->ignore($id),
            ],
            'password' => 'sometimes|required|string|min:6|confirmed',
            'password_confirmation' => 'sometimes|required|min:6'
        );

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0){
            $edit = true;
            $usuario = User::find($id);
        } else {
            $edit = false;
            $usuario = new User;

            $password_validations = array(
                'password' => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required|min:6'
            );
            array_push($array_validations, $password_validations);
        }
        
        $this->validate($request, $array_validations);

        $user_posting = Auth::user(); // el admin loggeado
        $isGlobal = $user_posting->hasRole('Global');
        $isAdmin = $user_posting->hasRole('Admin');

        if($isGlobal) { $continuar = true; } else {
            $continuar = $this->permisosEditarUsuario($user_posting, $usuario);
        }

        if($continuar){
            $usuario->nombre = $request->input('nombre');
            $usuario->apellido = $request->input('apellido');
            $usuario->usuario = $request->input('usuario');
            $usuario->email = $request->input('email');
            $usuario->password = bcrypt($request->input('password'));

            $usuario->save();

            if($isGlobal && $usuario->id == $user_posting->id){
                // es global editandose a si mismo, no cambiar los roles para nada
            } else if( $isGlobal || $isAdmin ) {
                $usuario->roles()->detach();
                /* if($request['roles'] && count($request['roles'])>0){
                    foreach ($request['roles'] as $item) {
                        $role_actual = Role::where('id', $item)->first();
                        if($role_actual->nombre != 'Global'){
                            $usuario->roles()->attach($role_actual);
                        }
                    }
                } */
                if($request['role'] && is_numeric($request['role'])){
                    $role_actual = Role::where('id', $request['role'])->first();
                    if($role_actual->nombre != 'Global'){ // no dejo que NUNCA se pueda asignar un permiso global
                        $usuario->roles()->attach($role_actual);
                    }
                }
            }

            Session::flash('success', 'El usuario fue guardado con éxito.');
            return redirect()->route('admin.usuarios.index');

        } else {
            $err = ['No tiene permisos para editar a este usuario'];
            return redirect()->route('admin.usuarios.index')->withErrors($err);
        }
    }

    public function edit($id=null)
    {
        if($id!=null){
            $user = User::find($id);
            $title = $user->usuario;
        } else {
            $user = null;
            $title = 'Nuevo usuario';
        }
        $roles = Role::all();
        
        $view = View::make('admin.usuarios.edit', [
            'user' => $user,
            'roles' => $roles,
            'title' => $title,
        ]);
        return $view;
    }

    public function updatePassword(Request $request, $id)
    {
        // validate data
        $this->validate($request, [
            'password' => 'string|min:6|confirmed',
            'password_confirmation' => 'min:6'
        ]);

        $user = Auth::user(); // el admin loggeado
        $usuario = User::find($id); // el usuario siendo editado
        
        $continuar = $this->permisosEditarUsuario($user, $usuario);

        if($continuar){
            $usuario->password = bcrypt($request->input('password'));
            $usuario->save();
            Session::flash('success', 'La contraseña del usuario se modificó exitosamente');
            return redirect()->route('admin.usuarios.edit', $usuario->id);
        
        } else {
            // Un Admin esta tratando de editar a un Global
            $roles = Role::all();
            $err = ['No tiene permisos para editar a este usuario'];
            return redirect()->route('admin.usuarios.index')->withErrors($err);
        }
        
    }
    
    public function permisosEditarUsuario($user, $usuario){
        $continuar = false;
        if($user->id == $usuario->id){
            $continuar = true;
        } else if($user->hasRole('Global')){
            $continuar = true;
        } else if( $user->hasRole('Admin') ){
            if($usuario->hasRole('Global') || $usuario->hasRole('Admin')){ // admin tratando d editar a otro admin o a global
                $continuar = false;
            } else $continuar = true;
        } else $continuar = false;
        return $continuar;
    }

    public function destroy($id)
    {
        /*$usuario = User::find($id);
        $usuario->roles()->detach();
        $usuario->delete();*/
        
        $usuario = User::find($id);
        $usuario->st2 = 0;
        $usuario->save(); // soft delete
        Session::flash('success', 'El usuario fue borrado!');
        return redirect()->route('admin.usuarios.index');
    }

    public function st(Request $request, $id)
    {
        // Guardo info del usuario
        $usuario = User::find($id);
        $usuario->st = $request->input('st');
        $usuario->save();

        // HACER SOFT DELETE MEJOR
        // NO DEJAR BORRAR SI EL USUARIO TIENE ROLES

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
}
