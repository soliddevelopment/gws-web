<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

class ImageController extends Controller
{

    public function thumb()
    {
        $img = \Input::get('img', null);
        $w = \Input::get('w', false);
        $h = \Input::get('h', false);

        if($w != null && $h != null && $w > 0 && $h > 0){
            $img = Image::make(public_path($img))->fit($w, $h)->response('jpg');
        } else {
            $img = Image::make(public_path($img))->response('jpg');
        }
        return $img;
        // return Image::make(public_path($img))->resize($w, $h)->response('jpg');
    }
}
