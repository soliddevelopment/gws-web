<?php

namespace App\Http\Controllers\Publico;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Session;
use View;

use App\Models\Post;

class BlogController extends BaseController
{

    public function index(Request $request){
        $posts = Post::ultimosPaginados('noticias', 15);
        $view= \View::make('publico.blog.index', [
            'posts' => $posts
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }

    public function detalle(Request $request, $id){
        $post = Post::find($id);
        $view= \View::make('publico.blog.detalle', [
            'post' => $post
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    /* public function contacto(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string|max:255',
            'empresa' => 'string|max:255',
            'email' => 'required|email',
            // 'telefono' => 'string|max:255',
            // 'mensaje' => 'string|max:255',
            // 'empresa' => 'sometimes|required|string|max:255',
            // 'img' => 'max:3000|mimes:jpeg,jpg,bmp,png', // 10240 10 mb // 'required|mimes:jpeg,bmp,png|size:5000',
        ]);

        $contacto = new Contacto;
        $contacto->tipo = $request->input('tipo');
        $contacto->nombre = $request->input('nombre');
        $contacto->empresa = $request->input('empresa');
        $contacto->email = $request->input('email');
        $contacto->telefono = $request->input('telefono');
        $contacto->mensaje = $request->input('mensaje');

        // Guardar post
        $contacto->save();

        // Session::flash('success', 'El contacto fue guardado con éxito.');
        $email = "heymans@brandy.la";
        Mail::to($email)->send(new NuevoContacto($contacto));

        return response()->json([
            'success'   => true, 
            'contacto'     => $contacto
        ], 200);

    } */
    

}
