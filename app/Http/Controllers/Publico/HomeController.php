<?php

namespace App\Http\Controllers\Publico;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Session;
use View;
use Validator;

use App\Models\Contacto;
use App\Models\User;
use App\Mail\NuevoContacto;

use App\Models\Post;
use App\Models\Custom\Producto;
// use App\Models\Custom\Galeria;

class HomeController extends BaseController
{

    public function index(Request $request){
        $slides = Post::find_for_grupo('home_banner');
        $destacados = Post::find_for_grupo('productos', 0, true);
        // $comercios = Galeria::find_comercios();

        $videos = [
            [
                'titulo' => 'Aperol Brand Film',
                'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum convallis posuere velit, pharetra dignissim purus tincidunt ut. Etiam vulputate vel orci et auctor. Nunc at erat eget enim pellentesque imperdiet sed a risus. Fusce molestie mauris ut mi dignissim volutpat. ',
                'img' => 'aperol2.jpg',
                'video_id' => '10VHupfyy0o'
            ],
            [
                'titulo' => 'Campari Timeless',
                'descripcion' => 'Nunc at erat eget enim pellentesque imperdiet sed a risus. Fusce molestie mauris ut mi dignissim volutpat. ',
                'img' => 'negroni.jpg',
                'video_id' => 'AkXhARMh7S8'
            ],
            [
                'titulo' => "Master's Gin",
                'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum convallis posuere velit, pharetra dignissim purus tincidunt ut. Etiam vulputate vel orci et auctor. Nunc at erat eget enim pellentesque imperdiet sed a risus. Fusce molestie mauris ut mi dignissim volutpat. ',
                'img' => 'gin2.jpg',
                'video_id' => 'siJ4n5p9HaI'
            ],
            [
                'titulo' => 'Stoli make it loud and clear',
                'descripcion' => '',
                'img' => 'stoli2.jpg',
                'video_id' => 'fqW_1fy8Uus'
            ]
        ];

        $imgs_dinamicas = array();
        foreach($slides as $item){
            array_push($imgs_dinamicas, thumb($item->img, 600, 400));
        }

        $view= \View::make('publico.home.home', [
            'slides' => $slides,
            'destacados' => $destacados,
            'videos' => $videos,
            // 'comercios' => $comercios,
            'imgs_dinamicas' => $imgs_dinamicas,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function nosotros(Request $request){
        $view= \View::make('publico.home.nosotros', []);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function contacto(Request $request){
        $view= \View::make('publico.home.contacto', []);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function contactoPost(Request $request){
        $array_validations = array(
            'nombre' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'g-recaptcha-response'=>'required|recaptcha'
        );
        
        $validator = Validator::make($request->all(), $array_validations, [
            'g-recaptcha-response.required' => 'El captcha check falló'
        ]);
        if ($validator->fails()) $this->validatorErrors($validator);


        if(Contacto::isSpam($request)){
            // SE SIMULA EXITOSO
            return response()->json([
                'success' => true,
                'error' => null
            ]);
        }

        $contacto = new Contacto();
        $contacto->nombre = $request->input('nombre');
        $contacto->email = $request->input('email');
        $contacto->mensaje = $request->input('mensaje');
        // $contacto->apellido = $request->input('apellido');
        // $contacto->empresa = $request->input('empresa');

        $contacto->save();

        //$heymansUser = User::where('usuario', 'heymans')->first();

        /*$userContacto = User::where('usuario', 'luis')->first();
        $userContacto->notify(new \App\Notifications\NuevoContacto($contacto));*/

        $to = [
            [
                'name' => 'GWS Comercial',
                'email' => 'acomercial@gws.com.sv',
            ],
            [
                'name' => 'Luis Alvarenga',
                'email' => 'luis.alvarenga@gws.com.sv',
            ],
        ];

        $mail = Mail::to($to)->send(
            new NuevoContacto($contacto)
        );

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
    
    private function validatorErrors($validator){
        return response()->json([
            'success'       => false,
            'errors'       => $validator->errors()
        ], 400);
    }
    
    /*public function index(Request $request){
        $servicios = Post::find_servicios('software');
        $clientes = Post::find_posts_for_grupo_categoria('clientes', 'software');
        $view= \View::make('publico.software.home', [
            'seccion' => 'software',
            'servicios' => $servicios,
            'clientes' => $clientes,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }*/

}
