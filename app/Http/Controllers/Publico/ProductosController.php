<?php

namespace App\Http\Controllers\Publico;

use App\Exports\ProductsExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

use App\Models\Grupo;
use App\Models\User;
use App\Models\Post;
use App\Models\Custom\CategoriaProducto;
use App\Models\Custom\Producto;

class ProductosController extends BaseController
{

    public function index(Request $request){
        $grupo = Grupo::where('nombre', 'productos')->first();
        $categorias = CategoriaProducto::where('grupo_id', $grupo->id)
            ->where('st', 1)->where('st2', 1)
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->get();
        
//        $productos = Producto::todos();
//        return Excel::download(new ProductsExport($productos), 'productos-gws.xlsx');

        $imgs_dinamicas = array();
        foreach($categorias as $cat){
            $cat->productos = $cat->productos->where('st', 1)->where('st2', 1)->sortBy('order');
        }

        // dd($categorias);
        $view = \View::make('publico.productos.index', [
            'categorias' => $categorias,
            'imgs_dinamicas' => $imgs_dinamicas, 
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function detalle(Request $request, $slug=null){
        $producto = Producto::where('slug', $slug)->first();
        $grupo = Grupo::where('nombre', 'productos')->first();
        $otros = Producto::inRandomOrder()->where('grupo_id', $grupo->id)->where('slug', '!=', $slug)->get();

        if($producto!=null){
            $view = \View::make('publico.productos.detalle', [
                'producto'=>$producto,
                'otros'=>$otros,
            ]);
            if ($request->ajax()) return $view->renderSections()['content']; else return $view;
        } else {
            return redirect()->route('publico.productos.index');
        }
    }
    
    private function validatorErrors($validator){
        return response()->json([
            'success'       => false, 
            'errors'       => $validator->errors()
        ], 400);
    }
    
}
