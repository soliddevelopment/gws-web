<?php

namespace App\Http\Controllers\Publico;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Session;
use View;

use App\Models\Custom\Test;

class TestController extends BaseController
{

    public function index(Request $request){
        $url = route('publico.test.index', [
            'recomendacion' => $request->input('recomendacion')
        ]);
        $view= \View::make('publico.test.index', [
            // 'nombre' => $request->input('nombre'),
            'recomendacion' => $request->input('recomendacion'),
            'url' => $url
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }

    public function subirResultado(Request $request){
        /*$this->validate($request, [
            'recomendacion' => 'required|integer|max:6|min:1',
            'nombre' => 'required|string|max:255',
            'email' => 'required|email',
            'fecha_ano' => 'required|integer',
            'fecha_mes' => 'required|integer',
            'fecha_dia' => 'required|integer',
            // 'telefono' => 'string|max:255',
            // 'mensaje' => 'string|max:255',
            // 'empresa' => 'sometimes|required|string|max:255',
            // 'img' => 'max:3000|mimes:jpeg,jpg,bmp,png', // 10240 10 mb // 'required|mimes:jpeg,bmp,png|size:5000',
        ]);*/

        $ano = $request->input('fecha_ano');
        $mes = $request->input('fecha_mes');
        $dia = $request->input('fecha_dia');

        if(strlen($mes)==1) $mes = "0".$mes;
        if(strlen($dia)==1) $dia = "0".$dia;

        $fecha = $ano."-".$mes."-".$dia;
        
        $test = new Test;
        $test->recomendacion = $request->input('recomendacion');
        $test->nombre = $request->input('nombre');
        $test->email = $request->input('email');
        $test->fecha_nacimiento = $fecha;
        $test->save();

        /* $view= \View::make('publico.test.resultados', [
            'nombre' => $request->input('nombre'),
            'email' => $request->input('email'),
            'recomendacion' => $request->input('recomendacion')
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view; */

        // Session::flash('success', 'El post fue guardado con éxito.');
        return redirect()->route('publico.test.resultados', [
            'nombre' => $request->input('nombre'),
            // 'email' => $request->input('email'),
            'recomendacion' => $request->input('recomendacion')
        ]);
    }

    public function resultados(Request $request){
        $url = route('publico.test.index', [
            'recomendacion' => $request->input('recomendacion')
        ]);
        $view= \View::make('publico.test.resultados', [
            'nombre' => $request->input('nombre'),
            'recomendacion' => $request->input('recomendacion'),
            'url' => $url
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    /* public function contacto(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string|max:255',
            'empresa' => 'string|max:255',
            'email' => 'required|email',
            // 'telefono' => 'string|max:255',
            // 'mensaje' => 'string|max:255',
            // 'empresa' => 'sometimes|required|string|max:255',
            // 'img' => 'max:3000|mimes:jpeg,jpg,bmp,png', // 10240 10 mb // 'required|mimes:jpeg,bmp,png|size:5000',
        ]);

        $test = new Contacto;
        $test->tipo = $request->input('tipo');
        $test->nombre = $request->input('nombre');
        $test->empresa = $request->input('empresa');
        $test->email = $request->input('email');
        $test->telefono = $request->input('telefono');
        $test->mensaje = $request->input('mensaje');

        // Guardar post
        $test->save();

        // Session::flash('success', 'El contacto fue guardado con éxito.');
        $email = "heymans@brandy.la";
        Mail::to($email)->send(new NuevoContacto($test));

        return response()->json([
            'success'   => true, 
            'contacto'     => $test
        ], 200);

    } */
    

}
