<?php

namespace App\Http\Controllers\Publico;

use Illuminate\Http\Request;
use View;

use App\Models\Proyecto;
use App\Models\Servicio;

class WorkController extends BaseController
{
    public function index(Request $request){
        $proyectos = Proyecto::getLista();
        $view= \View::make('publico.work.index', [
            'proyectos' => $proyectos
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function detalle(Request $request, $slug){
        $proyecto = Proyecto::where('st2', 1)->where('slug', $slug)->first();
        $imgs = $proyecto->imgs->sortBy('orden');
        
        $view= \View::make('publico.work.detalle', [
            'proyecto' => $proyecto,
            'imgs' => $imgs,
        ]);
        // dd($proyecto);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
}
