<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class CheckAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() === null){
            // return response('No tiene permisos para entrar aquí', 401);
            return redirect()->route('admin.home.home');
            // return response('No tiene permisos para entrar aquí 1 ', 401);
        }

        $actions = $request->route()->getAction();
        $roles = isset($actions['roles']) ? $actions['roles'] : null;

        if($request->user()->hasAnyRole($roles) || !$roles){
            return $next($request);
        }

        /*
        SELECT roles.*, user_role.user_id AS user_id
        FROM roles
        LEFT JOIN user_role ON user_role.role_id = roles.id
        WHERE user_role.user_id = 5
        */

        $roles = DB::table('roles')
            ->leftJoin('user_role', 'user_role.role_id', '=', 'roles.id')
            ->select('roles.*', 'user_role.user_id AS user_id')
            ->where('user_role.user_id', '=', $request->user()->id)
            ->get();

        if(count($roles)>0){
            return redirect()->route('admin.home.home');
        } else {
            return redirect()->route('publico.home.home');
        }

        // return response('No tiene permisos para entrar aquí 2', 401);
        

    }

}
