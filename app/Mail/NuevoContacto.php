<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NuevoContacto extends Mailable
{
    use Queueable, SerializesModels;

    public $contacto;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($contacto)
    {
        $this->contacto = $contacto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Nuevo contacto de '.$this->contacto->nombre;
        $titulo = 'Recibiste un nuevo mensaje de contacto';
        $url_boton = route('admin.contactos.detalle', ['id'=>$this->contacto->id]);
        $txt_boton = 'Ir a contacto';
        $logo_url = asset('p/img/logo-email.png');

        $body = '';
        $body .= '<p><b>Name</b>: '.$this->contacto->nombre.'</p>';
        $body .= '<p><b>Email</b>: '.$this->contacto->email.'</p>';
        //$body .= '<p><b>Company</b>: '.$this->contacto->empresa.'</p>';
        $body .= '<p><b>Message</b>: '.$this->contacto->mensaje.'</p>';

        return $this
            ->from('contact@solid.com.sv', 'GWS website')
            ->subject($subject)
            /*->attachData($this->attachedData, 'name.pdf', [
                'mime' => 'application/pdf',
            ])*/
            ->markdown('publico.emails.generico2', [
                'logo' => $logo_url,
                'titulo' => $titulo,
                'body' => $body,
                'btn_route' => $url_boton,
                'btn_txt' => $txt_boton,
            ]);
    }
}
