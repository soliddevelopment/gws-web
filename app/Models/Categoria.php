<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class Categoria extends Base
{
    
    protected $table = 'categorias';

    public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    public function subcategorias(){
        return $this->hasMany('App\Models\Subcategoria');
    }
}
