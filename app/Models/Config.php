<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Config extends Base
{
    protected $table = 'configs';
    protected $tienePermisos = true;

    public function getVal()
    {
        switch($this->tipo){
            case 'IMG':
                $val = $this->valor_img;
                break;
            case 'STR':
                $val = $this->valor_str;
                break;
            case 'INT':
                $val = $this->valor_int;
                break;
            case 'BOOL':
                $val = $this->valor_bool;
                break;
        }
        return $val;
    }

}
