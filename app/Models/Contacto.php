<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;
use Illuminate\Http\Request;

class Contacto extends Base
{
    protected $table = 'contactos';

    public static function isSpam(Request $request){
        $antiSpamArr = ['issue', 'inquiry', 'email_address'];
        $spamFields = 0;
        foreach($antiSpamArr as $item){
            if($request->input($item) && strlen($request->input($item))>0){
                $spamFields++;
                // fue llenado, se suma cantidad de spam llenado
            }
        }
        if($spamFields == 0) {
            return false;
        }

        // ES SPAM
        \Log::error('CONTACT FORM SPAM', $request->all());
        return true;
    }
    

}
