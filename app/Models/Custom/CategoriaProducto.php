<?php

namespace App\Models\Custom;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\Categoria;
use App\Models\Post;

class CategoriaProducto extends Categoria
{
    
    public function productos(){
        return $this->hasMany('App\Models\Custom\Producto', 'categoria_id');
    }
    
}
