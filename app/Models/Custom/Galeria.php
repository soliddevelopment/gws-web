<?php

namespace App\Models\Custom;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\Post;

class Galeria extends Post
{
    
    public static function find_comercios(){
        $grupo_nombre = 'galerias';
        
        // $posts = DB::table('posts')
        //     ->join('grupos', 'grupos.id', '=', 'posts.grupo_id')
        //     ->select('posts.*', 'grupos.nombre AS grupo_nombre')
        //     ->where('grupos.nombre', $grupo_nombre)
        //     ->where('posts.st', 1)
        //     ->where('posts.st2', 1)
        //     ->where('posts.slug', 'comercios');
        // $posts = $posts->first();
        // $comercios = $posts->imgs;
        // return $comercios;

        $post = Post::join('grupos', 'grupos.id', '=', 'posts.grupo_id')
            ->select('posts.*', 'grupos.nombre AS grupo_nombre')
            ->where('grupos.nombre', $grupo_nombre)
            ->where('posts.st', 1)
            ->where('posts.st2', 1)
            ->where('posts.slug', 'comercios')
            ->first();
        if($post !== null){
            $comercios = $post->imgs;
            $comercios = $comercios->sortBy('orden');
        } else {
            $comercios = [];
        }
        return $comercios;
    }

}
