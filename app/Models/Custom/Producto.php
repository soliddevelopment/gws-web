<?php

namespace App\Models\Custom;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Models\Post;

class Producto extends Post
{

    public static function todos($limit=0, $soloDestacados=false){
        $grupo_nombre = 'productos';
        $posts = DB::table('posts')
            ->join('grupos', 'grupos.id', '=', 'posts.grupo_id')
            ->join('categorias', 'categorias.id', '=', 'posts.categoria_id')  // Join con la tabla categorias
            ->select('posts.*', 'grupos.nombre AS grupo_nombre', 'categorias.nombre AS categoria_nombre')  // Seleccionar el nombre de la categoria
            ->where('grupos.nombre', $grupo_nombre)
            ->where('posts.st', 1)
            ->where('posts.st2', 1);
        if($limit > 0){
            $posts = $posts->take($limit);
        }
        if($soloDestacados){
            $posts = $posts->where('posts.destacado', 1);
        }
        $posts = $posts->get();
        return $posts;
    }


}
