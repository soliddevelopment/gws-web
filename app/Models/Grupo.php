<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Post;
use App\Models\Categoria;
use App\Models\Base;

class Grupo extends Base
{
    
    public function productos(){
    	return $this->hasMany('App\Models\Producto');
    }
    public function tipo(){
        return $this->belongsTo('App\Models\TipoGrupo');
    }
    public function posts(){
    	return $this->hasMany('App\Models\Post');
    }
    public function categorias(){
    	return $this->hasMany('App\Models\Categoria');
    }
    
    public function getCategorias(){
        return Categoria::all()->where('grupo_id', $this->id)->where('st2', 1)->sortBy('created_at');
    }

    public function getPosts(){
        $posts = Post::all()->where('grupo_id', $this->id);
        return $posts;
    }

}
