<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class GrupoMapping extends Base
{
    protected $table = 'grupos_mappings';

    public function grupo(){
        return $this->belongsTo('App\Models\Grupo');
    }

    public static function getMapping($campo, $array){
        foreach($array as $item){
            if($item->campo == $campo){
                return $item;
            } else {
                return null;
            }
        }
    }

    /* public static function findForGrupo(){
        $grupo_nombre = 'certificaciones';
        $posts = DB::table('posts')
            ->join('grupos', 'grupos.id', '=', 'posts.grupo_id')
            ->select('posts.*', 'grupos.nombre AS grupo_nombre')
            ->where('grupos.nombre', $grupo_nombre)
            ->where('posts.st', 1)
            ->where('posts.st2', 1);
        $posts = $posts->get();
        return $posts;
    } */
}
