<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class Img extends Base
{
    public function post(){
        return $this->belongsTo('App\Models\Post');
    }
    public function producto(){
        return $this->belongsTo('App\Models\Producto');
    }
    
    // public function updateOrden($id, $orden){
    //     return DB::table('imgs')->where('id', $id)->update(['orden' => $orden]);
    // }

    public function updateOrden($orden){
        $this->orden = $orden;
        return $this->update();
    }
}
