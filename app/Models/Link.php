<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class Link extends Base
{
    public function proyecto()
    {
        return $this->belongsTo('App\Models\Proyecto');
    }
}
