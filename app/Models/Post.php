<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Base;

class Post extends Base
{
    protected $table = 'posts';

    public function subcategoria(){
        return $this->belongsTo('App\Models\Subcategoria');
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Categoria');
    }

    public function grupo(){
        return $this->belongsTo('App\Models\Grupo');
    }

    public function imgs()
    {
    	return $this->hasMany('App\Models\Img');
    }

    /*public static function find_for_grupo($grupo_id){
        $posts = Post::all()->where('grupo_id', $grupo_id)->where('st', 1)->where('st2', 1);
        return $posts;
    }*/

    public static function find_for_grupo($grupo_nombre, $limit=0, $soloDestacados=false){
        $posts = DB::table('posts')
            ->join('grupos', 'grupos.id', '=', 'posts.grupo_id')
            ->select('posts.*', 'grupos.nombre AS grupo_nombre')
            ->where('grupos.nombre', $grupo_nombre)
            ->where('posts.st', 1)
            ->where('posts.st2', 1);
        if($limit > 0){
            $posts = $posts->take($limit);
        }
        if($soloDestacados){
            $posts = $posts->where('posts.destacado', 1);
        }
        if($grupo_nombre==='productos'){
            $posts = $posts->orderBy('order_destacados', 'asc');
        } else if($grupo_nombre==='home_banner'){
            $posts = $posts->orderBy('order', 'asc');
        }
        $posts = $posts->get();
        return $posts;
    }

    public static function ultimos($grupo_nombre, $limit){
        $posts = DB::table('posts')
            ->join('grupos', 'grupos.id', '=', 'posts.grupo_id')
            ->select('posts.*', 'grupos.nombre AS grupo_nombre')
            ->where('grupos.nombre', $grupo_nombre)
            ->where('posts.st', 1)
            ->where('posts.st2', 1)
            ->orderBy('id', 'desc')
            ->take($limit);
        $posts = $posts->get();
        return $posts;
    }

    public static function ultimosPaginados($grupo_nombre, $per_page){
        $posts = DB::table('posts')
            ->join('grupos', 'grupos.id', '=', 'posts.grupo_id')
            ->select('posts.*', 'grupos.nombre AS grupo_nombre')
            ->where('grupos.nombre', $grupo_nombre)
            ->where('posts.st', 1)
            ->where('posts.st2', 1)
            ->orderBy('id', 'desc')
            ->paginate($per_page);
        return $posts;
    }

    public static function find_posts_for_grupo_categoria($grupo_nombre, $categoria_nombre){
        if(strlen($categoria_nombre)>0){
            $posts = DB::table('posts')
                ->join('grupos', 'grupos.id', '=', 'posts.grupo_id')
                ->join('categorias', 'categorias.id', '=', 'posts.categoria_id')
                ->select('posts.*', 'grupos.nombre AS grupo_nombre', 'categorias.nombre AS categoria_nombre')
                ->where('categorias.nombre', $categoria_nombre)
                ->where('grupos.nombre', $grupo_nombre)
                ->where('posts.st', 1)
                ->where('posts.st2', 1)
                ->get();
            return $posts;
        } else {
            return $this->find_posts_for_grupo($grupo_nombre);
        }
    }

}
