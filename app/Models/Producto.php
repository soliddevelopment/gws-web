<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Base;

class Producto extends Base
{
    public function subcategoria(){
        return $this->belongsTo('App\Models\Subcategoria');
    }

    public function grupo(){
        return $this->belongsTo('App\Models\Grupo');
    }
    
    public function imgs()
    {
    	return $this->hasMany('App\Models\Img');
    }

}
