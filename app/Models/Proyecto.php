<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class Proyecto extends Base
{

    public function links()
    {
        return $this->hasMany('App\Models\Link');
    }

    public function servicios()
    {
        return $this->belongsToMany('App\Models\Servicio', 'proyecto_servicios');
        // return $this->belongsToMany('App\Models\Servicio', 'proyecto_servicios', 'proyecto_id', 'servicio_id');
    }
    
    public function imgs()
    {
    	return $this->hasMany('App\Models\ProyectoImg');
    }

    public static function getLista($destacados=false, $solohabilitados=true, $orderby=null, $limit=null)
    {
        $proyectos = Proyecto::st2();

        if($destacados) $proyectos = $proyectos->where('destacado', 1);
        if($solohabilitados) $proyectos = $proyectos->st();

        switch($orderby){
            // case "id":
            //     $proyectos = $proyectos->orderBy('id');
            //     break;
            case "fecha":
                $proyectos = $proyectos->orderBy('fecha');
                break;
            case "destacados":
                $proyectos = $proyectos->orderBy('destacado')->orderBy('orden', 'asc');
                break;
            default: 
                $proyectos = $proyectos->orderBy('orden', 'asc');
                break;
        }

        if($limit != null && is_numeric($limit) && $limit > 0){
            $proyectos = $proyectos->take($limit);
        }

        $proyectos = $proyectos->get();

        return $proyectos;
    }

    public static function updateOrden($id, $orden){
        $proyecto = Proyecto::find('id', $id);
        $proyecto->orden = $orden;
        $proyecto->update();
    }
}
