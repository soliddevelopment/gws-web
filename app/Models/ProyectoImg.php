<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class ProyectoImg extends Base
{
    public function proyecto(){
        return $this->belongsTo('App\Models\Proyecto');
    }
    
    // public function updateOrden($id, $orden){
    //     return DB::table('imgs')->where('id', $id)->update(['orden' => $orden]);
    // }

    public function updateOrden($orden){
        $this->orden = $orden;
        return $this->update();
    }
}
