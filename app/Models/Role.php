<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class Role extends Base
{
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_role', 'user_id', 'role_id');
    }
}
