<?php

namespace App\Models;

use DB;
use App\Models\Base;

class Servicio extends Base
{
    protected $table = 'servicios';

    public function proyectos()
    {
        return $this->belongsToMany('App\Models\Proyecto');
    }

    /* public static function ultimos($grupo_nombre, $limit){
        $posts = DB::table('posts')
            ->join('grupos', 'grupos.id', '=', 'posts.grupo_id')
            ->select('posts.*', 'grupos.nombre AS grupo_nombre')
            ->where('grupos.nombre', $grupo_nombre)
            ->where('posts.st', 1)
            ->where('posts.st2', 1)
            ->orderBy('id', 'desc')
            ->take($limit);
        $posts = $posts->get();
        return $posts;
    } */
    
}
