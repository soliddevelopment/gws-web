<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class Subcategoria extends Base
{
    protected $table = 'subcategorias'; // para casos en donde el plural no es solo agregar una S
    
    public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Categoria');
    }
}
