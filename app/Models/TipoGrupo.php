<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class TipoGrupo extends Base
{
    public $incrementing = false;
    public $timestamps = false;
    
    public function productos(){
    	return $this->hasMany('App\Models\Producto');
    }
}
