<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use App\Models\Base;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /* public function notifications()
    {
        return $this->hasMany('App\Models\Notification');
    } */

    protected $fillable = [
        'nombre', 'apellido', 'usuario', 'email', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        // si quiero ser especifico
        // return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
        return $this->belongsToMany('App\Models\Role', 'user_role');
    }

    public static function getUsuario($id){
        $usuario = User::find($id);
        $usuario->nombre_completo = $usuario->nombre.' '.$usuario->apellido;
        return $usuario;
    }

    public function hasAnyRole($roles)
    {
        if(is_array($roles)) {
            foreach($roles as $role){
                if($this->hasRole($role)){
                    return true;
                }
            }
        } else {
            if($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    public function mainRole(){
        if(count($this->roles()) > 0 ) {
            if($this->hasRole('Global')){
                return 'Global';
            } else if($this->hasRole('Admin')){
                return 'Admin';
            } else if($this->hasRole('Supervisor')){
                return 'Supervisor';
            } else if($this->hasRole('Basic')){
                return 'Basic';
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function hasRole($role){
        if($this->roles()->where('nombre', $role)->first()){
            return true;
        }
        return false;
    }
    public function hasRoles(){
        if(count($this->roles())>0){
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        if($this->roles()->where('nombre') === 'Admin') { 
            return true; 
        } else { 
            return false; 
        }
    }

    public function getRoles(){
        // return $this->roles();
        return $this->roles();
    }

    public function getUsers(){
        
        if($this->hasRole('Global')){ 
            // Si es un global, mostrar todos
            $usuarios = User::all()->sortBy('created_at');
        } else {
            // Si no es global, mostrar todos menos los que tienen admin
            $usuarios = User::whereHas('roles', function($q){
                $q->where('nombre', '!=', 'Global');
            })->get();
        }
        return $usuarios;
    }

    public function findForPassport($identifier) {
        return $this->orWhere('email', $identifier)->orWhere('usuario', $identifier)->first();
    }



}
