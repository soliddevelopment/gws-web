<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NuevoContacto extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($contacto)
    {
        $this->contacto = $contacto;
    }
    
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    public function toMail($notifiable)
    {
        $subject = 'Nuevo contacto de '.$this->contacto->nombre;
        $titulo = 'Recibiste un nuevo mensaje de contacto';
        $url_boton = route('admin.contactos.detalle', ['id'=>$this->contacto->id]);
        $txt_boton = 'Ir a contacto';
        $logo_url = asset('p/img/logo-email.png');
        
        $body = '';
        $body .= '<p><b>Name</b>: '.$this->contacto->nombre.'</p>';
        $body .= '<p><b>Email</b>: '.$this->contacto->email.'</p>';
        //$body .= '<p><b>Company</b>: '.$this->contacto->empresa.'</p>';
        $body .= '<p><b>Message</b>: '.$this->contacto->mensaje.'</p>';

        return (new MailMessage)
            ->from('noreply@gws.com.sv', 'GWS')
            ->subject($subject)
            ->markdown('publico.emails.generico2', [
                'titulo' => $titulo,
                'body' => $body,
                'txt_boton' => $txt_boton,
                'url_boton' => $url_boton,
                'logo_url' => $logo_url,
                'color' => 'black'
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
