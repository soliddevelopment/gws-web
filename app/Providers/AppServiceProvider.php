<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Validator;
use View;
use Auth;
use Route;

use App\Models\User;
use App\Models\Grupo;
use App\Models\Config;

use App\Models\Custom\Galeria;
use App\Models\Custom\Location;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Para arreglar error de migrations del largo de cada campo
        Schema::defaultStringLength(255);
    
        Validator::extend(
            'recaptcha',
            'App\\Validators\\ReCaptcha@validate'
        );

        // CODIGO INTERMEDIO PARA TODOS LOS QUE VAYAN A USAR EL LAYOUT ADMIN.MAIN
        app('view')->composer('*', function ($view) {

            $grupos = array();
            $count_prod = 0;
            $count_blog = 0;
            $route_name = '';
            $controller = '';
            $configs = array();
            $comercios = null;
            $locations = array();

            $configs = Config::all();

            // Obtengo nombre de controller para enviarlo y verlo en nav
            $route = app('request')->route();
            
            if($route != null){
                $action = app('request')->route()->getAction();
                $controller = class_basename($action['controller']);
                list($controller, $action) = explode('@', $controller);
                $view->with(compact('controller', 'action'));

                // Current route
                // $route = Route::current();
                $route_name = Route::currentRouteName();
                // $route_action = Route::currentRouteAction();

                // Data de usuario
                $user = Auth::user();
                if($user){
                    $user_id = $user->id;
                    $signed_in = Auth::check();
                    $usuario = User::find($user_id);
                } else {
                    $user_id = null;
                    $signed_in = false;
                    $usuario = null;
                }
                $view->with('signed_in', $signed_in);
                $view->with('usuario', $usuario);

                // Data de grupos
                $count_blog = 0; $count_prod = 0;
                $grupos = Grupo::all()->where('st', 1)->where('st2', 1);
                $comercios = Galeria::find_comercios();
                foreach($grupos as $item){
                    if($item->tipo_id === 'BLOG') $count_blog++;
                    if($item->tipo_id === 'PROD') $count_prod++;
                }

                $locations = Location::all();
            }

            $view->with('locations', $locations);
            $view->with('comercios', $comercios);
            $view->with('grupos_catalogo', $grupos);
            $view->with('count_prod', $count_prod);
            $view->with('count_blog', $count_blog);
            $view->with('route_name', $route_name);
            $view->with('configs', $configs);
            // $view->with('route', $route);
            // $view->with('route_action', $route_action);

            
            /*$view = View::make('admin.proyectos.edit', [
                'title' => $title,
                'proyecto' => $proyecto,
                'lista_servicios' => $lista_servicios
            ]);
            return $view;*/
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            // $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            // $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }
    }
}
