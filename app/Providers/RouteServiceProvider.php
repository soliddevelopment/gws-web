<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
        $this->mapPublicRoutes();
        $this->mapApiRoutes();
        $this->mapAdminRoutes();
        $this->mapAdminCustomRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    /*protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }*/
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }
    protected function mapPublicRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace.'\Publico')
             ->group(base_path('routes/publico.php'));
    }
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->namespace($this->namespace.'\Api')
             ->group(base_path('routes/api.php'));
    }
    protected function mapAdminRoutes()
    {
        Route::middleware('admin')
             ->namespace($this->namespace.'\Admin')
             ->group(base_path('routes/admin.php'));
    }
    protected function mapAdminCustomRoutes()
    {
        Route::middleware('admin')
             ->namespace($this->namespace.'\Admin')
             ->group(base_path('routes/admin_custom.php'));
    }
}
