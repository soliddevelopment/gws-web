<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('grupo_id')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned()->nullable()->default(null);
			$table->integer('subcategoria_id')->unsigned()->nullable();
			$table->string('img')->nullable();
            $table->string('img2')->nullable();
            $table->string('img3')->nullable();
			$table->string('titulo')->default('')->nullable();
			$table->string('slug')->nullable()->default(null);
			$table->string('titulo_corto')->nullable();
			$table->text('bajada')->nullable();
            $table->string('subida')->nullable()->default(null);
			$table->text('body', 65535)->nullable();
			$table->string('url_externo')->nullable();
			$table->text('simple_tags')->nullable();
			$table->decimal('precio')->nullable();
			$table->boolean('destacado')->default(false);
			$table->boolean('st')->default(1);
			$table->boolean('st2')->default(1);
			$table->timestamp('fecha_custom')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
