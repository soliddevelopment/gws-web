<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoGruposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipo_grupos', function(Blueprint $table)
		{
			$table->string('id', 30)->primary();
			$table->string('titulo');
			$table->text('descripcion', 65535);
			$table->boolean('st')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipo_grupos');
	}

}
