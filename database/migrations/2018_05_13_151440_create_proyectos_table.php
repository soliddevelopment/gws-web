<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectosTable extends Migration
{
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->increments('id');
			$table->boolean('destacado')->default(0);
            $table->string('cliente')->nullable();
            $table->string('titulo');
            $table->string('slug');
            $table->string('bajada')->nullable();
			$table->text('descripcion', 65535)->nullable();
			$table->string('img_logo')->nullable();
			$table->string('img_thumb')->nullable();
            $table->string('img_intro')->nullable();
            $table->date('fecha')->nullable();
            $table->integer('orden');
			$table->boolean('st')->default(1);
            $table->boolean('st2')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
