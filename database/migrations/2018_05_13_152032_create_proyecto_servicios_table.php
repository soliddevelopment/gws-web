<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoServiciosTable extends Migration
{
    public function up()
    {
        Schema::create('proyecto_servicios', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('proyecto_id');
			$table->integer('servicio_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('proyecto_servicios');
    }
}
