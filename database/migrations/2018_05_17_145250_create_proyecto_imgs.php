<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoImgs extends Migration
{
    public function up()
    {
        Schema::create('proyecto_imgs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proyecto_id')->unsigned()->nullable();
            $table->string('titulo')->default('');
            $table->string('descripcion')->default('');
            $table->string('img')->nullable();
            $table->tinyInteger('st')->default(1);
            $table->tinyInteger('st2')->default(1);
            $table->integer('orden')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('proyecto_imgs');
    }
}
