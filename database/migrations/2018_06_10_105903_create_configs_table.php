<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->string('slug');
            $table->string('titulo')->nullable()->default(null);
            $table->text('descripcion')->nullable()->default(null);
            
            $table->string('valor_img')->nullable()->default(null);
            $table->string('valor_str')->nullable()->default(null);
            $table->integer('valor_int')->nullable()->default(null);
            $table->tinyInteger('valor_bool')->default(0);
            
            $table->tinyInteger('only_global')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
