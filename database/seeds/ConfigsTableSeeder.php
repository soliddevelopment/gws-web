<?php

use Illuminate\Database\Seeder;
use App\Models\Config;

class ConfigsTableSeeder extends Seeder
{
    public function run()
    {

        // GLOBALES, que no quiero que los demas puedan editar, solo yo
        $config = new Config();
        $config->only_global = true;
        $config->tipo = 'BOOL';
        $config->slug = 'proyectos';
        $config->titulo = 'Proyectos';
        $config->descripcion = 'Habilitar o deshabilitar sección de proyectos';
        $config->valor_bool = false;
        $config->save();

        $config = new Config();
        $config->only_global = true;
        $config->tipo = 'BOOL';
        $config->slug = 'blogs';
        $config->titulo = 'Contenido blogs';
        $config->descripcion = 'Habilitar o deshabilitar blogs en general';
        $config->valor_bool = true;
        $config->save();

        $config = new Config();
        $config->only_global = true;
        $config->tipo = 'BOOL';
        $config->slug = 'contactos';
        $config->titulo = 'Contactos';
        $config->descripcion = 'Habilitar o deshabilitar contactos en general';
        $config->valor_bool = true;
        $config->save();

        
    }
}
