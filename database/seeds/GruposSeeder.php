<?php

use Illuminate\Database\Seeder;
use App\Models\Grupo;

class GruposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grupo = new Grupo();
        $grupo->tipo_id = 'BLOG';
        $grupo->nombre = 'home_banner';
        $grupo->titulo = 'Home banner';
        $grupo->st = 1;
        $grupo->st2 = 1;
        $grupo->save();
        
        $grupo = new Grupo();
        $grupo->tipo_id = 'BLOG';
        $grupo->nombre = 'comercios';
        $grupo->titulo = 'Comercios';
        $grupo->st = 1;
        $grupo->st2 = 1;
        $grupo->save();
        
        $grupo = new Grupo();
        $grupo->tipo_id = 'BLOG';
        $grupo->nombre = 'productos';
        $grupo->titulo = 'Productos';
        $grupo->st = 1;
        $grupo->st2 = 1;
        $grupo->save();
        
        $grupo = new Grupo();
        $grupo->tipo_id = 'BLOG';
        $grupo->nombre = 'galerias';
        $grupo->titulo = 'Galerías';
        $grupo->st = 1;
        $grupo->st2 = 1;
        $grupo->save();
    }
}
