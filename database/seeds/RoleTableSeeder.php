<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->nombre = 'Admin';
        $role_user->descripcion = 'Permisos totales, administrar usuarios y roles';
        $role_user->save();

        $role_user = new Role();
        $role_user->nombre = 'Supervisor';
        $role_user->descripcion = '';
        $role_user->save();

        $role_user = new Role();
        $role_user->nombre = 'Basic';
        $role_user->descripcion = '';
        $role_user->save();

        $role_user = new Role();
        $role_user->nombre = 'Global';
        $role_user->descripcion = 'PERMISOS ABSOLUTOS';
        $role_user->save();
    }
}
