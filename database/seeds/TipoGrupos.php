<?php

use Illuminate\Database\Seeder;
use App\Models\TipoGrupo;

class TipoGrupos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo = new TipoGrupo();
        $tipo->id = 'BLOG';
        $tipo->titulo = 'Blog';
        $tipo->descripcion = 'Blogs, proyectos, casi todo...';
        $tipo->st = 1;
        $tipo->save();

        $tipo = new TipoGrupo();
        $tipo->id = 'MISC';
        $tipo->titulo = 'Varios';
        $tipo->descripcion = 'Textos varios...';
        $tipo->st = 1;
        $tipo->save();
    }
}
