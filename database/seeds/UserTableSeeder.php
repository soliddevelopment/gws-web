<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /** *
     * Run the database seeds.d
     *
     * @return void
     */
    public function run()
    {	
        $role_global = Role::where('nombre', 'Global')->first();
        $role_admin = Role::where('nombre', 'Admin')->first();
        // $role_basic = Role::where('nombre', 'Basic')->first();

        $user = new User();
        $user->nombre = 'Jose Carlos';
        $user->usuario = 'heymans';
        $user->apellido = 'Heymans';
        $user->email = 'jcheymans87@gmail.com';
        $user->password = bcrypt('asdf123');
        $user->save();
        $user->roles()->attach($role_global);
        
        $user = new User();
        $user->nombre = 'Luis';
        $user->apellido = 'Alvarenga';
        $user->usuario = 'luis';
        $user->email = 'luis.alvarenga@gws.com.sv';
        $user->password = bcrypt('GwsJul2018');
        $user->save();
        $user->roles()->attach($role_admin);
        
        $user = new User();
        $user->nombre = 'Carlos';
        $user->apellido = 'Santamaría';
        $user->usuario = 'carlos';
        $user->email = 'carlos.santamaria@gws.com.sv';
        $user->password = bcrypt('GwsJul2018');
        $user->save();
        $user->roles()->attach($role_admin);


    }
}
