var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function(){
    return gulp.src([
        './resources/assets/admin/sass/*.scss',
        './resources/assets/public/css/sass/*.scss'
    ], { base: "./" })
        .pipe(sass())
        .pipe(gulp.dest('.')) // transpile sass to same directory as original .scss file?
        .once('finish', function () {
            console.log('notify clients of css change');
        })
        ;
});

gulp.task('watch', function(){
    gulp.watch([
        './resources/assets/admin/sass/*.scss',
        './resources/assets/public/css/sass/*.scss'
    ], ['sass']);
});

gulp.task('default', ['sass', 'watch']);