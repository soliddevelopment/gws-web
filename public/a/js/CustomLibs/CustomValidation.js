﻿

function getFileExtension(fileName) {
    var extension = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
    if (extension !== undefined) {
        return extension[0];
    }
    return extension;
}



jQuery.validator.addMethod("isfuturedate", function (value, element, params) {
    try {
        var fechaComparar = $.datepicker.parseDate('dd/mm/yy', value);
        if (fechaComparar == null) return true;
        var fechaActual = new Date();
        fechaActual=fechaActual.setHours(0, 0, 0, 0);
        if (fechaActual > fechaComparar) {
            return false;
        }
        return true;
    } catch (err) {
        return true;
    }
});

jQuery.validator.unobtrusive.adapters.add("isfuturedate", function (options) {
   options.rules["isfuturedate"] = true;
    if (options.message) {
        options.messages["isfuturedate"] = options.message;
    }
});


//Comparar 2 fechasW
jQuery.validator.addMethod('isdateafter', function(value, element, params) {
    var valorComparar = $("#" + params.propertytested).val();
    if (valorComparar === undefined || valorComparar === null) return true;
    try {
        var fechaValidar = $.datepicker.parseDate('dd/mm/yy', value);
        var fechaComparar = $.datepicker.parseDate('dd/mm/yy', valorComparar);
        if (fechaComparar === null || fechaValidar == null) return true;
        return fechaValidar > fechaComparar;
    } catch (err) {
        return true;
    }
});

jQuery.validator.unobtrusive.adapters.add('isdateafter', ["propertytested","allowequaldates"], function(options) {
        var params = {
            propertytested: options.params.propertytested,
            allowequaldates: options.params.allowequaldates
        };
        options.rules['isdateafter'] = params;
        options.messages['isdateafter'] = options.message;
 });

//Configuro el lado del cliente del vsalidador custom de extensiones de archivos

jQuery.validator.addMethod("fileextensions", function (value, element, param) {
    if (value !== "" && value.length > 0) {
        var extension = getFileExtension(value);
        var validExtension = $.inArray(extension, param.fileextensions) !== -1;
        return validExtension;
    }
    return true;
});

jQuery.validator.unobtrusive.adapters.add('fileextensions', ['fileextensions'], function (options) {
    var params = {
        fileextensions: options.params.fileextensions.split(',')
    };

    options.rules['fileextensions'] = params;
    if (options.message) {
        options.messages['fileextensions'] = options.message;
    }
});

// El javascript para el validador de la edad a partir de la fecha de nacimiento
$.validator.addMethod(
    'validateage',
    function (value, element, params) {
        var datosFechaMinima = params.minumumdate.split('/');
        var datosFechaMaxima = params.maximumdate.split('/');
        var datosFechaNacimiento = value.split('/');

        var fechaMinima = new Date(datosFechaMinima[2], parseInt(datosFechaMinima[1]) - 1, datosFechaMinima[0]);
        var fechaMaxima = new Date(datosFechaMaxima[2], parseInt(datosFechaMaxima[1]) - 1, datosFechaMinima[0]);
        var fechaActual = new Date(datosFechaNacimiento[2], parseInt(datosFechaNacimiento[1]) - 1, datosFechaNacimiento[0]);
        return fechaActual >= fechaMinima && fechaActual <= fechaMaxima;
    });

$.validator.unobtrusive.adapters.add(
    'validateage', ['minumumdate', 'maximumdate'], function (options) {
        var params = {
            minumumdate: options.params.minumumdate,
            maximumdate: options.params.maximumdate
        };
        options.rules['validateage'] = params;
        options.messages['validateage'] = options.message;
    });

$.validator.unobtrusive.adapters.add(
    'minElements', ['minimoElements'], function (options) {
        var params = {
            minimoElements: options.params.minimoElements
        };
        options.rules['minElements'] = params;
        options.messages['minElements'] = options.message;
    });

$.validator.addMethod(
    'minElements',
    function (value, element, params) {
        alert(element);
        return false;
    });


$.validator.addMethod('date', function (value, element) {
    if (this.optional(element)) {
        return true;
    }
    var valid = true;
    try {
        $.datepicker.parseDate('dd/mm/yy', value);
    }
    catch (err) {
        valid = false;
    }
    return valid;
});


$(document).ready(function () {

    $('.input-validation-error').parents('.form-group').addClass('has-error');
    $('.field-validation-error').addClass('text-danger');


    var form = $("form");
    var formData = null;
    if (form[0] !== undefined) {
        formData = $.data(form[0]);
    }

    if (formData !== null && formData.validator) {

        var settings = formData.validator.settings, oldErrorPlacement = settings.errorPlacement,
            oldSuccess = settings.success;

        settings.errorPlacement = function (label, element) {

            // Call old handler so it can update the HTML
            oldErrorPlacement(label, element);

            // Add Bootstrap classes to newly added elements
            label.parents('.form-group')
                .addClass('has-error has-feedback')
                .append("<span class='glyphicon glyphicon-remove form-control-feedback' aria-hidden='true'></span>");
            label.addClass('text-danger');
            //$(form).find(".validation-summary-errors").addClass("bg-danger");


            settings.success = function (label) {
                // Remove error class from <div class="form-group">, but don't worry about
                // validation error messages as the plugin is going to remove it anyway
                label.parents('.form-group').removeClass('has-error has-feedback');
                label.parents(".form-group").find("span.glyphicon-remove").remove();
                // Call old handler to do rest of the work
                oldSuccess(label);
            };
        }
    }
});