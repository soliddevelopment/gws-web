﻿/// <reference path="../dropzone.js" />
Dropzone.autoDiscover = false;


$(document).ready(function() {

    if ($("form#upload-widget").length === 0) return;

    var actualizarContenedorImagenes = function(html) {
        $("#ContenedorImagenes").html(html);
    };

    var htmlPreview = $("div#previewDropZone").html();

    

    var dropzoneGaleria = $("form#upload-widget")
        .dropzone({
            paramName: "Imagen",
            uploadMultiple: false,
            previewTemplate: htmlPreview,
            parallelUploads: 15,
            acceptedFiles: "image/jpeg,image/pjpeg,image/png",
            autoProcessQueue: false,
            maxFiles: 100,
            init: function() {
                //aqui agregar los handlers de los diferentes eventos
                var myDropZonse = this;
                myDropZonse.on("sending", function (file, xhr, formdata) {
                    var inputCaption = $(file.previewElement).find("input[name=Caption]").val();
                    formdata.append("caption", inputCaption);
                });
                myDropZonse.on("queuecomplete",function() {
                    myDropZonse.removeAllFiles();
                    //cierro el modal de las fotos
                    $("#btnguardar").removeAttr('disabled');
                    $("#modal_nueva_foto").modal("hide");
                    toastr.success('Las fotos fueron guardadas exitosamente');
                    hideLoading();
                });

                //Evento que se ejecuta con cada imagen subida con exito
                myDropZonse.on("success",function(file,respServer) {
                    actualizarContenedorImagenes(respServer.Html);
                });
            }
        })[0].dropzone;


    $(document).on("click","#btnguardar",function(e) {
        e.preventDefault();
        showLoading();
        $(this).attr('disabled', 'disabled');
        dropzoneGaleria.processQueue();
    });

    $("#ContenedorImagenes").on("click", "span.eliminarImagen", function (e) {
        e.preventDefault();
        e.stopPropagation();
        var url = $(this).data("url");
        $(this).parents("li").remove();
        $.post(url,null,function(resp) {
            actualizarContenedorImagenes(resp.Html);
            toastr.success('Se elimino la imagen con exito', 'Operacion Exitosa');
        });
    });


    $("#ContenedorImagenes").on("click", ".lista-fotos li .guardar", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var url = $(this).data("url");
        var idAlbum = $(this).data("idalbum");
        var textoCaption = $(this).parents("li").find("input[name=caption]").val();
        
        $.post(url, { id: id, idAlbum: idAlbum, caption: textoCaption }, function (resp) {
            actualizarContenedorImagenes(resp.Html);
            toastr.success('Se actualizo el caption con exito', 'Operacion Exitosa');
        });

    });

});