﻿/// <reference path="../jquery-1.10.2.js" />
/// <reference path="../bootstrap.js" />
/// <reference path="../jquery.form.min.js" />






$(document).ready(function() {

    //Creo una coockie con el timezone del la computadora del cliente
    function cookieExists(name) {
        var nameToFind = name + "=";
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            if (cookies[i].trim().indexOf(nameToFind) === 0) return true;
        }
        return false;
    }


    if (!cookieExists("_timeZoneOffset")) {
        var now = new Date();
        var timeZoneOffset = -now.getTimezoneOffset(); // in minutes
        now.setTime(now.getTime() + 10 * 24 * 60 * 60 * 1000); // keep it for 10 days
        document.cookie = "_timeZoneOffset=" +
            timeZoneOffset.toString() +
            ";expires=" +
            now.toGMTString() +
            ";path=/;" +
            document.cookie;

    };


    $(document).on("change", "select[data-ajaxcmd]", function () {
        var url = $(this).data("url");
        var cmdAfectar = $(this).data("cmdresultado");
        var datavalue = $(this).val();
        var $select = $("#" + cmdAfectar).empty();
        var mensajeDefault = $select.data("parentempty");
        var callBackDefault = function (objRespuesta) {
            for (var i = 0; i < objRespuesta.length; i++) {
                var objActual = objRespuesta[i];
                var $option = $("<option></option>");
                $option.attr("value", objActual.Valor).text(objActual.Nombre);
                $select.append($option);
            }
        };

       $select.append("<option value=''>" + mensajeDefault + "</option>");
        if (datavalue !== "") {
            $.get(url, { id: datavalue }, callBackDefault);
        }
    });
    

    $(document).on("change", "select.cmdpost", function(e) {
        var $form = $(this).parents("form");
        $form.submit();
    });

    $(document).on("submit", "form.ajaxform", function (e) {
        e.preventDefault();
        var $form = $(this);
        $form.find("button[type=submit]").addClass("btn-loading");
        var $parent = $(this).parent();
        if ($form.valid()) {
            var datos = $form.serialize();
            var urlpost = $form.attr("action");
            $.post(urlpost, datos, function(resp) {
                if (resp.Exitoso) window.location = resp.Redirect;
                else {
                    $parent.html(resp.Html);
                    $.validator.unobtrusive.parse("form");
                }
                $form.find("button[type=submit]").removeClass("btn-loading");
            }).fail(function () {
                alert("Hubo un error en el servidor");
            });
        }
    });

    $(document).on("submit", "form.ajaxformultipart", function (e) {
        e.preventDefault();
        var $form = $(this);
        $form.find("button[type=submit]").addClass("btn-loading");
        if ($form.valid()) {
            $form.ajaxSubmit({
                dataType: "json",
                beforeSend: function() {
                    updateProgressBar(0);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    updateProgressBar(percentComplete);
                    
                },
                error: function (err) {
                    updateProgressBar(0);
                    $form.resetForm();
                    alert("Hubo un error en el servidor");
                },
                success: function (resp) {
                    if (resp.Exitoso) window.location = resp.Redirect;
                    else {
                        $parent.html(resp.Html);
                        $.validator.unobtrusive.parse("form");
                        $form.resetForm();
                        updateProgressBar(0);
                    }
                    $form.find("button[type=submit]").removeClass("btn-loading");
                }
            });
        }
    });
  

    $(document).on("click", "a.ajaxedit", function(e) {
        e.preventDefault();
        var urlform = $(this).attr("href");
        var idmodal = $(this).data("modal");
        $.get(urlform, function (htmlform) {
            var $modal = $("#" + idmodal);
            $modal.find("div.modal-content").html(htmlform);
            $.validator.unobtrusive.parse("form");
            $modal.modal("show");
        });
    });

});