/**
* Theme: Minton Admin Template
* Author: Coderthemes
* Module/App: Core js
*/

//portlets
!function($) {
    "use strict";

    /**
    Portlet Widget
    */
    var Portlet = function() {
        this.$body = $("body"),
        this.$portletIdentifier = ".portlet",
        this.$portletCloser = '.portlet a[data-toggle="remove"]',
        this.$portletRefresher = '.portlet a[data-toggle="reload"]'
    };

    //on init
    Portlet.prototype.init = function() {
        // Panel closest
        var $this = this;
        $(document).on("click",this.$portletCloser, function (ev) {
            ev.preventDefault();
            var $portlet = $(this).closest($this.$portletIdentifier);
                var $portlet_parent = $portlet.parent();
            $portlet.remove();
            if ($portlet_parent.children().length == 0) {
                $portlet_parent.remove();
            }
        });

        // Panel Reload
        $(document).on("click",this.$portletRefresher, function (ev) {
            ev.preventDefault();
            var $portlet = $(this).closest($this.$portletIdentifier);
            // This is just a simulation, nothing is going to be reloaded
            $portlet.append('<div class="panel-disabled"><div class="loader-1"></div></div>');
            var $pd = $portlet.find('.panel-disabled');
            setTimeout(function () {
                $pd.fadeOut('fast', function () {
                    $pd.remove();
                });
            }, 500 + 300 * (Math.random() * 5));
        });
    },
    //
    $.Portlet = new Portlet, $.Portlet.Constructor = Portlet

}(window.jQuery),

/**
 * Notifications
 */
function($) {
    "use strict";

    var Notification = function() {};

    //simple notificaiton
    Notification.prototype.notify = function(style,position, title, text) {
        var icon = 'fa fa-adjust';
        if(style == "error"){
            icon = "fa fa-exclamation";
        }else if(style == "warning"){
            icon = "fa fa-warning";
        }else if(style == "success"){
            icon = "fa fa-check";
        }else if(style == "custom"){
            icon = "md md-album";
        }else if(style == "info"){
            icon = "fa fa-question";
        }else{
            icon = "fa fa-adjust";
        }
        $.notify({
            title: title,
            text: text,
            image: "<i class='"+icon+"'></i>"
        }, {
            style: 'metro',
            className: style,
            globalPosition:position,
            showAnimation: "show",
            showDuration: 0,
            hideDuration: 0,
            autoHide: true,
            clickToHide: true
        });
    },

    //auto hide notification
    Notification.prototype.autoHideNotify = function (style,position, title, text) {
        var icon = "fa fa-adjust";
        if(style == "error"){
            icon = "fa fa-exclamation";
        }else if(style == "warning"){
            icon = "fa fa-warning";
        }else if(style == "success"){
            icon = "fa fa-check";
        }else if(style == "custom"){
            icon = "md md-album";
        }else if(style == "info"){
            icon = "fa fa-question";
        }else{
            icon = "fa fa-adjust";
        }
        $.notify({
            title: title,
            text: text,
            image: "<i class='"+icon+"'></i>"
        }, {
            style: 'metro',
            className: style,
            globalPosition:position,
            showAnimation: "show",
            showDuration: 0,
            hideDuration: 0,
            autoHideDelay: 5000,
            autoHide: true,
            clickToHide: true
        });
    },
    //confirmation notification
    Notification.prototype.confirm = function(style,position, title) {
        var icon = "fa fa-adjust";
        if(style == "error"){
            icon = "fa fa-exclamation";
        }else if(style == "warning"){
            icon = "fa fa-warning";
        }else if(style == "success"){
            icon = "fa fa-check";
        }else if(style == "custom"){
            icon = "md md-album";
        }else if(style == "info"){
            icon = "fa fa-question";
        }else{
            icon = "fa fa-adjust";
        }
        $.notify({
            title: title,
            text: 'Are you sure you want to do nothing?<div class="clearfix"></div><br><a class="btn btn-sm btn-white yes">Yes</a> <a class="btn btn-sm btn-danger no">No</a>',
            image: "<i class='"+icon+"'></i>"
        }, {
            style: 'metro',
            className: style,
            globalPosition:position,
            showAnimation: "show",
            showDuration: 0,
            hideDuration: 0,
            autoHide: false,
            clickToHide: false
        });
        //listen for click events from this style
        $(document).on('click', '.notifyjs-metro-base .no', function() {
          //programmatically trigger propogating hide event
          $(this).trigger('notify-hide');
        });
        $(document).on('click', '.notifyjs-metro-base .yes', function() {
          //show button text
          alert($(this).text() + " clicked!");
          //hide notification
          $(this).trigger('notify-hide');
        });
    },
    //init - examples
    Notification.prototype.init = function() {

    },
    //init
    $.Notification = new Notification, $.Notification.Constructor = Notification
}(window.jQuery),

/**
 * Components
 */
function($) {
    "use strict";

    var Components = function() {};

    //initializing tooltip
    Components.prototype.initTooltipPlugin = function() {
        $.fn.tooltip && $('[data-toggle="tooltip"]').tooltip()
    },

    //initializing popover
    Components.prototype.initPopoverPlugin = function() {
        $.fn.popover && $('[data-toggle="popover"]').popover()
    },

    //initializing custom modal
    Components.prototype.initCustomModalPlugin = function() {
        $('[data-plugin="custommodal"]').on('click', function(e) {
        	Custombox.open({
                target: $(this).attr("href"),
                effect: $(this).attr("data-animation"),
                overlaySpeed: $(this).attr("data-overlaySpeed"),
                overlayColor: $(this).attr("data-overlayColor")
            });
        	e.preventDefault();
        });
    },

    //initializing nicescroll
    Components.prototype.initNiceScrollPlugin = function() {
        //You can change the color of scroll bar here
        $.fn.niceScroll &&  $(".nicescroll").niceScroll({ cursorcolor: '#98a6ad',cursorwidth:'6px', cursorborderradius: '5px'});
    },

    //range slider
    Components.prototype.initRangeSlider = function() {
        $.fn.slider && $('[data-plugin="range-slider"]').slider({});
    },

    /* -------------
     * Form related controls
     */
    //switch
    Components.prototype.initSwitchery = function() {
        $('[data-plugin="switchery"]').each(function (idx, obj) {
            new Switchery($(this)[0], $(this).data());
        });
    },
    //multiselect
    Components.prototype.initMultiSelect = function() {
        if($('[data-plugin="multiselect"]').length > 0)
            $('[data-plugin="multiselect"]').multiSelect($(this).data());
    },

     /* -------------
     * small charts related widgets
     */
     //peity charts
     Components.prototype.initPeityCharts = function() {
        $('[data-plugin="peity-pie"]').each(function(idx, obj) {
            var colors = $(this).attr('data-colors')?$(this).attr('data-colors').split(","):[];
            var width = $(this).attr('data-width')?$(this).attr('data-width'):20; //default is 20
            var height = $(this).attr('data-height')?$(this).attr('data-height'):20; //default is 20
            $(this).peity("pie", {
                fill: colors,
                width: width,
                height: height
            });
        });
        //donut
         $('[data-plugin="peity-donut"]').each(function(idx, obj) {
            var colors = $(this).attr('data-colors')?$(this).attr('data-colors').split(","):[];
            var width = $(this).attr('data-width')?$(this).attr('data-width'):20; //default is 20
            var height = $(this).attr('data-height')?$(this).attr('data-height'):20; //default is 20
            $(this).peity("donut", {
                fill: colors,
                width: width,
                height: height
            });
        });

         $('[data-plugin="peity-donut-alt"]').each(function(idx, obj) {
            $(this).peity("donut");
        });

         // line
         $('[data-plugin="peity-line"]').each(function(idx, obj) {
            $(this).peity("line", $(this).data());
         });

         // bar
         $('[data-plugin="peity-bar"]').each(function(idx, obj) {
            var colors = $(this).attr('data-colors')?$(this).attr('data-colors').split(","):[];
            var width = $(this).attr('data-width')?$(this).attr('data-width'):20; //default is 20
            var height = $(this).attr('data-height')?$(this).attr('data-height'):20; //default is 20
            $(this).peity("bar", {
                fill: colors,
                width: width,
                height: height
            });
         });
     },



    //initilizing
    Components.prototype.init = function() {
        var $this = this;
        this.initTooltipPlugin(),
        this.initPopoverPlugin(),
        this.initNiceScrollPlugin(),
        this.initCustomModalPlugin(),
        this.initRangeSlider(),
        this.initSwitchery(),
        this.initMultiSelect(),
        this.initPeityCharts(),
        //creating portles
        $.Portlet.init();
    },

    $.Components = new Components, $.Components.Constructor = Components

}(window.jQuery),
    //initializing main application module
function($) {
    "use strict";
    $.Components.init();
}(window.jQuery);





/**
* Theme: Minton Admin Template
* Author: Coderthemes
* Module/App: Main Js
*/


!function($) {
    "use strict";

    var Sidemenu = function() {
        this.$body = $("body"),
        this.$openLeftBtn = $(".open-left"),
        this.$menuItem = $("#sidebar-menu a")
    };
    Sidemenu.prototype.openLeftBar = function() {
      $("#wrapper").toggleClass("enlarged");
      $("#wrapper").addClass("forced");

      if($("#wrapper").hasClass("enlarged") && $("body").hasClass("fixed-left")) {
        $("body").removeClass("fixed-left").addClass("fixed-left-void");
      } else if(!$("#wrapper").hasClass("enlarged") && $("body").hasClass("fixed-left-void")) {
        $("body").removeClass("fixed-left-void").addClass("fixed-left");
      }
      
      if($("#wrapper").hasClass("enlarged")) {
        $(".left ul").removeAttr("style");
      } else {
        $(".subdrop").siblings("ul:first").show();
      }
      
      toggle_slimscroll(".slimscrollleft");
      $("body").trigger("resize");
    },
    //menu item click
    Sidemenu.prototype.menuItemClick = function(e) {
       if(!$("#wrapper").hasClass("enlarged")){
        if($(this).parent().hasClass("has_sub")) {

        }   
        if(!$(this).hasClass("subdrop")) {
          // hide any open menus and remove all other classes

          // HEYYY CUSTOMM: HEY YO OCULTE ESTO
          // $("ul",$(this).parents("ul:first")).slideUp(350);

          $("a",$(this).parents("ul:first")).removeClass("subdrop");
          $("#sidebar-menu .pull-right i").removeClass("md-remove").addClass("md-add");
          
          // open our new menu and add the open class
          $(this).next("ul").slideDown(350);
          $(this).addClass("subdrop");
          $(".pull-right i",$(this).parents(".has_sub:last")).removeClass("md-add").addClass("md-remove");
          $(".pull-right i",$(this).siblings("ul")).removeClass("md-remove").addClass("md-add");
        }else if($(this).hasClass("subdrop")) {
          $(this).removeClass("subdrop");
          $(this).next("ul").slideUp(350);
          $(".pull-right i",$(this).parent()).removeClass("md-remove").addClass("md-add");
        }
      } 
    },

    //init sidemenu
    Sidemenu.prototype.init = function() {
      var $this  = this;

      var ua = navigator.userAgent,
        event = (ua.match(/iP/i)) ? "touchstart" : "click";
      
      //bind on click
      this.$openLeftBtn.on(event, function(e) {
        e.stopPropagation();
        $this.openLeftBar();
      });

      // LEFT SIDE MAIN NAVIGATION
      $this.$menuItem.on(event, $this.menuItemClick);

      // NAVIGATION HIGHLIGHT & OPEN PARENT
      // HEYYY CUSTOM: hey yo oculte esto!!!
      // $("#sidebar-menu ul li.has_sub a.active").parents("li:last").children("a:first").addClass("active").trigger("click");
    },

    //init Sidemenu
    $.Sidemenu = new Sidemenu, $.Sidemenu.Constructor = Sidemenu
    
}(window.jQuery),


function($) {
    "use strict";

    var FullScreen = function() {
        this.$body = $("body"),
        this.$fullscreenBtn = $("#btn-fullscreen")
    };

    //turn on full screen
    // Thanks to http://davidwalsh.name/fullscreen
    FullScreen.prototype.launchFullscreen  = function(element) {
      if(element.requestFullscreen) {
        element.requestFullscreen();
      } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
      } else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
      } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
      }
    },
    FullScreen.prototype.exitFullscreen = function() {
      if(document.exitFullscreen) {
        document.exitFullscreen();
      } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    },
    //toggle screen
    FullScreen.prototype.toggle_fullscreen  = function() {
      var $this = this;
      var fullscreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled;
      if(fullscreenEnabled) {
        if(!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {
          $this.launchFullscreen(document.documentElement);
        } else{
          $this.exitFullscreen();
        }
      }
    },
    //init sidemenu
    FullScreen.prototype.init = function() {
      var $this  = this;
      //bind
      $this.$fullscreenBtn.on('click', function() {
        $this.toggle_fullscreen();
      });
    },
     //init FullScreen
    $.FullScreen = new FullScreen, $.FullScreen.Constructor = FullScreen
    
}(window.jQuery),



//main app module
 function($) {
    "use strict";
    
    var App = function() {
        this.VERSION = "1.7.0",
        this.AUTHOR = "Coderthemes", 
        this.SUPPORT = "coderthemes@gmail.com", 
        this.pageScrollElement = "html, body", 
        this.$body = $("body")
    };
    
     //on doc load
    App.prototype.onDocReady = function(e) {
      FastClick.attach(document.body);
      resizefunc.push("initscrolls");
      resizefunc.push("changeptype");

      $('.animate-number').each(function(){
        $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-duration"))); 
      });
    
      //RUN RESIZE ITEMS
      $(window).resize(debounce(resizeitems,100));
      $("body").trigger("resize");

      // right side-bar toggle
      $('.right-bar-toggle').on('click', function(e){

          $('#wrapper').toggleClass('right-bar-enabled');
      }); 

      
    },
    //initilizing 
    App.prototype.init = function() {
        var $this = this;
        //document load initialization
        $(document).ready($this.onDocReady);
        //init side bar - left
        $.Sidemenu.init();
        //init fullscreen
        $.FullScreen.init();
    },

    $.App = new App, $.App.Constructor = App

}(window.jQuery),

//initializing main application module
function($) {
    "use strict";
    $.App.init();
}(window.jQuery);



/* ------------ some utility functions ----------------------- */
//this full screen
var toggle_fullscreen = function () {

}

function executeFunctionByName(functionName, context /*, args */) {
  var args = [].slice.call(arguments).splice(2);
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for(var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  return context[func].apply(this, args);
}
var w,h,dw,dh;
var changeptype = function(){
    w = $(window).width();
    h = $(window).height();
    dw = $(document).width();
    dh = $(document).height();

    if(jQuery.browser.mobile === true){
        $("body").addClass("mobile").removeClass("fixed-left");
    }

    if(!$("#wrapper").hasClass("forced")){
      if(w > 1024){
        $("body").removeClass("smallscreen").addClass("widescreen");
          $("#wrapper").removeClass("enlarged");
      }else{
        $("body").removeClass("widescreen").addClass("smallscreen");
        $("#wrapper").addClass("enlarged");
        $(".left ul").removeAttr("style");
      }
      if($("#wrapper").hasClass("enlarged") && $("body").hasClass("fixed-left")){
        $("body").removeClass("fixed-left").addClass("fixed-left-void");
      }else if(!$("#wrapper").hasClass("enlarged") && $("body").hasClass("fixed-left-void")){
        $("body").removeClass("fixed-left-void").addClass("fixed-left");
      }

  }
  toggle_slimscroll(".slimscrollleft");
}


var debounce = function(func, wait, immediate) {
  var timeout, result;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) result = func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) result = func.apply(context, args);
    return result;
  };
}

function resizeitems(){
  if($.isArray(resizefunc)){  
    for (i = 0; i < resizefunc.length; i++) {
        window[resizefunc[i]]();
    }
  }
}

function initscrolls(){
    if(jQuery.browser.mobile !== true){
      //SLIM SCROLL
      $('.slimscroller').slimscroll({
        height: 'auto',
        size: "5px"
      });

      $('.slimscrollleft').slimScroll({
          height: 'auto',
          position: 'right',
          size: "5px",
          color: '#dcdcdc',
          wheelStep: 5
      });
  }
}
function toggle_slimscroll(item){
    if($("#wrapper").hasClass("enlarged")){
      $(item).css("overflow","inherit").parent().css("overflow","inherit");
      $(item). siblings(".slimScrollBar").css("visibility","hidden");
    }else{
      $(item).css("overflow","hidden").parent().css("overflow","hidden");
      $(item). siblings(".slimScrollBar").css("visibility","visible");
    }
}

// === following js will activate the menu in left side bar based on url ====
$(document).ready(function() {
    $("#sidebar-menu a").each(function() {
        if (this.href == window.location.href) {
            $(this).addClass("active");
            $(this).parent().addClass("active"); // add active to li of the current link
            $(this).parent().parent().prev().addClass("active"); // add active class to an anchor
            $(this).parent().parent().prev().click(); // click the item to make it drop
        }
    });
});

var wow = new WOW(
  {
    boxClass: 'wow', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 50, // distance to the element when triggering the animation (default is 0)
    mobile: false        // trigger animations on mobile devices (true is default)
  }
);
wow.init();


$(document).ready(function () {

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

    setImgPreview();
    setHeyConfirm2();
    setFancybox();
    setSwitchery();
    setSortableOrder();
    setDropzone();
    setSummernote();
	setTablesMisc();
});

function setTablesMisc(){
	$('#filtro_categorias').change(function(){
		$(this).closest('form').submit();
	});
}

function showLoading() {
    $("#loading-generico").fadeIn(200);
}
function hideLoading() {
    $("#loading-generico").fadeOut(200);
}

function setSummernote(){
	$('.summernote').summernote({
		height: 200,
		minHeight: null,
		maxHeight: null,
		focus: true,        
		tooltip: false,         
		popover: {
			image: [
				['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
				['float', ['floatLeft', 'floatRight', 'floatNone']],
				['remove', ['removeMedia']]
			],
			link: [
				['link', ['linkDialogShow', 'unlink']]
			],
			air: [
				['color', ['color']],
				['font', ['bold', 'underline', 'clear']],
				['para', ['ul', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture']]
			]
		},
		callbacks: {
	    	onInit: function() {
	    		console.log('Summernote is launched');
		    },
		    onPaste: function (e) {
		        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
		        e.preventDefault();
		        document.execCommand('insertText', false, bufferText);
		    }
		},
		toolbar: [
		    ['style', ['bold', 'italic', 'underline']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']],
		    ['insert', ['picture', 'link', 'video', 'table', 'hr']]
		],
		callbacks: {
	    	onInit: function() {
	    		console.log('Summernote is launched');
		    },
		    onPaste: function (e) {
		        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
		        e.preventDefault();
		        document.execCommand('insertText', false, bufferText);
		    }
		}
	});
}

function setDropzone(){
	
	Dropzone.autoDiscover = false;

	// SETEO TODO LO DEL DROPZONE
	$(".galeria-dropzone").each(function(){

		var arrayArchivosSubidos = [];

		var cont = $(this).closest(".galeria-cont");
		var id = $(this).data("id");
		var url = $(this).attr("action");
		var url_borrar = $(this).data("url-borrar");
		var galeriaProgress = cont.find(".galeria-progress");
		var this_dropzone = $(this);

		var myDropzone = new Dropzone(this, {
			url: url,
			acceptedFiles: ".png, .jpg, .jpeg",
			maxFilesize: 3000,
			method: "post",
			addRemoveLinks: true,
			parallelUploads: 20,
			autoProcessQueue: false,
			headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			} 
		});

		myDropzone.on("addedfile", function(file, message){
			// console.log("addedfile", file);
			cont.find(".btn.guardar").removeAttr("disabled");
		});

		myDropzone.on("totaluploadprogress", function(progress){
			// console.log("totaluploadprogress", progress);
		    updateProgressBar(progress, galeriaProgress);
		});

		myDropzone.on("queuecomplete", function(progress){
			// console.log("queuecomplete", progress);
			updateProgressBar(0, galeriaProgress);
			cont.find(".btn.guardar").removeAttr("disabled");
			//window.location.href = window.location.href;
			//agregarArchivosALista(cont, this_dropzone, arrayArchivosSubidos, galeriaProgress);

			// console.log('ARCHIVOS SUBIDOS', arrayArchivosSubidos);
			for(v in arrayArchivosSubidos){
				var actual = arrayArchivosSubidos[v];

				var urlBorrar = this_dropzone.data("url-borrar");
				var img = $('<div class="img" data-id="'+actual.id+'" id="img_'+actual.id+'"></div>');
				img.append('<a href="'+actual.imgpath_big+'" class="fancybox"><img src="'+actual.imgpath_small+'"></a> <a href="#" class="borrar" data-url="'+urlBorrar+'">Borrar</a>');

				cont.find(".galeria-actual").prepend(img).promise().done(function(){
					// Seteo links de borrar de nuevo
					setBorrarClick(cont, urlBorrar);
				});
			}

			// Se termino de subir todo
			resetDropzone(myDropzone, galeriaProgress, cont);

		});

		myDropzone.on("sending", function(file, xhr, formData){
			// console.log("sending", file);
			formData.append("id", id);

			cont.find(".btn.guardar").attr("disabled", "disabled");
		});

		myDropzone.on("success", function(file, response){
			//console.log("success response", response);
			// response = JSON.parse(response);
			// console.log("success response", response);
			if(response.success){
				arrayArchivosSubidos.push(response.img_obj);
				$.Notification.notify('success','top right', 'Se subieron los archivos.');
			} else {
				$.Notification.notify('error','top right', 'No se pudo subir todos los archivos.');
			}
			
		});

		cont.find(".btn.guardar").unbind("click").click(function(e){
			e.preventDefault();
			myDropzone.processQueue();
		});

		cont.find(".btn.reset").unbind("click").click(function(e){
			e.preventDefault();
			resetDropzone(myDropzone, galeriaProgress, cont);
		});

		// SETEO EL SORTABLE DE LA GALERIA
		var sortableDiv = cont.find(".galeria-actual");
		var url = this_dropzone.data("url-orden");

		sortableDiv.sortable({
			update: function (event, ui){
				var data = sortableDiv.sortable('serialize');
				console.log("Sorted", data);
				actualizarOrdenGaleria(sortableDiv, data, url);
			}
		});

		// SETEO BORRAR IMGS
		setBorrarClick(cont, url_borrar);

	});

	function agregarArchivosALista(cont, dropzone, arraySubidos, galeriaProgress){

		
		
	}

	function resetDropzone(myDropzone, galeriaProgress, cont){
		myDropzone.removeAllFiles(true);
		updateProgressBar(0, galeriaProgress);
		cont.find(".btn.guardar").attr("disabled", "disabled");
	}

	function setBorrarClick(cont, url_borrar){
		cont.find(".galeria-actual .img .borrar").unbind("click").click(function(e){
			e.preventDefault();
			var img = $(this).closest(".img");
			var id = img.data("id");
			var url = url_borrar;
			$.ajax({
				type: "POST",
				url: url,
				dataType: 'json',
				data: { id: id },
				success: function(data){
					console.log("borrar img", data);
					img.remove();
				}
			});	
		});
	}
}

function actualizarOrdenGaleria(sortableDiv, data, url){
	sortableDiv.sortable("disable");
	$.ajax({
		type: "POST",
		url: url,
		dataType: 'json',
		data: data,
		success: function(data){
			console.log("actualizarOrdenGaleria", data);
			sortableDiv.sortable("enable");
		}
	});	
}

function updateProgressBar(porcentaje, galeriaProgress){
    if(!isNaN(porcentaje)){
    	if(porcentaje > 0){
	    	$(".galeria-progress").css({
	    		opacity: 1
	    	});
    	} else {
    		$(".galeria-progress").css({
	    		opacity: 0
	    	});
    	}
        $(".galeria-progress .progress-bar").attr("aria-valuenow", porcentaje).css({
            width: porcentaje + "%"
        });
    }
}


function setSortableOrder(){
	/*var url = $(".sortable-table").data("url");
	$(".sortable-guardar").click(function(){
		var data = $('table.sortable tbody').sortable('serialize');
		console.log(data);
	});*/
	// table.heySort o ul.heySort o div.heySort
	$('.heySort').each(function(){
		var parent = $(this);
		var url = parent.data("url");

		var elemToSort;

		if(parent.is('table')){
			elemToSort = parent.find('tbody');
		} else {
			elemToSort = parent;
		}

		elemToSort.sortable({
			update: function (event, ui){
				var data = elemToSort.sortable('serialize');
				console.log("Sorted", data);
				actualizarOrden(parent, elemToSort, data, url);
			}
		});
	});
}
function actualizarOrden(parent, elemToSort, data, url){
	// $('table.sortable body').sortable("disable");
	elemToSort.sortable("disable");
	
	$.ajax({
		type: "POST",
		url: url,
		dataType: 'json',
		data: data,
		success: function(data){
			// console.log('success actualizando orden', data);
			elemToSort.sortable("enable");
		}
	});	
}

function setSwitchery(){
	$(".st-switch").change(function(){
		//console.log("Switch change");
		var url = $(this).data("url");
	    var type = $(this).data("type");
	    var id = $(this).data("id");
	    
	    var val;
	    if($(this).is(":checked")){
	    	//console.log("checked");
			val = 1;
	    } else {
	    	//console.log("not checked");
	    	val = 0;
	    }

	    $.ajax({
			type: "POST",
			url: url,
			dataType: 'json',
			/*headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},*/
			data: {
				id: id,
            	val: val
			},
			success: function(data){
				if(data.success){
					console.log("success switched", data);
				} else {
					alert("error");
					console.log("error switched", data);
				}
				
			}
		});	

	});
}
/* EJEMPLO:
<input type="checkbox" 
	class="st-switch" 
	data-id="<?=$pro->id;?>" 
	data-url="st_proyecto" 
	<?php if($pro->st == 1) echo "checked"; ?> 
	data-plugin="switchery" 
	data-color="#3bafda" 
	data-size="small" />
*/
function setFancybox(){
    
	$(".fancybox").fancybox({
		'type' : 'image'
	});
}

function setHeyConfirm2() {
	$(".hey-confirm").unbind("click").click(function(e){
        e.preventDefault();

	    var title = $(this).attr("data-title");
	    var text = $(this).attr("data-text");
	    var type = $(this).data("type");
	    var id_form = $(this).data("id-form");

		swal({
			title: title,
			text: text,
			type: type,
			showCancelButton: true,
			//confirmButtonColor: "#DD6B55",
			confirmButtonText: "aceptar",
			cancelButtonText: "cancelar",
			closeOnConfirm: true
		},
		function () {
			//window.location.href = window.location.href;
			if($('#'+id_form).length){
				document.getElementById(id_form).submit();
			}
		});
	});
}
function setHeyConfirm() {
    
    $(".hey-confirm").unbind("click").click(function(e){
        e.preventDefault();

	    var url = $(this).attr("href");
	    var title = $(this).attr("data-title");
	    var text = $(this).attr("data-text");
	    var method = $(this).data("method");
	    var type = $(this).data("type");
	    var id = $(this).data("id");
	    var tipo = $(this).data("tipo");

	    console.log("click1");

		swal({
			title: title,
			text: text,
			type: type,
			showCancelButton: true,
			//confirmButtonColor: "#DD6B55",
			confirmButtonText: "aceptar",
			cancelButtonText: "cancelar",
			closeOnConfirm: true
		},
		function () {
            if (method == undefined || method == null || method.toLowerCase() === "get") {
                window.location.href = url;
            } else {
            	//alert("post");
                //Entra aqui porque es un post
                $.ajax({
					type: "POST",
					url: url,
        			dataType: 'json',
					data: {
						id: id,
                    	tipo: tipo
					},
					success: function(data){
						//alert(data);
						if(data.success){
							console.log("success", data);
							window.location.href = window.location.href;
							if(data.RedirectUrl !== null){
                                window.location.href = data.RedirectUrl;
							}
						} else {
							alert("error");
							console.log("error", data);
						}
						
					},
					/*error: function(error){
						console.log("error", error);
					}*/
				});
            }
			  	
		});

	});
}

//"1<br /><font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Trying to get property of non-object in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Trying to get property of non-object in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-warning' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Warning: unlink(D:/PROYECTOS/urbanistas/sitio/php/img/home_slider/): Permission denied in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr><tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0295</td><td bgcolor='#eeeeec' align='right'>1248136</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.unlink' target='_new'>unlink</a>(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>25</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-fatal-error' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Fatal error: Call to a member function delete() on boolean in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>27</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font>"

function setImgPreview(){
	$(".img-preview .img").unbind("click").click(function(){
		var div = $(this).closest(".img-preview");
		div.find("input[type=file]").click();
	});
	$(".img-preview input[type=file]").unbind("change").change(function(e){
		var div = $(this).closest(".img-preview");
		var img = div.find(".img");
		var input = this;
		
	    if (input.files && input.files[0]) {

	        var reader = new FileReader();
	        var file = input.files[0];
	        
	        reader.readAsDataURL(file);
	        reader.onload = function (e) {
	        	
	            var thisresult = this.result;
	            var exif = EXIF.readFromBinaryFile(base64ToArrayBuffer(thisresult));
	            var orientation = exif.Orientation;
	            if (orientation >= 2 && orientation <= 8) { } else {
	                orientation = 0;
	            }
				
	            var src = e.target.result;
	            var hiddenObj = div.find(".hid-orientation");
	            img.css({ 'background-image': "url(" + src + ")" });
	            hiddenObj.val(orientation);

	            if (orientation === 3 || orientation === 8) {
	                img.addClass("rotar-izq-90");
	            } else if (orientation === 5 || orientation === 6 || orientation === 7) {
	                img.addClass("rotar-der-90");
	            }

	            // ********************************************************************
	            
	        }
	    }
	});
}
function base64ToArrayBuffer(base64) {
    base64 = base64.replace(/^data\:([^\;]+)\;base64,/gmi, '');
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}
/*
/* EJEMPLO: 
<div class="form-group img-preview">
    <span class="img" style="<?=$thumb_img_bg;?>"></span>
    <div class="info">
        <label>Imagen principal (~ 1200 x 800 px)</label>
        <input type="file" name="img_thumb">
        <input type="hidden" class="hid-orientation" name="img_thumb_orientation" value="0">
    </div>
    <div class="clearfix"></div>
</div>
*/
