$(document).ready(function () {
    setImgPreview();
    setHeyConfirm();
    setFancybox();
    setSwitchery();
    setSortableOrder();
    setDropzone();
    setSummernote();
});


function showLoading() {
    $("#loading-generico").fadeIn(200);
}
function hideLoading() {
    $("#loading-generico").fadeOut(200);
}

function setSummernote(){
	$('.summernote').summernote({
		height: 200,
		minHeight: null,
		maxHeight: null,
		focus: true,                 
		popover: {
			image: [
				['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
				['float', ['floatLeft', 'floatRight', 'floatNone']],
				['remove', ['removeMedia']]
			],
			link: [
				['link', ['linkDialogShow', 'unlink']]
			],
			air: [
				['color', ['color']],
				['font', ['bold', 'underline', 'clear']],
				['para', ['ul', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture']]
			]
		},
		callbacks: {
	    	onInit: function() {
	    		console.log('Summernote is launched');
		    },
		    onPaste: function (e) {
		        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
		        e.preventDefault();
		        document.execCommand('insertText', false, bufferText);
		    }
		},
		toolbar: [
		    ['style', ['bold', 'italic', 'underline']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']],
		    ['insert', ['picture', 'link', 'video', 'table', 'hr']]
		],
		callbacks: {
	    	onInit: function() {
	    		console.log('Summernote is launched');
		    },
		    onPaste: function (e) {
		        var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
		        e.preventDefault();
		        document.execCommand('insertText', false, bufferText);
		    }
		}
	});
}

function setDropzone(){
	
	Dropzone.autoDiscover = false;

	// SETEO TODO LO DEL DROPZONE
	$(".galeria-dropzone").each(function(){

		var arrayArchivosSubidos = [];

		var cont = $(this).closest(".galeria-cont");
		var id = $(this).data("id");
		var url = $(this).attr("action");
		var url_borrar = $(this).data("url-borrar");
		var galeriaProgress = cont.find(".galeria-progress");
		var this_dropzone = $(this);

		var myDropzone = new Dropzone(this, {
			url: url,
			acceptedFiles: ".png, .jpg, .jpeg",
			method: "post",
			addRemoveLinks: true,
			parallelUploads: 20,
			autoProcessQueue: false
		});

		myDropzone.on("addedfile", function(file, message){
			//console.log("addedfile", file);
			cont.find(".btn.guardar").removeAttr("disabled");
		});

		myDropzone.on("totaluploadprogress", function(progress){
			console.log("totaluploadprogress", progress);
		    updateProgressBar(progress, galeriaProgress);
		});

		myDropzone.on("queuecomplete", function(progress){
			console.log("queuecomplete", progress);
			updateProgressBar(0, galeriaProgress);
			cont.find(".btn.guardar").removeAttr("disabled");
			//window.location.href = window.location.href;
			//agregarArchivosALista(cont, this_dropzone, arrayArchivosSubidos, galeriaProgress);

			for(v in arrayArchivosSubidos){
				var actual = arrayArchivosSubidos[v];

				var urlBorrar = this_dropzone.data("url-borrar");
				var img = $('<div class="img" data-id="'+actual.id+'" id="img_'+actual.id+'"></div>');
				img.append('<a href="'+actual.imgpath_big+'" class="fancybox"><img src="'+actual.imgpath_small+'"></a> <a href="#" class="borrar" data-url="'+urlBorrar+'">Borrar</a>');

				cont.find(".galeria-actual").prepend(img).promise().done(function(){
					// Seteo links de borrar de nuevo
					setBorrarClick(cont, urlBorrar);
				});
			}

			// Se termino de subir todo
			resetDropzone(myDropzone, galeriaProgress, cont);

		});

		myDropzone.on("sending", function(file, xhr, formData){
			console.log("sending", file);
			formData.append("id", id);

			cont.find(".btn.guardar").attr("disabled", "disabled");
		});

		myDropzone.on("success", function(file, response){
			//console.log("success response", response);
			response = JSON.parse(response);
			console.log("success response", response);
			arrayArchivosSubidos.push(response.img_obj);

		});

		cont.find(".btn.guardar").unbind("click").click(function(e){
			e.preventDefault();
			myDropzone.processQueue();
		});

		cont.find(".btn.reset").unbind("click").click(function(e){
			e.preventDefault();
			resetDropzone(myDropzone, galeriaProgress, cont);
		});

		// SETEO EL SORTABLE DE LA GALERIA
		var sortableDiv = cont.find(".galeria-actual");
		var url = this_dropzone.data("url-orden");

		sortableDiv.sortable({
			update: function (event, ui){
				var data = sortableDiv.sortable('serialize');
				console.log("Sorted", data);
				actualizarOrdenGaleria(sortableDiv, data, url);
			}
		});

		// SETEO BORRAR IMGS
		setBorrarClick(cont, url_borrar);

	});

	function agregarArchivosALista(cont, dropzone, arraySubidos, galeriaProgress){

		
		
	}

	function resetDropzone(myDropzone, galeriaProgress, cont){
		myDropzone.removeAllFiles(true);
		updateProgressBar(0, galeriaProgress);
		cont.find(".btn.guardar").attr("disabled", "disabled");
	}

	function setBorrarClick(cont, url_borrar){
		cont.find(".galeria-actual .img .borrar").unbind("click").click(function(e){
			e.preventDefault();
			var img = $(this).closest(".img");
			var id = img.data("id");
			var url = url_borrar;
			$.ajax({
				type: "POST",
				url: url,
				dataType: 'json',
				data: { id: id },
				success: function(data){
					console.log("borrar img", data);
					img.remove();
				}
			});	
		});
	}
}

function actualizarOrdenGaleria(sortableDiv, data, url){
	sortableDiv.sortable("disable");
	$.ajax({
		type: "POST",
		url: url,
		dataType: 'json',
		data: data,
		success: function(data){
			console.log("actualizarOrdenGaleria", data);
			sortableDiv.sortable("enable");
		}
	});	
}

function updateProgressBar(porcentaje, galeriaProgress){
    if(!isNaN(porcentaje)){
    	if(porcentaje > 0){
	    	$(".galeria-progress").css({
	    		opacity: 1
	    	});
    	} else {
    		$(".galeria-progress").css({
	    		opacity: 0
	    	});
    	}
        $(".galeria-progress .progress-bar").attr("aria-valuenow", porcentaje).css({
            width: porcentaje + "%"
        });
    }
}


function setSortableOrder(){
	/*var url = $(".sortable-table").data("url");
	$(".sortable-guardar").click(function(){
		var data = $('table.sortable tbody').sortable('serialize');
		console.log(data);
	});*/
	// table.heySort o ul.heySort o div.heySort
	$('.heySort').each(function(){
		var parent = $(this);
		var url = parent.data("url");

		var elemToSort;

		if(parent.is('table')){
			elemToSort = parent.find('tbody');
		} else {
			elemToSort = parent;
		}

		elemToSort.sortable({
			update: function (event, ui){
				var data = elemToSort.sortable('serialize');
				console.log("Sorted", data);
				actualizarOrden(parent, elemToSort, data, url);
			}
		});
	});
}
function actualizarOrden(parent, elemToSort, data, url){
	// $('table.sortable body').sortable("disable");
	elemToSort.sortable("disable");
	
	$.ajax({
		type: "POST",
		url: url,
		dataType: 'json',
		data: data,
		success: function(data){
			// console.log('success actualizando orden', data);
			elemToSort.sortable("enable");
		}
	});	
}

function setSwitchery(){
	$(".st-switch").change(function(){
		//console.log("Switch change");
		var url = $(this).data("url");
	    var type = $(this).data("type");
	    var id = $(this).data("id");
	    
	    var st;
	    if($(this).is(":checked")){
	    	//console.log("checked");
			st = 1;
	    } else {
	    	//console.log("not checked");
	    	st = 0;
	    }

	    $.ajax({
			type: "POST",
			url: url,
			dataType: 'json',
			data: {
				id: id,
            	st: st
			},
			success: function(data){
				if(data.success){
					console.log("success switched", data);
				} else {
					alert("error");
					console.log("error switched", data);
				}
				
			}
		});	

	});
}
/* EJEMPLO:
<input type="checkbox" 
	class="st-switch" 
	data-id="<?=$pro->id;?>" 
	data-url="st_proyecto" 
	<?php if($pro->st == 1) echo "checked"; ?> 
	data-plugin="switchery" 
	data-color="#3bafda" 
	data-size="small" />
*/
function setFancybox(){
    
	$(".fancybox").fancybox({
		'type' : 'image'
	});
}

function setHeyConfirm() {
    
    $(".hey-confirm").unbind("click").click(function(e){
        e.preventDefault();

	    var url = $(this).attr("href");
	    var title = $(this).attr("data-title");
	    var text = $(this).attr("data-text");
	    var method = $(this).data("method");
	    var type = $(this).data("type");
	    var id = $(this).data("id");
	    var tipo = $(this).data("tipo");

	    console.log("click1");

		swal({
			title: title,
			text: text,
			type: type,
			showCancelButton: true,
			//confirmButtonColor: "#DD6B55",
			confirmButtonText: "aceptar",
			cancelButtonText: "cancelar",
			closeOnConfirm: true
		},
		function () {
            if (method == undefined || method == null || method.toLowerCase() === "get") {
                window.location.href = url;
            } else {
            	//alert("post");
                //Entra aqui porque es un post
                $.ajax({
					type: "POST",
					url: url,
        			dataType: 'json',
					data: {
						id: id,
                    	tipo: tipo
					},
					success: function(data){
						//alert(data);
						if(data.success){
							console.log("success", data);
							window.location.href = window.location.href;
							if(data.RedirectUrl !== null){
                                window.location.href = data.RedirectUrl;
							}
						} else {
							alert("error");
							console.log("error", data);
						}
						
					},
					/*error: function(error){
						console.log("error", error);
					}*/
				});
            }
			  	
		});

	});
}

//"1<br /><font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Trying to get property of non-object in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Trying to get property of non-object in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-warning' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Warning: unlink(D:/PROYECTOS/urbanistas/sitio/php/img/home_slider/): Permission denied in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr><tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0295</td><td bgcolor='#eeeeec' align='right'>1248136</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.unlink' target='_new'>unlink</a>(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>25</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-fatal-error' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Fatal error: Call to a member function delete() on boolean in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>27</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font>"

function setImgPreview(){
	$(".img-preview .img").unbind("click").click(function(){
		var div = $(this).closest(".img-preview");
		div.find("input[type=file]").click();
	});
	$(".img-preview input[type=file]").unbind("change").change(function(e){
		var div = $(this).closest(".img-preview");
		var img = div.find(".img");
		var input = this;
		
	    if (input.files && input.files[0]) {

	        var reader = new FileReader();
	        var file = input.files[0];
	        
	        reader.readAsDataURL(file);
	        reader.onload = function (e) {
	        	
	            var thisresult = this.result;
	            var exif = EXIF.readFromBinaryFile(base64ToArrayBuffer(thisresult));
	            var orientation = exif.Orientation;
	            if (orientation >= 2 && orientation <= 8) { } else {
	                orientation = 0;
	            }
				
	            var src = e.target.result;
	            var hiddenObj = div.find(".hid-orientation");
	            img.css({ 'background-image': "url(" + src + ")" });
	            hiddenObj.val(orientation);

	            if (orientation === 3 || orientation === 8) {
	                img.addClass("rotar-izq-90");
	            } else if (orientation === 5 || orientation === 6 || orientation === 7) {
	                img.addClass("rotar-der-90");
	            }

	            // ********************************************************************
	            
	        }
	    }
	});
}
function base64ToArrayBuffer(base64) {
    base64 = base64.replace(/^data\:([^\;]+)\;base64,/gmi, '');
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}
/*
/* EJEMPLO: 
<div class="form-group img-preview">
    <span class="img" style="<?=$thumb_img_bg;?>"></span>
    <div class="info">
        <label>Imagen principal (~ 1200 x 800 px)</label>
        <input type="file" name="img_thumb">
        <input type="hidden" class="hid-orientation" name="img_thumb_orientation" value="0">
    </div>
    <div class="clearfix"></div>
</div>
*/