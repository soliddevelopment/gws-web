﻿/// <reference path="../jquery-1.9.0.min.js" />
/// <reference path="../fusioncharts/fusioncharts.js" />
/// <reference path="../fusioncharts/fusioncharts.charts.js" />


(function ( window, undefined ) {
    window.Bitworks = ( typeof ( window.Bitworks ) === "undefined" ) ? {} : window.Bitworks;


    $( document ).ready( function () {
        Bitworks.FusionChart.Configurar();
    } );

    //Obj Grafico que Guarda Las instancias de los Graficos Actuales en el Sitio
    Bitworks.Grafico = function ( strId, urlData, boolActualizable, objChart ) {
        this.strID = strId;
        this.objFusion = objChart;
        this.UrlData = urlData;
        this.Actualizable = boolActualizable;
    };

    Bitworks.Grafico.prototype = {
        RefrescarGrafico: function () {
            var obj = this.objFusion;
            var hasQueryString = ( this.UrlData.indexOf( "?" ) > 0 ) ? true : false;
            var UrlUpdate = this.UrlData + ( ( hasQueryString == false ) ? "?Animacion=false" : "&Animacion=false" );
            obj.setJSONUrl( UrlUpdate );
        },
        destuirGrafico: function () {
            var obj = this.objFusion;
            obj.dispose();
        }
    };

    Bitworks.FusionChart = function () {
        var arrObjGraficos = new Array(), boolStop = false, idTimeout = 0;

        var InicializarCharts = function () {
            idTimeout = 0;
            var $divGraph = $( "div.graph" );
            $divGraph.each( function () {
                var idDiv = this.id;
                var urlData = $( this ).data( "graphurl" );
                var Actualizable = ( parseInt( $( this ).data( "autoupdate" ) ) === 1 ) ? true : false;
                var TipoGrafico = $( this ).data( "graphtype" );
                var width = $( this ).data( "width" );
                var height = $( this ).data( "height" );
                var objfusion = CrearFusionChart( TipoGrafico, idDiv, urlData, width, height );
                arrObjGraficos.push( new Bitworks.Grafico( idDiv, urlData, Actualizable, objfusion ) );
            } );
            //ConfiurarAutoRefresh();
        },
    CrearFusionChart = function ( chartype, strIDElemento, urlData, width, height ) {
        var ancho = "100%";
        var alto = "100%";

        var objFusion = new FusionCharts( {
            type: chartype,
            renderAt: strIDElemento,
            width: ancho,
            heigth: alto,
            dataFormat: "jsonurl",
            dataSource: urlData,
            events: {
                dataUpdated: function () {
                    ConfiurarAutoRefresh();
                }
            }

        } );
        console.log( objFusion );
        if ( height && width )
        {
            objFusion.resizeTo( width, height );
        }



        objFusion.configure( "LoadingText ", "Cargando el grafico" );
        objFusion.configure( "XMLLoadingText ", "Cargando los  datos" );
        objFusion.configure( "ChartNoDataText", "No hay Datos" );
        objFusion.render();
        return objFusion;
    },
    RefrescarTodosLosGraficos = function () {
        for ( var i = 0; i < arrObjGraficos.length; i++ )
        {
            var objActual = arrObjGraficos[i];
            objActual.RefrescarGrafico();
        }
    },
    RefrescarGraficosActualizables = function () {
        for ( var i = 0; i < arrObjGraficos.length; i++ )
        {
            var objActual = arrObjGraficos[i];
            if ( objActual.Actualizable === true )
            {
                objActual.RefrescarGrafico();
            }
        }
    },
    GetObjGraficoFromId = function ( strId ) {
        var objBuscado = null;
        for ( var i = 0; i < arrObjGraficos.length; i++ )
        {
            var objActual = arrObjGraficos[i];
            if ( objActual.strID === strId )
            {
                objBuscado = objActual;
                break;
            }
        }
        return objBuscado;
    },
     destroy = function ( strId ) {
         var objBuscado = null;
         for ( var i = 0; i < arrObjGraficos.length; i++ )
         {
             var objActual = arrObjGraficos[i];
             objActual.destuirGrafico();
         }
         arrObjGraficos = new Array();
         clearTimeout( idTimeout );
         idTimeout = 0;
     };

        //Funcionalidad para Refrescamiento Automatico
        var ConfiurarAutoRefresh = function () {
            if ( idTimeout === 0 )
            {
                idTimeout = setTimeout( function () {
                    RefrescarGraficosActualizables();
                    idTimeout = 0;
                }, 5000 );
            }
        };

        return {
            Configurar: InicializarCharts,
            DisposeGraficos: destroy,
            GetObjGraficoFromId: GetObjGraficoFromId
        };
    } ();
} )( window );