﻿/// <reference path="../../jquery-2.1.1.js" />


$(document).ready(function () {

    var CroppieOptions= function() {

        // Medidas output imagen default
        this.anchoViewPort = 100;
        this.altoViewPort = 100;

        // Medidas cuadro para croppear
        //this.anchoBoundary = 1200; // va a ser el ancho del output deseado
        //this.altoBoundary = 900; // va a ser el alto del output deseado

        this.proporcionBoundary = 0.8;
        this.proporcionViewport = 0.6;

        this.enableZoom = false;
        this.showZoomer = false;
        this.viewPortType = "square";

        this.idHidden = "";
        this.idImagenResultado = "";
        
        this.img = "";
    }

    //funcion de la copie a heymans que lee el archivo y se lo sampa a croppie
    function readFile(input,$uploadCropie,divContainer) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var demo = $(input).closest('.upload-demo');

            reader.onload = function (e) {
                $uploadCropie.croppie('bind', {
                    url: e.target.result
                });

                demo.addClass('ready');
            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            //swal("Sorry - you're browser doesn't support the FileReader API");
            alert("Sorry - your browser doesn't support the FileReader API");
        }
    }


    function configurarCroppiePopup(opcionesCroppie) {
        var $divPopup = $("div.upload-demo");
        var $croppieArea = $divPopup.find(".croppie-area");

        $divPopup.find(".croppie-area").croppie("destroy");

        // medidas de output
        var output_w = opcionesCroppie.anchoViewPort;
        var output_h = opcionesCroppie.altoViewPort;
        
        // el viewport va a ser 50% mas pequeño que el output
        var viewport_w = (output_w * opcionesCroppie.proporcionViewport);
        var viewport_h = (output_h * opcionesCroppie.proporcionViewport);
        
        // el boundary es del mismo tamaño del output
        var boundary_w = (output_w * opcionesCroppie.proporcionBoundary);
        var boundary_h = (output_h * opcionesCroppie.proporcionBoundary);

        //Inicializo el cropie con las propiades configuradas
         var $Croppie = $croppieArea.croppie({
            viewport: {
                width: viewport_w,
                height: viewport_h,
                type: opcionesCroppie.viewPortType
            },
            boundary: {
                width: boundary_w,
                height: boundary_h
            },
            exif: true,
            showZoomer: opcionesCroppie.showZoomer,
            enableZoomboolean: opcionesCroppie.enableZoom
         });

         //Quito los eventos previos para que no se ejecuten 2 vecex
         $divPopup.off("change", "input.croppie-upload");
         $divPopup.off("click", "button.upload-result");

        //Agrego el evento change al fileupload del popup
        $divPopup.on("change","input.croppie-upload",function() {
            readFile(this, $Croppie, $divPopup);
            //console.log("FILE INPUT", $(this).val());
        });

        //Ahora agrego el ultimo evento
        $divPopup.on("click","button.upload-result",function (e) {
            e.preventDefault();
            $Croppie.croppie("result",
            {   
                type: 'canvas',
                //size: 'original'
                //size: 'viewport'

                // aca tiene q ir el ancho original q quiero d la imagen
                size: { width: output_w },

            }).then(function(resp) {
                //console.log("resp", resp);
               
                $("#" + opcionesCroppie.idImagenResultado).attr("src", resp);
                $("#" + opcionesCroppie.idHidden).val(resp);
               

                $divPopup.closest(".modal").find(".close").trigger("click");
               
            });
        })

    }


    //Pongo el event handler a todos los botones que tienen un croppie selector
    $(document).on("click", "div.croppie-seleccionar-imagen button", function (e) {
        e.preventDefault();

        var $hidden = $(this).parent().find("input[type=hidden]");


        //Creo el objeto de opciones de croopie que vamos a usar para samparselo al popup cholero de bootstrap
        var datosCropie = new CroppieOptions();

        datosCropie.anchoViewPort = $(this).data("anchow");//altoViewPort
        datosCropie.altoViewPort = $(this).data("altow");

        var dataZoom = $(this).data("enablezoom");
        if (dataZoom && dataZoom.toLowerCase() === "true") {
            datosCropie.enableZoom = true;
            datosCropie.showZoomer = true;
        }

        var img = $hidden.val();
        if (img && img.length > 0) {
            datosCropie.img = img; // esto es el base64
        }

        datosCropie.idHidden = $hidden.attr("id");
        datosCropie.idImagenResultado = $(this).parent().find("img.img-pre").attr("id");
        datosCropie.viewPortType = $(this).data("viewporttype");
    
        //confiuro el cropie con los datos me dice el helper mfc
        configurarCroppiePopup(datosCropie);

    });


});