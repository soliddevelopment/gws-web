/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/assets/admin/js/view/grupos-mappings-posts.js":
/*!*****************************************************************!*\
  !*** ./resources/assets/admin/js/view/grupos-mappings-posts.js ***!
  \*****************************************************************/
/***/ (() => {

$(document).ready(function () {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // PANTALLA MAPPINGS

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  function prellenar($formgroup, campo) {
    var actualVal = '';
    var $input = $formgroup.find('.inputs .input-titulo');
    actualVal = $input.val();
    if (actualVal === undefined || actualVal === null || actualVal.length === 0) {
      var newVal = capitalizeFirstLetter(campo);
      $input.val(newVal);
    }
  }
  function actualizarMappings() {
    var cantidadChecked = 0;
    $('#grupo_mappings_page .columnas .cb').each(function () {
      var $cb = $(this);
      var $label = $cb.closest('label');
      var campo = $cb.data('campo');
      var $formgroup = $('#formGroup_' + campo);
      if (this.checked) {
        $formgroup.find('.inputs .form-control').removeAttr('disabled');
        prellenar($formgroup, campo);
        $formgroup.show();
        $label.addClass("sel");
        cantidadChecked++;
      } else {
        // $formgroup.find('.inputs input.form-control').val('');
        // $formgroup.find('.inputs select.form-control').val(0);
        $formgroup.find('.inputs .form-control').attr('disabled', 'disabled');
        $formgroup.hide();
        $label.removeClass("sel");
      }
    });
    if (cantidadChecked > 0) {
      $('.btn-cont-mappings').show();
      $('.no-hay-mappings').hide();
    } else {
      $('.btn-cont-mappings').hide();
      $('.no-hay-mappings').show();
    }
  }
  actualizarMappings();
  $('#grupo_mappings_page .columnas .cb').change(function () {
    actualizarMappings();
  });

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // PANTALLA POST FORM

  function chequearCaracteres($input) {
    var $formgroup = $input.closest('.form-group');
    var $max_txt = $formgroup.find('.max-txt');
    var limit = parseInt($input.attr('maxlength'));
    var text = $input.val();
    var chars = text.length;
    var chars_left = limit - chars;
    if (limit !== null && limit !== undefined && limit > 0) {
      if (chars > limit) {
        // se esta pasando del limite
        var new_text = text.substr(0, limit);
        $input.val(new_text);
        chars_left = 0;
      }
      if (chars === limit) {
        $max_txt.addClass('red');
      } else {
        $max_txt.removeClass('red');
      }
    }
    $max_txt.find('strong').html(chars_left);
  }
  $('.form-control.max-char-length').each(function () {
    var $input = $(this);
    chequearCaracteres($input);
    $input.keyup(function () {
      chequearCaracteres($input);
    });
  });

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // SLUGS AUTOMATICOS

  function removeAccents(str) {
    var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    str = str.split('');
    var strLen = str.length;
    var i, x;
    for (i = 0; i < strLen; i++) {
      if ((x = accents.indexOf(str[i])) != -1) {
        str[i] = accentsOut[x];
      }
    }
    return str.join('');
  }
  $('#posts_form_page input[name=slug]').each(function () {
    var $slug = $(this);
    var $form = $slug.closest('form');
    var $titulo = $form.find('input[name=titulo]');
    var editMode = $form.data('edit');
    $titulo.keyup(function () {
      if (editMode === false || editMode === 'false') {
        var tituloVal = $titulo.val();
        tituloVal = removeAccents(tituloVal);
        tituloVal = tituloVal.toLowerCase();
        var nuevoSlug = tituloVal.replace(/[^A-Z0-9]+/ig, "-");
        $slug.val(nuevoSlug);
      }
    });
  });
});

/***/ }),

/***/ "./resources/assets/public/css/sass/app.scss":
/*!***************************************************!*\
  !*** ./resources/assets/public/css/sass/app.scss ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/a/js/view/grupos-mappings-posts": 0,
/******/ 			"p/css/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["p/css/app"], () => (__webpack_require__("./resources/assets/admin/js/view/grupos-mappings-posts.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["p/css/app"], () => (__webpack_require__("./resources/assets/public/css/sass/app.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;