$(document).ready(function(){

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // PANTALLA MAPPINGS

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function prellenar($formgroup, campo){
        var actualVal = '';
        var $input = $formgroup.find('.inputs .input-titulo');
        actualVal = $input.val();
        if(actualVal === undefined || actualVal === null || actualVal.length === 0){
            var newVal = capitalizeFirstLetter(campo);
            $input.val(newVal);
        }
    }

    function actualizarMappings(){
        var cantidadChecked = 0;
        $('#grupo_mappings_page .columnas .cb').each(function(){
            var $cb = $(this);
            var $label = $cb.closest('label');
            var campo = $cb.data('campo');
            var $formgroup = $('#formGroup_'+campo);
    
            if(this.checked){
                $formgroup.find('.inputs .form-control').removeAttr('disabled');
                prellenar($formgroup, campo);
                $formgroup.show();
                $label.addClass("sel");
                cantidadChecked++;
                
            } else {
                // $formgroup.find('.inputs input.form-control').val('');
                // $formgroup.find('.inputs select.form-control').val(0);
                $formgroup.find('.inputs .form-control').attr('disabled', 'disabled');
                $formgroup.hide();
                $label.removeClass("sel");
            }
        });

        if(cantidadChecked>0){
            $('.btn-cont-mappings').show();
            $('.no-hay-mappings').hide();
        } else {
            $('.btn-cont-mappings').hide();
            $('.no-hay-mappings').show();
        }
    }

    actualizarMappings();

    $('#grupo_mappings_page .columnas .cb').change(function(){
        actualizarMappings();
    });

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // PANTALLA POST FORM

    function chequearCaracteres($input){
        var $formgroup = $input.closest('.form-group');
        var $max_txt = $formgroup.find('.max-txt');

        var limit = parseInt($input.attr('maxlength'));
        var text = $input.val();
        var chars = text.length;
        var chars_left = limit - chars;

        if(limit !== null && limit !== undefined && limit > 0){
            if(chars > limit){ // se esta pasando del limite
                var new_text = text.substr(0, limit);
                $input.val(new_text);
                chars_left = 0;
            }
            if(chars === limit){
                $max_txt.addClass('red');
            } else {
                $max_txt.removeClass('red');
            }
        }

        $max_txt.find('strong').html(chars_left);
    }

    $('.form-control.max-char-length').each(function(){
        var $input = $(this);
        chequearCaracteres($input);
        
        $input.keyup(function(){
            chequearCaracteres($input);
        });

    });
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // SLUGS AUTOMATICOS

    function removeAccents(str) {
        var accents    = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
        var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
        str = str.split('');
        var strLen = str.length;
        var i, x;
        for (i = 0; i < strLen; i++) {
          if ((x = accents.indexOf(str[i])) != -1) {
            str[i] = accentsOut[x];
          }
        }
        return str.join('');
    }

    $('#posts_form_page input[name=slug]').each(function(){
        var $slug = $(this);
        var $form = $slug.closest('form');
        var $titulo = $form.find('input[name=titulo]');
        var editMode = $form.data('edit');

        $titulo.keyup(function(){
            if(editMode === false || editMode === 'false'){
                var tituloVal = $titulo.val();
                tituloVal = removeAccents(tituloVal);
                tituloVal = tituloVal.toLowerCase();
                var nuevoSlug = tituloVal.replace(/[^A-Z0-9]+/ig, "-");
                $slug.val(nuevoSlug);
            }
        });
    });

});