// **********************************************************************************************************************************
$(document).ready(function(){
    setGeneral();
    setGeneralOnce();
});
// **********************************************************************************************************************************
// COSAS QUE SOLO DEBEN LLAMARSE UNA VEZ AUNQUE SE CAMBIE CON AJAX NAV
function setGeneralOnce(){

    setSolidUtilities();
    setSlidersOnce();

    $('.desktop-nav li a.pri').click(function(e){
        var $li = $(this).closest('li');
        $li.addClass('active').siblings().removeClass('active');
    });
}
function setSolidUtilities(){
    Solid.init({
        loading: true,
        loadingTimeout: 0,
        ajaxNav: false,
        ajaxNavCallback: function(){
            initMap();
        },
        menuAnimado: true,
        menuAnimadoSide: 'right',
        setHeaderChivo: true,
        heywow: true,

        // Validate y eso
        solidValidate: true,
        solidValidateAuto: false, // TRUE si quiero que se manejen el hideloading y showloading form solos, si no especificar aca abajo:
        showLoadingForm: function(form){
            var $form = $(form);
            $form.find('*[type=submit]').attr("disabled", "disabled").addClass("loading").val("Enviando..");
        },
        hideLoadingForm: function(form){
            var $form = $(form);
            $form.find('*[type=submit]').removeAttr("disabled").removeClass("loading").val("Enviar");
        },
        resetForm: function(form){
            var $form = $(form);
            $form.find('input[type=text], input[type=email], input[type=tel], select, textarea').val('');
        }
    });
    Browser.init();
}
// **********************************************************************************************************************************
// COSAS QUE DEBEN LLAMARSE CADA VEZ QUE HAY AJAX PAGE CHANGE
function setGeneral(){
    setSliders();
    setHeader();
    setVideos();
    setNosotros();
    setProductos();
    // setMapaCustom();

    // Llamar funciones custom aca
    Solid.showHiddenEmail('.hidden-email-1', 'acomercial', 'gws', '.com.sv');
    //acomercial@gws.com.sv
}

// **********************************************************************************************************************************
// FUNCIONES CUSTOM

function setSlidersOnce(){
    if($(".comercios-slider").length){
        // $(".comercios-slider").unslick();
        $(".comercios-slider").slick({
            slidesToShow: 7,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            autoplay: true,
            pauseOnHover: false,
            speed: 300,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 5,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                    }
                }
            ]
        });
    }
}
function setSliders(){

    $(".home-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        autoplay: true,
        pauseOnHover: false,
        speed: 1400,
        autoplaySpeed: 7000,
        fade: true
    });

    $(".destacados-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        autoplay: true,
        pauseOnHover: false,
        adaptiveHeight: true,
        speed: 500,
        autoplaySpeed: 7000
    });

    $(".otros-slider").slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        autoplay: true,
        pauseOnHover: true,
        speed: 300,
        autoplaySpeed: 3000,
        infinite: false,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 6,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 650,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 340,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    setProdSliders();

}

function setHeader(){

}

var player;
function setVideos(){
    $('.video-modal .cerrar, .video-modal .bg').unbind('click').click(function(e){
        // cerrarVideoModals();
        $('#videoModal').fadeOut(444, function(){
            if(typeof(player.stopVideo)==='function'){
                player.stopVideo();
                $('#videoModal iframe').remove();
                $('#videoModal .cuadro .cont').append($('<div id="player"></div>'));
            }
        });
    });

    window.YT || function(){
        var a=document.createElement("script");
        a.setAttribute("type","text/javascript");
        a.setAttribute("src","https://www.youtube.com/player_api");
        a.onload=function(){};
        a.onreadystatechange=function(){
            if (this.readyState=="complete"||this.readyState=="loaded") doYT()
        };
        (document.getElementsByTagName("head")[0]||document.documentElement).appendChild(a)
    }();

    $('#sec_videos .vid').unbind('click').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var youtubeId = $(this).data('video-id');
        // abrirVideoModal(id);
        openVideoModal(youtubeId);
    });
}

function openVideoModal(youtubeId){
    console.log('opening video', youtubeId);
    $('#videoModal').fadeIn(300, function(){
        player = new YT.Player('player', {
            height: '1205',
            width: '678',
            videoId: youtubeId,
            playerVars: {
                'autoplay': 1,        // Auto-play the video on load
                'controls': 1,        // Show pause/play buttons in player
                'showinfo': 1,        // Hide the video title
                'rel': 0,             // Do not show related videos
                'modestbranding': 1   // Hide the Youtube logo
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });

        // 4. The API will call this function when the video player is ready.
        function onPlayerReady(event) {
            event.target.playVideo();
        }

        var done = false;
        function onPlayerStateChange(event) {
            /*if (event.data == YT.PlayerState.PLAYING && !done) {
                setTimeout(stopVideo, 6000);
                done = true;
            }*/
        }
        function stopVideo() {
            player.stopVideo();
        }

    });



    // 2. This code loads the IFrame Player API code asynchronously.
    /*var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);*/


}

function abrirVideoModal(id){
    $('#'+id).fadeIn(300);
}

/* function cerrarVideoModals(){
    $('.video-modal').fadeOut(444, function(){
        if(typeof(player.stopVideo)==='function'){
            player.stopVideo();
            $('.video-modal')
        }
    });
} */

function changeValoresImg(id){
    $('#'+id).addClass('sel').siblings().removeClass('sel');
}

function setNosotros(){
    if(Solid.medidas.W > 1100){
        $('#valoresImg .valores .elem').removeClass('sel');
    } else {
        var $firstElem = $('#valoresImg .valores .elem').first();
        var id = $firstElem.data('id');
        changeValoresImg(id);
    }
    $('#valoresImg .valores .elem').unbind('hover').hover(
        function(){
            console.log('hover on valor elem');
            if(Solid.medidas.W > 1100){
                var id = $(this).data('id');
                changeValoresImg(id);
            }
        }
    );
    $('#valoresImg .valores .elem').unbind('click').click(function(e){
        e.preventDefault();
        if(Solid.medidas.W <= 1100){
            $(this).addClass('sel').siblings().removeClass('sel');
            var id = $(this).data('id');
            changeValoresImg(id);
        }
    });
}

$(window).resize(function(){
    setNosotros();
});

function setMapaCustom(){
    $('.ver-mapa').unbind('click').click(function(e){
        e.preventDefault();
        abrirMapaCustom();
    });
    $('#mapaVentana .cerrar, #mapaVentanaOverlay').unbind('click').click(function(e){
        cerrarMapaCustom();
    });

    function abrirMapaCustom(){
        $('#mapaVentana, #mapaVentanaOverlay').fadeIn(444);
    }
    function cerrarMapaCustom(){
        $('#mapaVentana, #mapaVentanaOverlay').fadeOut(444);
    }
}

function initMap() {
    var markerSample = {
        title: 'a',
        lat: 13.9842092,
        lng: -89.5678072
    };

    if($('#mapaContacto').length){
        Solid.initGoogleMap('mapaContacto', markerSample);
    }

    var limit = 0;
    var markersLocations = [];
    console.log('locationsArray', locationsArray);
    if(locationsArray && locationsArray.length > 0){
        for(v in locationsArray){
            var actual = locationsArray[v];
            var marker = {
                icon: '/p/img/flag.png',
                title: actual.nombre,
                lat: actual.lat,
                lng: actual.lng
            };
            if(v < limit || limit === 0) markersLocations.push(marker);
        }
    }

    Solid.initGoogleMap('mapa-comercios', markersLocations);
}

function setProdSliders(){
    $(".prod-slider").slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        autoplay: true,
        pauseOnHover: true,
        speed: 300,
        autoplaySpeed: 3000,
        infinite: false,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 650,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 340,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
}

function setProductos(){
    $('#buscarField').val('');
    $('#buscarField').keyup(function(){
        var str = $(this).val();
        var filter = str.toUpperCase();
        var htmlGlobal = '';
        var cantProds = 0;

        if(str.length > 0){
            $('.lista-productos .cat .slide.slick-slide').each(function(){
                var nombre_producto = $(this).find('a').data('nombre');
                var nombre_categoria = $(this).find('a').data('categoria');
                if (nombre_producto.toUpperCase().indexOf(filter) > -1) {
                    var html = '<div class="res">';
                    html+= $(this).html();
                    html+= '</div>';
                    htmlGlobal += html;
                    cantProds++;
                } else if(nombre_categoria.toUpperCase().indexOf(filter) > -1) {
                    var html = '<div class="res">';
                    html+= $(this).html();
                    html+= '</div>';
                    htmlGlobal += html;
                    cantProds++;
                }
            });
        }
        if(str.length > 0){
            if(cantProds > 0){
                console.log('resutlados mostrados', cantProds);
                $('#resultados').html(htmlGlobal).show();
                $('#listaProductos').hide();
                $('#noHay').hide();
                // $(".prod-slider").unslick();
            } else {
                $('#resultados').html('').hide();
                $('#listaProductos').show();
                $('#noHay').show();
                // setProdSliders();
                $('.prod-slider').slick('setPosition');
            }
        } else {
            $('#resultados').html('').hide();
            $('#listaProductos').show();
            $('#noHay').hide();
            $('.prod-slider').slick('setPosition');
            // setProdSliders();
        }
    });
}
