// Mierdas custom de JQUERY //////////////////////////////////////////////////////////////////////
$.fn.random = function() {
	return this.eq(Math.floor(Math.random() * this.length));
}   

jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requerido.",
    remote: "Please fix this field.",
    email: "Este email no es válido.",
    url: "Este no es un URL válido.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Muy corto.")
});

jQuery.validator.addMethod("greaterThanZero", function(value, element) {
    return this.optional(element) || (parseFloat(value) > 0);
}, "* Este campo es requerido");
/*
toastr.options = {
	"closeButton": false,
	"debug": false,
	"newestOnTop": false,
	"progressBar": false,
	"positionClass": "toast-bottom-left",
	"preventDuplicates": false,
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}
*/
// Utilidades Solid ////////////////////////////////////////////////////////////////////////////////
var Solid = {

	ImgArray: [],

	Medidas: {
		w: null,
		h: null
	},

    Init: function(ops){

    	var opciones = $.extend({
    		loading: true,
    		loadingTimeout: 0,

    		medidas: true,
			seeMore: true,
			fancybox: true,
			scrollAnimado: true,
			ajaxNav: true,
			titanNav: true,
			titanNavSettings: {
				tipo: 1
			},
			solidModals: true,
			wow: true,
			
			heywow: true,
			heywowSettings: {
				className: "heywow",
				classIn: "in",
				timeout: 700,
				reset: false // si es false, es como wow, que no vuelven a reaparecer las cosas
			},

			menuBasico: true,
			touchClass: true,
			parallax: true,
			headerChivo: true,
			sticky: true
		}, ops);

    	// PRIMERO VAN LAS COSAS QUE SE SETEAN SOLO UNA VEZ
		Solid.SetOnce(opciones);

		// ACA VAN LAS COSAS QUE SE SETEAN SIEMPRE QUE CARGA UNA PAGINA
		Solid.SetTodo(opciones);

		////////////////////////////////////////////////////////////////////////////////////////////////////////

		if(opciones.headerChivo){
			Solid.SetHeaderChivo();
		}

    }, // fin de Init //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // LAS COSAS QUE SE SETEAN SOLO UNA VEZ
    SetOnce: function(opciones){
    	//console.log("Solid.ImgArray", Solid.ImgArray);
    	// LOADING
		/*if(opciones.loading){
			if( typeof imgArray !== 'undefined' && imgArray instanceof Array && imgArray.length>0 ){
				Solid.Preload(imgArray, function(){

					Solid.Load();
				});
			} else {
				Solid.Load();
			}
		}*/
    	// SETEAR VARIABLES MEDIDAS VENTANA
    	if(opciones.medidas){
    		this.Medidas.W = $(window).width();
    		this.Medidas.H = $(window).height();
    	}
		// AJAX NAV
		if(opciones.ajaxNav){
			Solid.SetAjaxNav(opciones);
		}
		// Menú tipo mobile (siempre mobile hasta en web)
		if(opciones.menuBasico){
			Solid.SetMenuBasico();
		}
		if(opciones.sticky){
			Solid.SetSticky();
		}
		if(opciones.heywow){
			Solid.SetHeywow(opciones.heywowSettings);
		}
		if(opciones.titanNav){
			Solid.SetTitanNav(opciones.titanNavSettings);
		}
    },

    // LAS COSAS QUE SE SETEAN SIEMPRE QUE CARGA UNA PAGINA
	SetTodo: function(opciones){

		Solid.SetMisc();
		
		// PARA PARRAFOS LARGOS CON UN LINK "SEE MORE"
		// Tiene estilos CSS dependientes en snippets.css
    	if(opciones.seeMore){
			$(".see-more").unbind("click").click(function(e){
				e.preventDefault();
				var link = $(this); var content = $(this).parent().find(".more");
				if(content.is(":visible")){
					content.hide();
					link.html("Ver más");
				} else {
					content.show();
					link.html("Ver menos");
				}
			});
			/*<p>
			Nuestra familia acaba de vivir una bellísima experiencia, casar un hijo. <br><br>
			<span class="more">Ellos son eficientes, puntuales, recursivas, creativas, en fin… ¡Pura energía¡, todo lo resuelven, lo conocen, lo consiguen y lo logran haciéndolo sentir a uno bien siempre al punto de disfrutar, no solamente de la organización sino la realización del evento.</span>
			<a href="#" class="see-more">Ver más</a>
			</p>*/
		}
		
		// HEY LOADING INICIAL Y DE AJAX
		if(opciones.loading){
			if( typeof imgArray !== 'undefined' && imgArray instanceof Array && imgArray.length>0 ){
		        Solid.Preload(imgArray, function(){
		            Solid.Load(opciones.loadingTimeout);
		        });
		    } else {
		        Solid.Load(opciones.loadingTimeout);
		    }
		} else {
			$('#loading').remove();
		}
		
		// FANCYBOX PARA IMAGENES DINAMICAS
		if(opciones.fancybox){
			$(".fancybox").fancybox({
				'type' : 'image'
			});
		}

		// SCROLL ANIMADO 
		if(opciones.scrollAnimado){
			// Agregar clase .scroll-to a link para aimate scroll a elemento con x id
			$(".scroll-to").unbind("click").click(function(e){
				e.preventDefault();
				var ms = $(this).data("ms") || null;
				var selector = $(this).data("href") || $(this).attr("href") || null;
				var easing = $(this).data("easing") || null;
				var offset = $(this).data("offset") || null;
				Solid.ScrollTo(selector, ms, easing, offset);
			});
			//<a class="scroll-to" data-ms="1100" data-easing="easeOutCirc" href="#contacto">CONTACTO</a>
		}


		// Modals genéricos
		if(opciones.solidModals){
			Solid.SetModals();
		}

		// Animaciones en viewport
		if(opciones.wow){
			new WOW().init();
		}

		// Animaciones en viewport
		if(opciones.heywow){
			Solid.SetHeywow(opciones.heywowSettings);
			setTimeout(function(){
				Solid.CheckHeywow(opciones.heywowSettings);
			}, 100);
		}

		// Agrega clase a <body> para saber si es touch o no el device
		if(opciones.touchClass){
			if(Solid.Touch()){
				$("body").addClass("hey-touch");
				$(".hvr-grow").removeClass("hvr-grow");
				$(".hvr-float").removeClass("hvr-float");
			} else {
				$("body").addClass("hey-no-touch");
			}
		}

		if(opciones.parallax){
			// ESTA FUNCION LA HICE PARA LLAMARLA CON AJAX Y RESETEAR PARALLAX QUE HAYA QUE RESETEAR
			// primero borro los q ya estaban
			$(".parallax-mirror").remove();
			// aca estoy reseteando manualmente (o programaticamente)
			$('.parallax').each(function(){
				var path = $(this).attr("data-image-src");
				$(this).parallax({ imageSrc: path });
			});
		}
	},

	SetMisc: function(){
		//$('.datepicker').datepicker();
	},

	SetHeywow: function(settings){
		//console.log(settings);
		//Solid.CheckHeywow(settings);
		$(window).scroll(function(){
			Solid.CheckHeywow(settings);
		});
	},

	CheckHeywow: function(settings){
		if(settings && settings.className && settings.className.length){
			$("."+settings.className).each(function(){
				if($(this).is(":in-viewport")){
					$(this).addClass(settings.classIn);
				} else {
					if(settings.reset) $(this).removeClass(settings.classIn);
				}
			});
		}
	},

	SetTitanNav: function(settings){

		// NAV tradicional como heynav, al llegar a cierto punto se hace mobile
		if(settings.tipo === 1){
			$('#menu-btn').unbind('click').click(function(e){
				e.preventDefault();
				$('.nav1').toggleClass('opened');
				$('.nav-overlay').toggleClass('opened');
			});

		} else if(settings.tipo === 2){
			$('#menu-btn2').unbind('click').click(function(e){
				e.preventDefault();
				$('.nav2').toggleClass('opened');
				$('.nav-overlay').toggleClass('opened');
			});
		}

		$('.nav-overlay').click(function(){
			cerrarTodas();
		});

		function cerrarTodas(){
			$('#nav, .nav-overlay').removeClass('opened');
		}

		/*$("#nav ul li a").click(function(e){
			$(this).closest("li").addClass('sel').siblings().removeClass("sel");
			$('#nav').removeClass('opened');
			$('.nav-overlay').removeClass('opened');
		});*/
	},

    ShowHiddenEmail: function(selector, username, dominio, dom){
		var r = 'mailto:' + username + '@' + dominio + dom;
		if($(selector).length) $(selector).attr('href',r).html(username + '@' + dominio + dom);
    },

	Preload: function(imgArray, callback_final, callback_siempre){
		if(imgArray && typeof imgArray !== 'undefined'){
			if(imgArray && imgArray.length>0){
				var img_count = imgArray.length;
			    //$log = $('#loading .log').val(''); //$progress = $('#loading .info').text('0 / '+img_count); 
				loader = new PxLoader(); 
				for(i in imgArray){
					var pxImage = new PxLoaderImage(imgArray[i]); pxImage.imageNumber = i + 1; loader.add(pxImage);
				}
				loader.addProgressListener(function(e) { // callback that runs every time an image loads 
					if(typeof callback_siempre === "function") callback_siempre();
				    //$log.val($log.val() + 'Image ' + e.resource.imageNumber + ' Loaded\r'); // log which image completed 
				    //$progress.text(e.completedCount + ' / ' + e.totalCount); // the event provides stats on the number of completed items
				});
				loader.addCompletionListener(function() {
					if(typeof callback_final === "function") callback_final();
				});  
				loader.start(); 
			} else {
				if(typeof callback_final === "function") callback_final();
			}
		} else {
			if(typeof callback_final === "function") callback_final();
		}
	},

    SetAjaxNav: function(opciones){
    	$("a").ajaxnav({
			claseLink: "ajax-link",
			idContainer: "content",
			delay: 0,
			beforeLoad: function(){ // siempre antes de cambiar page
				Solid.ShowLoading();
			},
			success: function(){ // each page change
				Solid.SetTodo(opciones);
				setGeneral();

				Solid.Preload(imgArray, function(){
					$("html, body").scrollTop(0);
					Solid.HideLoading();
				});
			}
		});

    	// Esto esta por gusto?
		/*History = window.History;
		History.Adapter.bind(window,'statechange',function(){ 
		    var State = History.getState(); 
		    //History.log(State.data, State.title, State.url);
		    //console.log(State.data, State.title, State.url);
		    console.log("State.data", State.data);
		});*/
    },

	Load: function(loadingTimeout){
		if(loadingTimeout >= 0){
			setTimeout(function(){
				Solid.HideLoading();
			}, loadingTimeout);
		} else {
			Solid.HideLoading();
		}
	},

	ShowLoading: function(){
		$("#loading").fadeIn(144, function(){
			//$(this).remove();
			// $("#loading").addClass("ajax");
		});
	},

	HideLoading: function(){
		$("#loading").fadeOut(144, function(){
			//$(this).remove();
			$("#loading").addClass("ajax");
		});
	},

    SetHeaderChivo: function(){
		
		$('#header').data('bg','black');


		if(Solid.w<=1200 || $("#sec_inicio").length === 0){
			// no estoy en el home
			var scrollPosition = 20;
		} else {
			// estoy en el home
			//var scrollPosition = ($(".home-slider").height() - 144);
			var scrollPosition = 20;
		}
		scrollPosition = 50;

		$(window).scroll(function(){
		  	if($(document).scrollTop() > scrollPosition){
			    if($('#header').data('bg') == 'black'){
			        $('#header').data('bg','white');
					$("#header").addClass("compact");

					// $("#sidemenu_btn").addClass("compact");
			    }
			} else {
			    if($('#header').data('bg') == 'white'){
		        	$('#header').data('bg','black');
					$("#header").removeClass("compact");

					// $("#sidemenu_btn").removeClass("compact");
			    }  
		 	}
		});
    },

    StickyNav: function(){
    	var selector = ".sticky-elem";
    	if($(selector).length){
	    	var stickyNavTop = $(selector).offset().top;
			var scrollTop = $(window).scrollTop();
			console.log(scrollTop, stickyNavTop);
			if (scrollTop > stickyNavTop) { 
			    $(selector).addClass('sticky');
			} else {
			    $(selector).removeClass('sticky'); 
			}
		}
    },

    SetSticky: function(){
    	console.log("Setting Sticky");
		Solid.StickyNav();
		$(window).scroll(function() {
		  	Solid.StickyNav();
		});
    },

    OpenMenu: function(){
		$("#menu, #menu-overlay").addClass("opened");
		Solid.menuOpened = true;
	},

	CloseMenu: function(){
		$("#menu, #menu-overlay").removeClass("opened");
		Solid.menuOpened = false;
	},

    SetMenuBasico: function(){
    	$(".menu-btn").unbind("click").click(function(e){
			e.preventDefault();
			if(Solid.menuOpened){
				Solid.CloseMenu();
			} else {
				Solid.OpenMenu();
			}
		});

		$("#menu-overlay").unbind("click").click(function(e){
			Solid.CloseMenu();
		});

		// MENU NUEVO
		$("#sidemenu_btn, #sidemenu_overlay").unbind("click").click(function(e){
			e.preventDefault();
			toggleOpen();
		});
		$("#sidemenu .sidemenu-nav li a").not('.void').click(function(){
			toggleOpen();
			var li = $(this).closest("li");
			$('#sidemenu li').removeClass('active');
			li.addClass("active");
		});
		$("#sidemenu .sidemenu-nav .void").click(function(e){
			e.preventDefault();
		});

		function toggleOpen(){
			$("#sidemenu_overlay").toggleClass("open");
			$("#sidemenu_btn").toggleClass("open");
			$("#sidemenu").toggleClass("open");
		}

    },

    SetModals: function(){
    	$(".alerta .cerrar").unbind("click").click(function(e){
			e.preventDefault();
			Solid.CerrarModal($(this).closest(".alerta"));
		});
    },

    AbrirModal: function(tipo, mensaje, titulo){
    	if(!tipo) var tipo = 1; // alerta normal

    	if(mensaje && mensaje.length > 0){
    		var alerta = $("<div class='alerta'></div>");
			var html = '<span class="cerrar fa fa-remove"></span>';
			if(titulo && titulo.length > 0 ){
				html += '<p class="titulo">'+titulo+'</p>';
			}
			html += '<p class="descripcion">'+mensaje+'</p>';

			alerta.html(html);

			$("body").append(alerta).promise().done(function(){
				alerta.fadeIn(144);
				Solid.SetModals();
			});
		}
    },

    CerrarModal: function(modal){
    	modal.fadeOut(144, function(){
			if(!modal.hasClass("persistent")) modal.remove();
		});
    },

	// HACER UN SCROLL ANIMADO A UN ELEMENTO DEL DOM
	ScrollTo: function(selector, ms, easing, offset, callback){
		if(selector.length > 0){
			if(offset===null) var offset = 20;
			if(ms===null) var ms = 2000;
			if(easing===null) var easing = "easeOutQuart";

			var scrolltop = $(selector).offset().top;
			//if(scrolltop!==undefined && offset>0) scrollTop += offset;

			$('html, body').animate({
				//width: [ "toggle", "swing" ],
				//height: [ "toggle", "swing" ]
				//scrollTop: [scrolltop, "easeOutBounce"]
				scrollTop: scrolltop
			}, ms, easing, function() {
				if(typeof(callback) === "function"){
					callback();
				}				
			});
		}
	},
    Touch: function () {
        if(Browser.browser==="Firefox"){
			if(Modernizr.touchevents===true){
				//console.log("Dispositivo touch (Firefox)");
				return true;
			} else {
				//console.log("Dispositivo no touch (Firefox)");
				return false;
			}
			
		} else {
			if(Modernizr.touch===true){
				//console.log("Dispositivo touch");
				return true;
			} else {
				//console.log("Dispositivo no touch");
				return false;
			}
		}
    },
    ValidateEmail: function(email){
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
    }
};

// PARA DETERMINAR BROWSER ////////////////////////////////////////////////////////////////////////////////
var Browser = {
    Init: function () {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;
            if (dataString.indexOf(data[i].subString) !== -1) return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index === -1) return;
        var rv = dataString.indexOf("rv:");
        if (this.versionSearchString === "Trident" && rv !== -1) {
            return parseFloat(dataString.substring(rv + 3));
        } else {
            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
        }
    },
    dataBrowser: [
        {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
        {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
        {string: navigator.userAgent, subString: "Safari", identity: "Safari"},
        {string: navigator.userAgent, subString: "Opera", identity: "Opera"}
    ]
};

/////////////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function(){
	// console.log("INIT");
});
/////////////////////////////////////////////////////////////////////////////////////////////////


/* SNIPPETS UTILES: */

/*
swal({
	title: "Are you sure?",
	text: "You will not be able to recover this imaginary file!",
	type: "warning",
	showCancelButton: true,
	confirmButtonColor: "#DD6B55",
	confirmButtonText: "Yes, delete it!",
	closeOnConfirm: false
},
function(){
	swal("Deleted!", "Your imaginary file has been deleted.", "success");
});

$(".clientes-slider").slick({
	slidesToShow: 8,
	slidesToScroll: 1,
	dots: false,
	arrows: false,
	infinite: true,
	autoplay: true,
	pauseOnHover: false,
	autoplaySpeed: 2000,
	responsive: [
	    {
	      breakpoint: 1000,
	      settings: {
	        slidesToShow: 4
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 3
	      }
	    }
	]
});

*/