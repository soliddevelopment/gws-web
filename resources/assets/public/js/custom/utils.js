
// Utilidades Solid ////////////////////////////////////////////////////////////////////////////////
var Solid = {

	imgArray: [],
	pagesLoaded: 0,
	medidas: {
		w: null,
		h: null
	},

    init: function(ops){

    	var opciones = $.extend({

            // el preload de imagenes
    		loading: true,
    		loadingTimeout: 0,

            // mierdas varias
			seeMore: true,
			fancybox: false,
			scrollAnimado: true,
			ajaxNav: true,
			ajaxNavCallback: function(){},
			solidModals: true,
			wow: true,
			touchClass: true,
			parallax: true,
			headerChivo: true,
            sticky: true,
            solidValidate: true,
            solidValidateAuto: true,
            showLoadingForm: function(){},
            hideLoadingForm: function(){},
            resetForm: function(){},
            
            // navegacion (menus y eso)
			menuAnimado: true,
			menuAnimadoSide: 'right', // right o left
            
            // animaciones when scroll in view custom 
			heywow: true,
			heywowSettings: {
				onceClassName: "heywow-once",
				className: "heywow",
				classIn: "in",
				timeout: 700,
				reset: false // si es false, es como wow, que no vuelven a reaparecer las cosas
			},

		}, ops);

    	// PRIMERO VAN LAS COSAS QUE SE SETEAN SOLO UNA VEZ
		Solid.setOnce(opciones);

		// ACA VAN LAS COSAS QUE SE SETEAN SIEMPRE QUE CARGA UNA PAGINA
		Solid.setTodo(opciones);

		////////////////////////////////////////////////////////////////////////////////////////////////////////

		if(opciones.headerChivo){
			Solid.setHeaderChivo();
		}

		Solid.pagesLoaded++;

    }, // fin de Init //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // LAS COSAS QUE SE SETEAN SOLO UNA VEZ
    setOnce: function(opciones){
		
		// Seteo el loading inicial
		function cargarSitio(timeout, ops){
			function confirmar(ops){
				$("."+ops.heywowSettings.onceClassName).each(function(){
					$(this).addClass(ops.heywowSettings.classIn);
				});
			}
			if(timeout > 0){
				setTimeout(function(){
					Solid.loadWebsite(ops);
					confirmar(ops);
				}, timeout);
			} else {
				Solid.loadWebsite(ops);
				confirmar(ops);
			}
		}

		if(opciones.loading){
			if( typeof imgArray !== 'undefined' && imgArray instanceof Array && imgArray.length>0 ){
				Solid.preload(imgArray, function(){
					cargarSitio(opciones.loadingTimeout, opciones);
				});
			} else {
				cargarSitio(opciones.loadingTimeout, opciones);
			}
        }
		
		$(window).resize(function(){
			Solid.setMedidas();
		});
		
		Solid.setMedidas();

		// AJAX NAV
		if(opciones.ajaxNav){
			$("a").ajaxnav({
                claseLink: "ajax-link",
                idContainer: "content",
                delay: 0,
                beforeLoad: function(){ // siempre antes de cambiar page
                    Solid.showLoading();
                },
                success: function(){ // each page change
                    Solid.setTodo(opciones);
                    setGeneral();
    
                    Solid.preload(imgArray, function(){
                        $("html, body").scrollTop(0);
                        // Solid.HideLoading();
                        Solid.loadWebsite(opciones);
                    });
                }
            });
            // Esto esta por gusto?
            /*History = window.History;
            History.Adapter.bind(window,'statechange',function(){ 
                var State = History.getState(); 
                //History.log(State.data, State.title, State.url);
                //console.log(State.data, State.title, State.url);
                console.log("State.data", State.data);
            });*/
        }
        
		// Menú tipo mobile animado (siempre mobile hasta en web)
		if(opciones.menuAnimado){
			Solid.setMenuAnimado(opciones.menuAnimadoSide);
        }
        
		if(opciones.sticky){
			Solid.setSticky();
		}
		if(opciones.heywow){
			Solid.setHeywow(opciones.heywowSettings);
		}
		if(opciones.titanNav){
			Solid.setTitanNav(opciones.titanNavSettings);
		}
    },

    // LAS COSAS QUE SE SETEAN SIEMPRE QUE CARGA UNA PAGINA
	setTodo: function(opciones){

		Solid.setMisc();
		
		// PARA PARRAFOS LARGOS CON UN LINK "SEE MORE"
		// Tiene estilos CSS dependientes en snippets.css
    	if(opciones.seeMore){
			$(".see-more").unbind("click").click(function(e){
				e.preventDefault();
				var link = $(this); var content = $(this).parent().find(".more");
				if(content.is(":visible")){
					content.hide();
					link.html("Ver más");
				} else {
					content.show();
					link.html("Ver menos");
				}
			});
			/*<p>
			Nuestra familia acaba de vivir una bellísima experiencia, casar un hijo. <br><br>
			<span class="more">Ellos son eficientes, puntuales, recursivas, creativas, en fin… ¡Pura energía¡, todo lo resuelven, lo conocen, lo consiguen y lo logran haciéndolo sentir a uno bien siempre al punto de disfrutar, no solamente de la organización sino la realización del evento.</span>
			<a href="#" class="see-more">Ver más</a>
			</p>*/
		}
		
		// FANCYBOX PARA IMAGENES DINAMICAS
		if(opciones.fancybox){
			$(".fancybox").fancybox({
				'type' : 'image'
			});
		}

		// SCROLL ANIMADO 
		if(opciones.scrollAnimado){
			// Agregar clase .scroll-to a link para aimate scroll a elemento con x id
			$(".scroll-to").unbind("click").click(function(e){
				e.preventDefault();
				var ms = $(this).data("ms") || null;
				var selector = $(this).data("href") || $(this).attr("href") || null;
				var easing = $(this).data("easing") || null;
				var offset = $(this).data("offset") || null;
				Solid.scrollTo(selector, ms, easing, offset);
			});
			//<a class="scroll-to" data-ms="1100" data-easing="easeOutCirc" href="#contacto">CONTACTO</a>
		}

		// Modals genéricos
		if(opciones.solidModals){
			Solid.setModals();
		}

		// Animaciones en viewport
		if(opciones.wow){
			new WOW().init();
		}

		// Agrega clase a <body> para saber si es touch o no el device
		if(opciones.touchClass){
			if(Solid.isTouch()){
				$("body").addClass("hey-touch");
				$(".hvr-grow").removeClass("hvr-grow");
				$(".hvr-float").removeClass("hvr-float");
			} else {
				$("body").addClass("hey-no-touch");
			}
		}

		if(opciones.parallax){
			// ESTA FUNCION LA HICE PARA LLAMARLA CON AJAX Y RESETEAR PARALLAX QUE HAYA QUE RESETEAR
			// primero borro los q ya estaban
			$(".parallax-mirror").remove();
			// aca estoy reseteando manualmente (o programaticamente)
			$('.parallax').each(function(){
				var path = $(this).attr("data-image-src");
				$(this).parallax({ imageSrc: path });
			});
        }
        
        if(opciones.solidValidate){
			$("form.solid-validate").each(function(){
				var $form = $(this);
				$form.unbind("validate").validate({
					submitHandler: function (form) {
						console.log("form submit", form);
						Solid.submitFormSolidValidate(form, opciones);
					}
				});
			});
        }
	},

	setMedidas: function(){
        this.medidas.W = $(window).width();
        this.medidas.H = $(window).height();
	},
	
    ajaxForm: function(form, callback_success, callback_error){
		$.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            dataType: 'json',
            // contentType: "application/json",
            success: function(response) {
                console.log("submitForm success", response);
				if(typeof(callback_success)==='function') callback_success(response);
            }, 
            error: function(error){
                console.log("submitForm error", error);
				if(typeof(callback_error)==='function') callback_error(error);
            }            
        });
	},
    
    // Funcion para formulario ajax
    submitFormSolidValidate: function(form, opciones){

		var $form = $(form);
		var submitOriginalVal = $form.find('*[type=submit]').val();

		if(opciones.solidValidateAuto){
			Solid.showLoadingFormAuto($form);
		} else {
			opciones.showLoadingForm(form);
		}
		
		Solid.ajaxForm(form, function(resp){
			console.log("submitFormSolidValidate response", resp);
			if(resp.success){
				Solid.abrirModal(null, "Te corresponderemos a la brevedad.", "¡Mensaje enviado!");
			} else {
				Solid.abrirModal(null, resp.mensaje, "Hubo un error");
			}
			
			if(opciones.solidValidateAuto){
				Solid.hideLoadingFormAuto($form, submitOriginalVal);
				Solid.resetFormAuto($form);
			} else {
				opciones.hideLoadingForm(form);
				opciones.resetForm(form);
			}

		}, function(err){
			console.log("error submitFormSolidValidate", err);
			if(opciones.solidValidateAuto){
				Solid.hideLoadingFormAuto($form, submitOriginalVal);
			} else {
				opciones.hideLoadingForm(form);
			}
			Solid.abrirModal(null, 'Please try again later', "There was an error");
		});
    },
    
    showLoadingFormAuto: function($form){
		var submitLoadingVal = $form.attr('data-loading-val');
		if(submitLoadingVal === undefined || submitLoadingVal === null || submitLoadingVal.length === 0 ){
			var submitLoadingVal = 'Enviando...';
		}
        $form.find("button[type=submit], input[type=submit]").attr("disabled", "disabled").addClass("loading").val(submitLoadingVal);
    },
    
    hideLoadingFormAuto: function($form, submitOriginalVal){
		if(submitOriginalVal === undefined || submitOriginalVal === null || submitOriginalVal.length === 0 ){
			var submitOriginalVal = 'Enviar';
		}
        $form.find("button[type=submit], input[type=submit]").removeAttr("disabled").removeClass("loading").val(submitOriginalVal);
    },
    
    resetFormAuto: function($form){
		$form.find('input[type=text], input[type=email], input[type=tel], select, textarea').val('');
    },

    // Funcion que se llama siempre q carga una nueva pagina
	setMisc: function(){
		//$('.datepicker').datepicker();
	},

    // Animaciones propias in scroll view
	setHeywow: function(settings){
		$(window).scroll(function(){
			Solid.checkHeywow(settings);
		});
	},
	checkHeywow: function(settings){
		if(settings && settings.className && settings.className.length){
			$("."+settings.className).each(function(){
				if($(this).is(":in-viewport")){
					$(this).addClass(settings.classIn);
				} else {
					if(settings.reset) $(this).removeClass(settings.classIn);
				}
			});
		}
	},

    showHiddenEmail: function(selector, username, dominio, dom){
		var r = 'mailto:' + username + '@' + dominio + dom;
		if($(selector).length) $(selector).attr('href',r).html(username + '@' + dominio + dom);
    },

	preload: function(imgArray, callback_final, callback_siempre){
		if(imgArray && typeof imgArray !== 'undefined'){
			if(imgArray && imgArray.length>0){
				var img_count = imgArray.length;
			    //$log = $('#loading .log').val(''); //$progress = $('#loading .info').text('0 / '+img_count); 
				loader = new PxLoader(); 
				for(i in imgArray){
					var pxImage = new PxLoaderImage(imgArray[i]); pxImage.imageNumber = i + 1; loader.add(pxImage);
				}
				loader.addProgressListener(function(e) { // callback that runs every time an image loads 
					if(typeof callback_siempre === "function") callback_siempre();
				    //$log.val($log.val() + 'Image ' + e.resource.imageNumber + ' Loaded\r'); // log which image completed 
				    //$progress.text(e.completedCount + ' / ' + e.totalCount); // the event provides stats on the number of completed items
				});
				loader.addCompletionListener(function() {
					if(typeof callback_final === "function") callback_final();
				});  
				loader.start(); 
			} else {
				if(typeof callback_final === "function") callback_final();
			}
		} else {
			if(typeof callback_final === "function") callback_final();
		}
	},

    setAjaxNav: function(opciones){
    	
    },

	loadWebsite: function(opciones){
		if(opciones.heywow){
			Solid.setHeywow(opciones.heywowSettings);
			setTimeout(function(){
				Solid.checkHeywow(opciones.heywowSettings);
				if(typeof(opciones.ajaxNavCallback) === 'function'){
					opciones.ajaxNavCallback();
				}
			}, 100);
		}
		Solid.hideLoading();
	},

	showLoading: function(){
		$("#loading").fadeIn(144, function(){
			//$(this).remove();
			// $("#loading").addClass("ajax");
		});
	},

	hideLoading: function(){
		$("#loading").fadeOut(144, function(){
			//$(this).remove();
			$("#loading").addClass("ajax");
		});
	},

    setHeaderChivo: function(){
		
		var scrollPosition = 20;

		$(window).scroll(function(){
		  	if($(document).scrollTop() > scrollPosition){
				$("#header").addClass("compact");
				$(".page").addClass("compact");
			} else {
				$("#header").removeClass("compact");
				$(".page").removeClass("compact");
			}
		});
    },

    stickyNav: function(){
    	var selector = ".sticky-elem";
    	if($(selector).length){
	    	var stickyNavTop = $(selector).offset().top;
			var scrollTop = $(window).scrollTop();
			console.log(scrollTop, stickyNavTop);
			if (scrollTop > stickyNavTop) { 
			    $(selector).addClass('sticky');
			} else {
			    $(selector).removeClass('sticky'); 
			}
		}
    },

    setSticky: function(){
    	console.log("Setting Sticky");
		Solid.stickyNav();
		$(window).scroll(function() {
		  	Solid.stickyNav();
		});
    },

    openMenu: function(){
		$("#menu, #menu-overlay").addClass("opened");
		Solid.menuOpened = true;
	},

	closeMenu: function(){
		$("#menu, #menu-overlay").removeClass("opened");
		Solid.menuOpened = false;
	},

    setMenuAnimado: function(menuAnimadoSide){
    	$(".menu-btn").unbind("click").click(function(e){
			e.preventDefault();
			if(Solid.menuOpened){
				Solid.closeMenu();
			} else {
				Solid.openMenu();
			}
		});

		$("#menu-overlay").unbind("click").click(function(e){
			Solid.closeMenu();
		});

		// Menu Animado
		$("#sidemenu_btn, #sidemenu").addClass(menuAnimadoSide);
		$("#sidemenu_btn, #sidemenu_overlay").unbind("click").click(function(e){
			e.preventDefault();
			toggleOpen();
		});
		$("#sidemenu .sidemenu-nav li a").not('.void').click(function(){
			toggleOpen();
			var li = $(this).closest("li");
			$('#sidemenu li').removeClass('active');
			li.addClass("active");
		});
		$("#sidemenu .sidemenu-nav .void").click(function(e){
			e.preventDefault();
		});

		function toggleOpen(){
			$("#sidemenu_overlay").toggleClass("open");
			$("#sidemenu_btn").toggleClass("open");
			$("#sidemenu").toggleClass("open");
		}

    },

    setModals: function(){
    	$(".alerta .cerrar").unbind("click").click(function(e){
			e.preventDefault();
			Solid.cerrarModal($(this).closest(".alerta"));
		});
    },

    abrirModal: function(tipo, mensaje, titulo){
    	if(!tipo) var tipo = 1; // alerta normal

    	if(mensaje && mensaje.length > 0){
    		var alerta = $("<div class='alerta'></div>");
			var html = '<span class="cerrar fa fa-remove"></span>';
			if(titulo && titulo.length > 0 ){
				html += '<p class="titulo">'+titulo+'</p>';
			}
			html += '<p class="descripcion">'+mensaje+'</p>';

			alerta.html(html);

			$("body").append(alerta).promise().done(function(){
				alerta.fadeIn(144);
				Solid.setModals();
			});
		}
    },

    cerrarModal: function(modal){
    	modal.fadeOut(144, function(){
			if(!modal.hasClass("persistent")) modal.remove();
		});
    },

	// HACER UN SCROLL ANIMADO A UN ELEMENTO DEL DOM
	scrollTo: function(selector, ms, easing, offset, callback){
		if(selector.length > 0){
			if(offset===null) var offset = 20;
			if(ms===null) var ms = 2000;
			if(easing===null) var easing = "easeOutQuart";

			var scrolltop = $(selector).offset().top;
			//if(scrolltop!==undefined && offset>0) scrollTop += offset;

			$('html, body').animate({
				//width: [ "toggle", "swing" ],
				//height: [ "toggle", "swing" ]
				//scrollTop: [scrolltop, "easeOutBounce"]
				scrollTop: scrolltop
			}, ms, easing, function() {
				if(typeof(callback) === "function"){
					callback();
				}				
			});
		}
	},
    isTouch: function () {
        if(Browser.browser==="Firefox"){
			if(Modernizr.touchevents===true){
				//console.log("Dispositivo touch (Firefox)");
				return true;
			} else {
				//console.log("Dispositivo no touch (Firefox)");
				return false;
			}
			
		} else {
			if(Modernizr.touch===true){
				//console.log("Dispositivo touch");
				return true;
			} else {
				//console.log("Dispositivo no touch");
				return false;
			}
		}
    },
	
	validateEmail: function(email){
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	},
	
	initGoogleMap: function(mapaId, markers, zoom){
		console.log('iniciando mapa', mapaId, markers, zoom)
		if(markers.constructor !== Array){
			markers = [markers];
		}
		if(!zoom || zoom === undefined || zoom === null){
			zoom = 16;
		}

		var first = markers[0];
		var uluru = { lat: first.lat, lng: first.lng };

		var map = new google.maps.Map(document.getElementById(mapaId), {
			zoom: zoom,
			center: uluru,
			styles: [
				{
					"featureType": "administrative",
					"elementType": "all",
					"stylers": [
						{
							"saturation": "-100"
						}
					]
				},
				{
					"featureType": "administrative.province",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": 65
						},
						{
							"visibility": "on"
						}
					]
				},
				{
					"featureType": "poi",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"lightness": "50"
						},
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"saturation": "-100"
						}
					]
				},
				{
					"featureType": "road.highway",
					"elementType": "all",
					"stylers": [
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "road.arterial",
					"elementType": "all",
					"stylers": [
						{
							"lightness": "30"
						}
					]
				},
				{
					"featureType": "road.local",
					"elementType": "all",
					"stylers": [
						{
							"lightness": "40"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "all",
					"stylers": [
						{
							"saturation": -100
						},
						{
							"visibility": "simplified"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"hue": "#ffff00"
						},
						{
							"lightness": -25
						},
						{
							"saturation": -97
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"lightness": "58"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels",
					"stylers": [
						{
							"lightness": -25
						},
						{
							"saturation": -100
						}
					]
				}
			]
		});
		// var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
		
		var bounds = new google.maps.LatLngBounds();
		var contentString;

		for(var v in markers){
			var actual = markers[v];
			//console.log('actual', actual);

			contentString = '<div id="content">'+
				'<div id="siteNotice">'+
				'</div>'+
				'<h1 id="firstHeading" class="firstHeading">'+actual.title+'</h1>'+
			'</div>';
			var infowindow = new google.maps.InfoWindow();

			var ubic = { lat: actual.lat, lng: actual.lng };
			var marker = new google.maps.Marker({
				position: ubic,
				map: map,
				title: actual.title,
				info: contentString,
				icon: actual.icon
			});
			/* marker.addListener('click', function() {
				infowindow.open(map, marker);
			}); */
			google.maps.event.addListener(marker, 'click', function () {
				infowindow.setContent(this.info);
				infowindow.open(map, this);
			});
	  
			if(markers.length > 1){
				bounds.extend(ubic);
			}
		}
		
		if(markers.length > 1){
			map.fitBounds(bounds);
		} else {
			map.setZoom(zoom);
			map.panTo(uluru);
		}

	}

};

// **************************************************************************************************************************
// **************************************************************************************************************************
// **************************************************************************************************************************

// PARA DETERMINAR BROWSER ////////////////////////////////////////////////////////////////////////////////

var Browser = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;
            if (dataString.indexOf(data[i].subString) !== -1) return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index === -1) return;
        var rv = dataString.indexOf("rv:");
        if (this.versionSearchString === "Trident" && rv !== -1) {
            return parseFloat(dataString.substring(rv + 3));
        } else {
            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
        }
    },
    dataBrowser: [
        {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
        {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
        {string: navigator.userAgent, subString: "Safari", identity: "Safari"},
        {string: navigator.userAgent, subString: "Opera", identity: "Opera"}
    ]
};


// **************************************************************************************************************************
// **************************************************************************************************************************
// **************************************************************************************************************************

// Mierdas custom de JQUERY

$.fn.random = function() {
	return this.eq(Math.floor(Math.random() * this.length));
}   

jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requerido.",
    // required: "This field is required.",
    remote: "Please fix this field.",
    email: "Este email no es válido.",
    // email: "This is not a valid email.",
    url: "Este no es un URL válido.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Muy corto.")
});

jQuery.validator.addMethod("greaterThanZero", function(value, element) {
    return this.optional(element) || (parseFloat(value) > 0);
}, "* Este campo es requerido");


// **************************************************************************************************************************
// **************************************************************************************************************************
// **************************************************************************************************************************

// SNIPPETS:

/*

swal({
	title: "Are you sure?",
	text: "You will not be able to recover this imaginary file!",
	type: "warning",
	showCancelButton: true,
	confirmButtonColor: "#DD6B55",
	confirmButtonText: "Yes, delete it!",
	closeOnConfirm: false
},
function(){
	swal("Deleted!", "Your imaginary file has been deleted.", "success");
});

$(".clientes-slider").slick({
	slidesToShow: 8,
	slidesToScroll: 1,
	dots: false,
	arrows: false,
	infinite: true,
	autoplay: true,
	pauseOnHover: false,
	autoplaySpeed: 2000,
	responsive: [
	    {
	      breakpoint: 1000,
	      settings: {
	        slidesToShow: 4
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 3
	      }
	    }
	]
});

toastr.options = {
	"closeButton": false,
	"debug": false,
	"newestOnTop": false,
	"progressBar": false,
	"positionClass": "toast-bottom-left",
	"preventDuplicates": false,
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}
*/