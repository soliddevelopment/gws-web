/* heynav */

(function($){

	var History;
	
	$.fn.ajaxnav = function(options) {
	    
	    // DEFAULT SETTINGS
        var settings = $.extend({
			claseLink: "ajax-link", // esto queda quemado por ahora, ni modo
			idContainer: "content",
			delay: 0,
			beforeLoad: function(){},
			success: function(){}
		}, options);

	    // Seteo los clicks de cada link que aplique
	    this.each(function(){
	    	
	    	if($(this).hasClass(settings.claseLink)){
		    	$(this).on("click", function(e){
		    		e.preventDefault();
		    		setLinkClick(this);
		    	});
		    }
			
	    });

	    var triggerChange = function(){
	    	alert("ulaaaa");
	    };

	    var setLinkClick = function(link){
    		var href = $(link).attr("href");
    		var title = $(link).data("page-title");
    		History.pushState({ tipo: 1 }, title, href);
	    };

	    var cambiarPagina = function(href, title){

	    	if(typeof(settings.success)==='function') settings.beforeLoad();

	    	ajaxGet(href + "?ajax=true", function(data){

				setTimeout(function(){

					$("#"+settings.idContainer).html(data).promise().done(function(){

						// Hay que buscar las nuevas ajax-links agregadas, y setearles el click
						$("#"+settings.idContainer).find("."+settings.claseLink).each(function(){
							$(this).on("click", function(e){
					    		e.preventDefault();
					    		console.log("click sublink");
					    		setLinkClick(this);
					    	});
						});

						// Llamo callback success
						if(typeof(settings.success)==='function') settings.success();

					});

				}, settings.delay);

    		}, function(data){
    			console.log("error ajaxnav", data);
    		});
	    }

	    var configHistory = function(){
	    	History = window.History;
			History.Adapter.bind(window,'statechange',function(){ 
			    var state = History.getState(); 
			    console.log("state", state);
			    // if(state.data && state.data.tipo && state.data.tipo === 1){
			    	cambiarPagina(state.url, state.title);
			    // }
			});

	    }

	    var ajaxGet = function(url, success, failure){
	    	$.ajax({
	            type: "GET",
	            url: url,
	            dataType: "HTML",
	            cache: false,
	            success: function (data, textStatus, jqXHR) {
	                if(typeof(success)==='function') success(data);
	            },
	            error: function (response, text, errorThrown) {
	                if (errorThrown == "Internal Server Error") {
	                    try {
	                        //este es un error interno del servidor. Problemas en el codigo!
	                        //error de programacion... O errores custom como falta de permisos etc....
	                        var objetoRespuesta = JSON.parse(response.responseText);
	                        failure(objetoRespuesta);
	                    }
	                    catch (err) {
	                        //este error es cuando no encuentra el web service generalmente cuando el web servidor ha fallado
	                        // $error.MostrarError("<b>Ha ocurrido un error en la comunicación con el servidor.</b><br/>Generalmente este error se debe a problemas de red. Recargue la página e intente de nuevo. Si no se puede restablecer la comunicación en 10 segundos sus conversaciones se asignaran a otro operador.");
	                        failure(null);
	                    }
	                } else {
	                    //este error es cuando hay un problema del lado del cliente generalmente cuando no hay internet
	                    //$error.MostrarError("<b>Ha ocurrido un error en la comunicación con el servidor.</b><br/>Generalmente este error se debe a problemas de red. Recargue la página e intente de nuevo. Si no se puede restablecer la comunicación en 10 segundos sus conversaciones se asignaran a otro operador.");
	                    failure(null);
	                }
	            }
	        });
	    }

	    configHistory();

	};
	 
}(jQuery, History));