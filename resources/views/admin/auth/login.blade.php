<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="solid.com.sv">

        <link rel="shortcut icon" href="{{asset('a/css/lib/images/favicon_1.ico')}}">

        <title>Administrador de contenidos</title>

        <link rel="stylesheet" href="{{asset('a/css/lib/bootstrap.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/lib/icons.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/core.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/components.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/pages.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/menu.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/responsive.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/custom/ajustes.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/custom/login.css')}}" type="text/css">

        <script type="text/javascript" src="{{asset('a/js/lib/modernizr.min.js')}}"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>

        <div class="wrapper-page login-page">

            {{-- <div class="text-center">
                <a href="/" class="logo">
                    <i class="md md-equalizer"></i> <span>Solid CMS</span>
                </a>
            </div> --}}

            <form class="form-horizontal m-t-20" action="" method="post" action="{{ action('Admin\Auth\LoginController@login') }}">

                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input id="email" placeholder="Email o usuario" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        <i class="md md-account-circle form-control-feedback l-h-34"></i>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input id="password" placeholder="Contraseña" type="password" class="form-control" name="password" required>
                        <i class="md md-vpn-key form-control-feedback l-h-34"></i>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="pull-right submit-cont">
                            <button type="submit" class="btn btn-primary">
                                Iniciar sesión
                            </button>
                        </div>
                        <div class="checkbox checkbox-primary">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <div class="col-md-8 col-md-offset-4">
                        

                        {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a> --}}
                    </div>
                </div>

            </form>
        </div>
    
        <script>
            var resizefunc = [];
        </script>

        <!-- Main  -->
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/detect.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/fastclick.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.slimscroll.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.blockUI.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/waves.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/wow.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.nicescroll.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.scrollTo.min.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('a/js/pages/jquery.core.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/pages/jquery.app.js')}}"></script>

    </body>
</html>