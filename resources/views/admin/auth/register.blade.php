<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="solid.com.sv">

        <link rel="shortcut icon" href="{{asset('a/css/lib/images/favicon_1.ico')}}">

        <title>Administrador de contenidos</title>

        <link rel="stylesheet" href="{{asset('a/css/lib/bootstrap.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/core.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/icons.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/components.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/pages.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/menu.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('a/css/pages/responsive.css')}}" type="text/css">
        {{-- <link rel="stylesheet" href="{{asset('css/custom/ajustes.css')}}" type="text/css"> --}}

        <script type="text/javascript" src="{{asset('a/js/lib/modernizr.min.js')}}"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>

        <div class="wrapper-page login-page">

            <div class="text-center">
                <a href="/" class="logo">
                    <i class="md md-equalizer"></i> <span>Solid CMS</span>
                </a>

            </div>

            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label for="nombre" class="col-md-4 control-label">Nombre</label>

                    <div class="col-md-6">
                        <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" required autofocus>

                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                    <label for="apellido" class="col-md-4 control-label">Apellido</label>

                    <div class="col-md-6">
                        <input id="apellido" type="text" class="form-control" name="apellido" value="{{ old('apellido') }}" required autofocus>

                        @if ($errors->has('apellido'))
                            <span class="help-block">
                                <strong>{{ $errors->first('apellido') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('usuario') ? ' has-error' : '' }}">
                    <label for="usuario" class="col-md-4 control-label">Usuario</label>

                    <div class="col-md-6">
                        <input id="usuario" type="text" class="form-control" name="usuario" value="{{ old('usuario') }}" required>

                        @if ($errors->has('usuario'))
                            <span class="help-block">
                                <strong>{{ $errors->first('usuario') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">Email</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Registrarse
                        </button>
                    </div>
                </div>
            </form>


        </div>
    
        <script>
            var resizefunc = [];
        </script>

        <!-- Main  -->
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/detect.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/fastclick.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.slimscroll.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.blockUI.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/waves.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/wow.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.nicescroll.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/lib/jquery.scrollTo.min.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('a/js/pages/jquery.core.js')}}"></script>
        <script type="text/javascript" src="{{asset('a/js/pages/jquery.app.js')}}"></script>

    </body>
</html>
