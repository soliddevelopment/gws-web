@extends('admin.main')

@section('title', 'Categorías de '.$grupo->titulo)

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.posts.index', ['grupo_id'=>$grupo->id]) }}">{{ $grupo->titulo }}</a></li>
                <li class="active">Categorías de {{ $grupo->titulo }}</li>
            </ol>
            <h4 class="page-title">Categorías de {{ $grupo->titulo }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="pull-right">
                {{-- <a href="{{ route('admin.subcategorias.index', ['grupo'=> $grupo->id]) }}" class="btn btn-primary">Subcategorías</a> --}}
                <a href="{{ route('admin.categorias.create', ['grupo'=> $grupo->id]) }}" class="btn btn-primary">+ Nueva</a>
            </div>
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de {{ $grupo->titulo }}</b>
            </h4> --}}
            
            <div class="row">
                <div class="col-md-12">
                	<table class="table table-striped">
                		<thead>
                			<tr>
                                <th></th>
                				<th>id</th>
                				<th>titulo</th>
                				<th>creado</th>
                				<th></th>
                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($categorias as $item)
	                			<tr>
                                    <td><a class="fancybox" href="{{ thumb($item->img, 800, 600) }}"><img src="{{ thumb($item->img, 50, 50) }}"></a></td>
	                				<td>{{ $item->id }}</td>
                                    <td>{{ $item->titulo }}</td>
	                				<td>{{ $item->created_at }}</td>
                                    <td class="align-center">
                                        <?php /*<input class="st-switch" data-id="1" data-url="st" type="checkbox" <?php if($elem->st == 1) echo "checked";  data-plugin="switchery" data-color="#3bafda" data-size="small" /> */ ?>
                                        <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.categorias.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->st == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                    </td>
                                    <td style="text-align:right;">
                                        <a href="{{ route('admin.subcategorias.index', ['categoria'=> $item->id]) }}" class="btn btn-primary btn-sm waves-effect waves-light">Subcategorías</a>
                                        <a href="{{ action('Admin\CategoriasController@edit', ['id' => $item->id]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                            Editar
                                        </a>

                                        {!! Form::open(['route'=>['admin.categorias.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                        {!! Form::close() !!}

                                        <a href="#" 
                                            data-method="delete" 
                                            data-title="¡Precaución!" 
                                            data-text="¿Estás seguro que quieres borrar esto?" 
                                            data-type="warning" 
                                            data-id-form="form_{{ $item->id }}"
                                            class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                            Borrar
                                        </a>
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection