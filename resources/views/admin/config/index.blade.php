@extends('admin.main')

@section('title', 'Configuración')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="/">Administrador</a></li>
                <li class="active">Config</li>
            </ol>
            <h4 class="page-title">Config</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <?php if($usuario->hasRole('Global')){ ?>
                <div class="pull-right">
                    <a href="{{ route('admin.config.create') }}" class="btn btn-primary">+ Nueva</a>
                </div>
            <?php } ?>
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de {{ $grupo->titulo }}</b>
            </h4> --}}
            
            <div class="row">
                <div class="col-md-12">

                    @if(count($configuraciones)>0)
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <?php if($usuario->hasRole('Global')){ ?>
                                        <th>id</th>
                                        <th>ONLY GLOBAL</th>
                                        <th>slug</th>
                                    <?php } ?>
                                    <th>título</th>
                                    <th>descripción</th>
                                    <th>tipo</th>
                                    <th class="align-center">valor</th>
                                        
                                    <?php if($usuario->hasRole('Global')){ ?>
                                        <th>creado</th>
                                    <?php } ?>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($configuraciones as $item)
                                    <tr>
                                        <?php if($usuario->hasRole('Global')){ ?>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->only_global }}</td>
                                            <th>{{ $item->slug }}</th>
                                        <?php } ?>
                                        <td>{{ $item->titulo }}</td>
                                        <td>{{ $item->descripcion }}</td>
                                        <td>{{ $item->tipo }}</td>
                                        <td class="align-center">
                                            @if($item->tipo == 'IMG')
                                                <a class="fancybox" href="{{ thumb($item->valor_img, 800, 600) }}"><img src="{{ thumb($item->valor_img, 44, 44) }}"></a>
                                            @elseif($item->tipo == 'STR')
                                                <strong>{{$item->valor_str}}</strong>
                                            @elseif($item->tipo == 'INT')
                                                <strong>{{$item->valor_int}}</strong>
                                            @elseif($item->tipo == 'BOOL')
                                                <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.config.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->valor_bool == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                            @endif
                                        </td>
                                        
                                        <?php if($usuario->hasRole('Global')){ ?>
                                            <td>
                                                {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                            </td>
                                        <?php } ?>
                                        <td style="text-align:right;">
                                            <a href="{{ route('admin.config.edit', [ 'id' => $item->id  ]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                                Editar
                                            </a>
                                            &nbsp;

                                            {!! Form::open(['route'=>['admin.config.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}
                                            
                                            <?php if($usuario->hasRole('Global')){ ?>
                                                <a href="#" 
                                                    data-method="delete" 
                                                    data-title="¡Precaución!" 
                                                    data-text="¿Estás seguro que quieres borrar esto?" 
                                                    data-type="warning" 
                                                    data-id-form="form_{{ $item->id }}"
                                                    class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                                    Borrar
                                                </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div style="padding:20px 0;">
                            <h2>No hay configuraciones disponibles</h2>
                        </div>
                    @endif

                    {{-- {!! $proyectos->links() !!} --}}
                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection