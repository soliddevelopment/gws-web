@extends('admin.main')

@section('title', 'Detalle de contacto')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.contactos.index') }}">Contactos</a></li>
                <li class="active">Detalle de contacto</li>
            </ol>
            <h4 class="page-title">Detalle de contacto</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div class="card-box">
            
            <div class="detalle-contacto">
                @if(strlen($contacto->nombre) > 0)
                    <div class="elem">
                        <span class="dato">Nombre:</span>
                        <span class="valor">
                            {{$contacto->nombre}}
                            @if(strlen($contacto->apellido)>0)
                                {{$contacto->apellido}}
                            @endif
                        </span>
                    </div>
                @endif
                
                @if(strlen($contacto->empresa) > 0)
                    <div class="elem">
                        <span class="dato">Empresa:</span>
                        <span class="valor">
                            {{$contacto->empresa}}
                        </span>
                    </div>
                @endif
                
                @if(strlen($contacto->email) > 0)
                    <div class="elem">
                        <span class="dato">Email:</span>
                        <span class="valor">
                            {{$contacto->email}}
                        </span>
                    </div>
                @endif
                
                @if(strlen($contacto->telefono) > 0)
                    <div class="elem">
                        <span class="dato">Tel:</span>
                        <span class="valor">
                            {{$contacto->telefono}}
                        </span>
                    </div>
                @endif
                
                @if(strlen($contacto->mensaje) > 0)
                    <div class="elem">
                        <span class="dato">Mensaje:</span>
                        <span class="valor">
                            {{$contacto->mensaje}}
                        </span>
                    </div>
                @endif

            </div>

		</div>
	</div>
</div>

@section('scripts')
	{{-- <script type="text/javascript" src="{{ mix('a/js/custom/editarPost.js') }}"></script> --}}
@endsection

@endsection