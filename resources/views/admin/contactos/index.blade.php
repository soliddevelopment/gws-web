@extends('admin.main')

@section('title', 'Lista de contactos')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li class="active">Contactos</li>
            </ol>
            <h4 class="page-title">Contactos</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="pull-right">
                {{-- <a href="{{ route('admin.contactos.create') }}" class="btn btn-primary">+ Nuevo</a> --}}
            </div>
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de {{ $grupo->titulo }}</b>
            </h4> --}}
            
            <div class="row">
                <div class="col-md-12">
                	<table class="table table-striped">
                		<thead>
                			<tr>
                				<th>id</th>
                				<th>nombre</th>
                				<th>email</th>
                				<th>teléfono</th>
                				<th>empresa</th>
                				<th>mensaje</th>
                				<th>fecha</th>
                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($contactos as $item)
	                			<tr>
	                				<td>{{ $item->id }}</td>
                                    <td>
                                        {{ $item->nombre }}
                                        @if(strlen($item->apellido)>0)
                                            {{$item->apellido}}
                                        @endif
                                    </td>
	                				<td>{{ $item->email }}</td>
	                				<td>{{ $item->teléfono }}</td>
	                				<td>{{ $item->empresa }}</td>
	                				<td>{{ $item->mensaje }}</td>
	                				<td>
                                        {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                    </td>
                                    <td><a href="{{route('admin.contactos.detalle', ['id'=>$item->id])}}">Ver detalle</a></td>
                                    <td style="text-align:right;">
                                        {!! Form::open(['route'=>['admin.contactos.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                        {!! Form::close() !!}

                                        <a href="#" 
                                            data-method="delete" 
                                            data-title="¡Precaución!" 
                                            data-text="¿Estás seguro que quieres borrar esto?" 
                                            data-type="warning" 
                                            data-id-form="form_{{ $item->id }}"
                                            class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                            Borrar
                                        </a>
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>

                    {{-- {!! $contactos->links() !!} --}}

                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection