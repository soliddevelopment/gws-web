<div class="form-group">
    {{ Form::label('nombre', 'Nombre:') }}
    {{ Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'Ej. normales']) }}
</div>
<div class="form-group">
    {{ Form::label('titulo', 'Título:') }}
    {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'Ej. Posts normales']) }}
</div>
<div class="form-group">
    {{ Form::label('tipo_id', 'Tipo:') }}
    <select name="tipo_id" id="tipo_id" class="form-control">
        @foreach($tipos as $item)
            <option value="{{ $item->id }}" {{ $grupo != null && $item->id == $grupo->tipo_id ? 'selected':'' }}>{{ $item->titulo }}</option>
        @endforeach
    </select>
</div>

<div class="btn-cont">
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>
