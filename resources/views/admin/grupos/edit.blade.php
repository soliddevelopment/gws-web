@extends('admin.main')

@section('title', $title)

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ action('Admin\GruposController@index') }}">Grupos</a></li>
                {{--  <li class="active">Editar</li>  --}}
            </ol>
            <h4 class="page-title">Editar grupo</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div class="card-box">
            {{-- <div class="pull-right">
                <a href="" class="btn btn-primary">+ Nuevo</a>
            </div> --}}
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de grupos</b>
			</h4> --}}
			
			@if($grupo != null)
				{!! Form::model($grupo, ['route'=>['admin.grupos.update', $grupo->id], 'method'=>'PUT' ]) !!}
					@include('admin.grupos._form')
				{!! Form::close() !!}
			@endif

			@if($grupo == null)
				{!! Form::open(['route'=>['admin.grupos.store', null], 'method'=>'post']) !!}
					@include('admin.grupos._form')
				{!! Form::close() !!}
			@endif


		</div>
	</div>
</div>

@endsection