@extends('admin.main')

@section('title', 'Lista de grupos')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li class="active">Grupos de posts</li>
            </ol>
            <h4 class="page-title">Grupos de posts</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="pull-right">
                <a href="{{ route('admin.grupos.create') }}" class="btn btn-primary">+ Nuevo</a>
            </div>
            <h4 class="m-t-0 header-title">
                <b>Lista de grupos</b>
            </h4>
            
            <div class="row">
                <div class="col-md-12">
                	<table class="table table-striped">
                		<thead>
                			<tr>
                				<th>id</th>
                				<th>tipo</th>
                                <th>nombre</th>
                				<th>título</th>
                				<th></th>
                				<th></th>
                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($grupos as $item)
	                			<tr>
	                				<td>{{ $item->id }}</td>
	                				<td>{{ $item->tipo_id }}</td>
	                				<td>{{ $item->nombre }}</td>
	                				<td>{{ $item->titulo }}</td>
                                    <td class="align-center">
                                        <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.grupos.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->st == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                    </td>
                                    <td>
{{--                                        <a href="{{ action( 'Admin\MappingsController@edit', ['grupo_id' => $item->id, null] ) }}">Mappings</a>--}}
                                        <a href="{{ route( 'admin.grupos.mappings.edit2', ['grupo_id' => $item->id] ) }}">Mappings</a>
                                    </td>
                                    <td style="text-align:right;">
                                        <a href="{{ action('Admin\GruposController@edit', ['id' => $item->id]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                            Editar
                                        </a>
                                        &nbsp;

                                        {!! Form::open(['route'=>['admin.grupos.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                        {!! Form::close() !!}

                                        <a href="#" 
                                            data-method="delete" 
                                            data-title="¡Precaución!" 
                                            data-text="¿Estás seguro que quieres borrar esto?" 
                                            data-type="warning" 
                                            data-id-form="form_{{ $item->id }}"
                                            class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                            Borrar
                                        </a>
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection