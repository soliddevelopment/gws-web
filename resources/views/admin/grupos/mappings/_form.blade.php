<div id="grupo_mappings_page" class="row">
    <div class="col-sm-3">
        <h3>Columnas:</h3>
        @foreach($columns as $col)
            <div class="columnas">
                <label>
                    <span>{{ $col['campo'] }}</span>
                    <input type="checkbox" class="cb" data-campo="{{ $col['campo'] }}" name="map_{{ $col['campo'] }}" {{ $col['existe'] === true ? "checked" : "" }}>
                </label>
            </div>
        @endforeach
    </div>
    <div class="col-sm-9">
        {{-- <div class="card-box"> --}}
            <div class="lista-mappeada">

                @foreach($columns as $col)
                    <div id="formGroup_{{ $col['campo'] }}" class="elem">
                        <div class="card-box">
                            <div class="form-group">
                                <p class="campo">{{ $col['campo'] }}</p>
                                <div class="inputs">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control input-titulo" placeholder="Título" name="map_{{ $col['campo'] }}_titulo" value="{{ $col['titulo'] }}">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control input-placeholder" placeholder="Placeholder" name="map_{{ $col['campo'] }}_placeholder" value="{{ $col['placeholder'] }}">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control input-helptext" placeholder="Help text" name="map_{{ $col['campo'] }}_helptext" value="{{ $col['helptext'] }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <select name="map_{{ $col['campo'] }}_requerido" class="form-control input-requerido">
                                                <option value="1" <?php if($col['requerido'] && $col['requerido']==1) echo 'selected'; ?>>Requerido</option>
                                                <option value="0" <?php if($col['requerido']!=1) echo 'selected'; ?>>No requerido</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="number" class="form-control input-maxcharlength" placeholder="Max. caracteres" name="map_{{ $col['campo'] }}_max_char_length" value="{{ $col['max_char_length'] }}">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <p class="no-hay no-hay-mappings">Chequear columnas a la izquierda para iniciar mappeo...</p>

                <div class="btn-cont btn-cont-mappings">
                    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
                </div>

            </div>
        {{-- </div> --}}
    </div>
</div>


<?php /*
<div class="columnas">
    @foreach($columns as $col)
        <div class="form-group">
            <div><label>{{ $col['campo'] }}</label></div>
            <div class="campos">
                <input type="checkbox" class="cb" name="map_{{ $col['campo'] }}" {{ $col['existe'] === true ? "checked" : "" }}>
                <div class="inputs">
                    <input type="text" class="form-control" placeholder="Título" name="map_{{ $col['campo'] }}_titulo" value="{{ $col['titulo'] }}">
                    <input type="text" class="form-control" placeholder="Placeholder" name="map_{{ $col['campo'] }}_placeholder" value="{{ $col['placeholder'] }}">
                    <input type="text" class="form-control" placeholder="Help text" name="map_{{ $col['campo'] }}_helptext" value="{{ $col['helptext'] }}">
                    <select name="map_{{ $col['campo'] }}_requerido" class="form-control">
                        <option value="1" <?php if($col['requerido'] && $col['requerido']==1) echo 'selected'; ?>>Requerido</option>
                        <option value="0" <?php if($col['requerido']!=1) echo 'selected'; ?>>No requerido</option>
                    </select>
                </div>
            </div>
        </div>
    @endforeach
</div>
*/ ?>



<script>
    /*$(document).ready(function(){
        $('.campos .inputs input.form-control').each(function(){
            var $campos = $(this).closest('.campos');
            var $cb = $campos.find('.cb');
            if($cb.is(":checked")){

            } else {
                $campos.find('.inputs select.form-control').val(0);
                $campos.find('.inputs input.form-control').val('');
                $campos.find('.inputs .form-control').attr('disabled', 'disabled');
            }
        });
        
        $('.campos .cb').change(function(){
            var $cb = $(this);
            var $campos = $cb.closest('.campos');
            if(this.checked){
                $campos.find('.inputs .form-control').removeAttr('disabled');
            } else {
                $campos.find('.inputs input.form-control').val('');
                $campos.find('.inputs select.form-control').val(0);
                $campos.find('.inputs .form-control').attr('disabled', 'disabled');
            }
        });
    });*/
</script>

{{--  <div class="form-group">
    {{ Form::label('campo', 'Campo:') }}
    {{ Form::text('campo', null, ['class'=>'form-control', 'placeholder'=>'Ej. simple_tags']) }}
</div>
<div class="form-group">
    {{ Form::label('titulo', 'Título:') }}
    {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'Ej. Puesto']) }}
</div>
<div class="form-group">
    {{ Form::label('helptext', 'Help text:') }}
    {{ Form::text('helptext', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('placeholder', 'Placeholder:') }}
    {{ Form::text('placeholder', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>  --}}