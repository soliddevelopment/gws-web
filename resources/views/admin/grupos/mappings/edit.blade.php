@extends('admin.main')

@section('title', $title)

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ action('Admin\GruposController@index') }}">Grupos</a></li>
                {{--  <li class="active">Editar</li>  --}}
            </ol>
            <h4 class="page-title">Editar mapping</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        {!! Form::open(['route'=>['admin.grupos.mappings.store', $grupo->id, null], 'method'=>'post']) !!}
            @include('admin.grupos.mappings._form')
        {!! Form::close() !!}

	</div>
</div>

@endsection

@section('view_scripts')
    <script type="text/javascript" src="{{mix('a/js/view/grupos-mappings-posts.js')}}"></script>
@endsection