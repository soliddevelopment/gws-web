@extends('admin.main')

@section('title', 'Lista de imágenes')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ $breadcrumbAction }}">{{ $grupo->titulo }}</a></li>
                <li class="active">Imágenes</li>
            </ol>
            <h4 class="page-title">Imágenes <small class="text-muted">Peso máximo 3 mb, Formatos: jpg, png</small></h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 portlets">

        <div class="card-box">
            <div class="galeria-cont">
                <div class="galeria-actual ui-sortable">
                    @foreach($imgs as $elem)
                        <div class="img ui-sortable-handle" data-id="{{ $elem->id }}" id="img_{{ $elem->id }}">
                            <a href="{{ thumb($elem->img, 1200, 900) }}" class="fancybox"><img width="84" height="84" src="{{ thumb($elem->img, 84, 84) }}"></a>
                            <a href="#" class="borrar">Borrar</a>
                        </div>
                    @endforeach
                </div>

                <form 
                    action="{{ $imgUploadAction }}" 
                    data-url-borrar="{{ action('Admin\ImgsController@destroy') }}" 
                    data-url-orden="{{ action('Admin\ImgsController@sort') }}" data-id="4" class="galeria-dropzone dropzone dz-clickable">
                    <div class="dz-default dz-message"><span>Arrastra aquí las imágenes que deseas subir</span></div>
                </form>

                <div class="clearfix submit-cont">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="galeria-progress progress progress-striped active">
                                <div style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar" class="progress-bar progress-bar-primary">
                                    <span class="sr-only">40% Complete (success)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 align-right">
                            <button class="reset btn btn-primary waves-effect waves-light">Reset</button>
                            <button class="guardar btn btn-primary waves-effect waves-light" name="submit_slides">Subir imágenes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input name="id" value="4" type="hidden">
        
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection