
<div class="form-group">
    {{ Form::label('latitude', 'Latitud:') }}
    {{ Form::text('latitude', null, ['class'=>'form-control', 'placeholder'=>'Ej. 13.678495', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('longitude', 'Longitud:') }}
    {{ Form::text('longitude', null, ['class' => 'form-control', 'placeholder' => 'Ej. -89.270460', 'required']) }}
</div>
<div class="form-group">
    {{ Form::label('nombre', 'Nombre:') }}
    {{ Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('descripcion', 'Descripción:') }}
    {{ Form::textarea('descripcion', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows'=>3]) }}
</div>
<hr>

<hr>

<div class="btn-cont">
    {{-- {{ Form::hidden('grupo_id', $grupo->id) }} --}}
    {{-- {{ Form::hidden('metas_nuevos', $post->grupo_id, ['id'=>'metas_nuevos']) }} --}}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>