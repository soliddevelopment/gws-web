@extends('admin.main')

@section('title', 'Lista de locations')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="/">Administrador</a></li>
                <li class="active">Locations</li>
            </ol>
            <h4 class="page-title">Locations</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="pull-right">
                <a href="{{ route('admin.locations.script') }}" class="btn btn-primary">Subir script</a>
                <a href="{{ route('admin.locations.create') }}" class="btn btn-primary">+ Nuevo</a>
            </div>
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de {{ $grupo->titulo }}</b>
            </h4> --}}
            
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped lista-work">
                		<thead>
                			<tr>
                				<th>id</th>
                				<th>nombre</th>
                				<th>latitud</th>
                				<th>longitud</th>
                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($locations as $item)
	                			<tr id="elem_{{$item->id}}">
	                				<td># {{ $item->id }}</td>
                                    <td>{{ $item->nombre }}</td>
                                    <td>{{ $item->latitude }}</td>
                                    <td>{{ $item->longitude }}</td>
                                    <td style="text-align:right;">
                                        <a href="{{ route('admin.locations.edit', [ 'id' => $item->id  ]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                            Editar
                                        </a>
                                        &nbsp;

                                        {!! Form::open(['route'=>['admin.locations.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                        {!! Form::close() !!}

                                        <a href="#" 
                                            data-method="delete" 
                                            data-title="¡Precaución!" 
                                            data-text="¿Estás seguro que quieres borrar esto?" 
                                            data-type="warning" 
                                            data-id-form="form_{{ $item->id }}"
                                            class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                            Borrar
                                        </a>
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>

                    {{-- {!! $locations->links() !!} --}}

                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection