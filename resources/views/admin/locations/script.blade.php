@extends('admin.main')

@section('title', 'Script')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.locations.index') }}">Locations</a></li>
                <li class="active">Script</li>
            </ol>
            <h4 class="page-title">Script</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div class="card-box">
            {{-- <div class="pull-right">
                <a href="" class="btn btn-primary">+ Nuevo</a>
            </div> --}}
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de usuarios</b>
			</h4> --}}
			
            {!! Form::open(['route'=>['admin.locations.script.store', null], 'method'=>'post', 'files'=>true ]) !!}
                <h2>Subir archivo</h2>
                <p>El formato del archivo debe ser .json o .geojson. Para convertir un archivo KML a un .geojson: <a href="https://mygeodata.cloud/converter/kml-to-json" target="_blank">tool online</a> </p>
                
                <hr>
                <div class="form-group">
                    {{ Form::label('json', 'Archivo:') }}
                    {{ Form::file('json', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
                </div>
                <hr>

                <div class="form-group">
                    {{ Form::label('borrar_anteriores', '¿Qué desea hacer respecto a las ubicaciones ya guardadas?') }}
                    <select class="form-control" name="borrar_anteriores">
                        <option value="0">Borrar todas las anteriores (incluso las que fueron agregadas manualmente)</option>
                        <option value="1">Borrar solo las que fueron por medio de archivo</option>
                        <option value="2">No borrar nada</option>
                    </select>
                </div>

                <hr>

                <div class="btn-cont">
                    {{-- {{ Form::hidden('grupo_id', $grupo->id) }} --}}
                    {{-- {{ Form::hidden('metas_nuevos', $post->grupo_id, ['id'=>'metas_nuevos']) }} --}}
                    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
                </div>
                
            {!! Form::close() !!}

		</div>
	</div>
</div>

@section('scripts')
	{{-- <script type="text/javascript" src="{{ mix('a/js/custom/editarPost.js') }}"></script> --}}
@endsection

@endsection