<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="solid.com.sv">

    {{-- Esto me sirve para ajax calls --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Administrador | @yield('title')</title>

    @yield('stylesheets')
    @yield('header_scripts')

    {{-- {!! Html::style('css/app.css') !!} --}}

    <link rel="stylesheet" href="{{mix('a/css/lib/all.css')}}" type="text/css">
    <link rel="stylesheet" href="{{mix('a/css/pages/all.css')}}" type="text/css">
    <link rel="stylesheet" href="{{mix('a/css/custom/custom-admin.css')}}" type="text/css">
    
    <script type="text/javascript" src="{{mix('a/js/header.js')}}"></script>
    
</head>
{{-- @include('partials._head') --}}

<body class="fixed-left">

    <div id="wrapper">

        @include('admin.partials._top')
        @include('admin.partials._nav')

        <div class="content-page">
            <div class="content">
                <div class="container">
                    @yield('content')
                </div>
            </div>

            <footer class="footer text-right">
                &nbsp;
            </footer>
        </div>

    </div>
    
    @include('admin.partials._notifs')

    <script>
        // Esto es necesario para que funcione la navegacion del cms
        var resizefunc = [];
    </script>
    
    <script type="text/javascript" src="{{asset('a/js/lib/exif.js')}}"></script>
    <script type="text/javascript" src="{{asset('a/js/datatables.js')}}"></script>
    <script type="text/javascript" src="{{mix('a/js/footer.js')}}"></script>
    <script type="text/javascript" src="{{mix('a/js/custom.js')}}"></script>
    @yield('view_scripts')

</body>
</html>