@extends('main')

@section('title', 'Editar post: '.$post->titulo)

@section('content')
	
	<h1>{{ $post->titulo }}</h1>
	<p>{{ $post->body }}</p>

	{!! Form::model($post, ['route'=>['posts.update', $post->id], 'method'=>'PUT' ]) !!}

		<div class="form-group">
			{{ Form::label('titulo', 'Titulo:') }}
			{{ Form::text('titulo', null, ['class' => 'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('body', 'Body:') }}
			{{ Form::textarea('body', null, ['class' => 'form-control']) }}
		</div>

		<div class="btn-cont">
			{{ Form::submit('Save'), ['class' => 'btn btn-primary btn-lg'] }}
		</div>

	{!! Form::close() !!}

	<p>BORRAR</p>


	{!! Form::open(['route'=>['posts.destroy', $post->id], 'method'=>'DELETE' ]) !!}
		{{ Form::submit('Borrar'), ['class' => 'btn btn-primary btn-lg'] }}
	{!! Form::close() !!}

@endsection