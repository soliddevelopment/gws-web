{{-- <header id="header">
    <nav>
        <div id="nav" class="nav2 left">
            <ul>
                <li class="{{ Request::is('/') ? "active" : "" }}"><a href="/">inicio</a></li>
                <li class="{{ Request::is('about') ? "active" : "" }}"><a href="/about">about</a></li>
                <li class="{{ Request::is('posts') ? "active" : "" }}"><a href="/posts">posts</a></li>
                <li class="{{ Request::is('posts/create') ? "active" : "" }}"><a href="/posts/create">create post</a></li>
            </ul>
        </div>
    </nav>
</header> --}}

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">

            <ul>
                <li class="menu-title">NAVEGACIÓN</li>
                
                <li>
                    <a href="{{ route('admin.home.home') }}" class="waves-effect waves-primary {{ $controller=='HomeController' ? "active" : "" }}">
                        <i class="fa fa-home"></i><span> Home </span>
                    </a>
                </li>
                
                <?php if($usuario->hasRole('Global')){ ?>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect waves-primary {{ $controller=='GruposController' ? "active" : "" }}">
                            <i class="fa fa-folder"></i><span> Grupos </span><span class="menu-arrow"></span>
                        </a>
                        <ul class="list-unstyled" style="{{ $controller=='GruposController' ? "display:block" : "display:none" }}">
                            <li><a href="{{ action('Admin\GruposController@index') }}">Lista</a></li>
                            <li><a href="{{ route('admin.grupos.create') }}">+ Nuevo</a></li>
                        </ul>
                    </li>
                <?php } ?>
                
                <?php if($usuario->hasRole('Global') || $usuario->hasRole('Admin')){ ?>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect waves-primary {{ $controller=='UsuariosController' ? "active" : "" }}">
                            <i class="fa fa-user"></i><span> Usuarios </span><span class="menu-arrow"></span>
                        </a>
                        <ul class="list-unstyled" style="{{ $controller=='Admin\UsuariosController' ? "display:block" : "display:none" }}">
                            <li><a href="{{ route('admin.usuarios.index') }}">Lista</a></li>
                            <li><a href="{{ route('admin.usuarios.create') }}">+ Nuevo</a></li>
                        </ul>
                    </li>
                <?php } ?>

                @if($count_blog>0 && configVal($configs, 'blogs') == 1)
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect waves-primary {{ $controller=='PostsController' || $controller=='CategoriasController' || $controller=='SubcategoriasController' ? "active" : "" }}">
                            <i class="fa fa-file"></i><span> Contenidos </span><span class="menu-arrow"></span>
                        </a>
                        <ul class="list-unstyled" style="{{ $controller=='PostsController' || $controller=='CategoriasController' || $controller=='SubcategoriasController' ? "display:block" : "display:none" }}">
                            @foreach($grupos_catalogo as $item)
                                @if($item->tipo_id == 'BLOG')
                                    <li class="{{ isset($grupo) && $item->id == $grupo->id ? 'active':'' }}"><a href="{{ action('Admin\PostsController@index', ['grupo_id'=>$item->id]) }}">{{ $item->titulo }}</a></li>
                                @endif
                            @endforeach
                            <li><a href="{{ route('admin.locations.index') }}">Locations</a></li>
                        </ul>
                    </li>
                @endif
                    
                @if(configVal($configs, 'proyectos') == 1)
                
                    <?php if($usuario->hasRole('Global') || $usuario->hasRole('Admin')){ ?>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect waves-primary {{ $controller=='ProyectosController' ? "active" : "" }}">
                                <i class="fa fa-briefcase"></i><span> Work </span><span class="menu-arrow"></span>
                            </a>
                            <ul class="list-unstyled" style="{{ $controller=='ProyectosController' ? "display:block" : "display:none" }}">
                                <li><a href="{{ action('Admin\ProyectosController@index') }}">Lista</a></li>
                                <li><a href="{{ route('admin.proyectos.create') }}">+ Nuevo</a></li>
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect waves-primary {{ $controller=='ServiciosController' ? "active" : "" }}">
                                <i class="fa fa-list"></i><span> Servicios </span><span class="menu-arrow"></span>
                            </a>
                            <ul class="list-unstyled" style="{{ $controller=='ServiciosController' ? "display:block" : "display:none" }}">
                                <li><a href="{{ action('Admin\ServiciosController@index') }}">Lista</a></li>
                                <li><a href="{{ route('admin.servicios.create') }}">+ Nuevo</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                @endif

                @if(configVal($configs, 'contactos') == 1)
                    <li>
                        <a href="{{ route('admin.contactos.index') }}" class="waves-effect waves-primary {{ $controller=='ContactosController' ? "active" : "" }}">
                            <i class="fa fa-envelope"></i><span> Contactos </span>
                        </a>
                    </li>
                @endif
                
                <?php if($usuario->hasRole('Global') || $usuario->hasRole('Admin')){ ?>
                <li>
                    <a href="{{ route('admin.config.index') }}" class="waves-effect waves-primary">
                        <i class="fa fa-gear"></i><span> Configuración </span>
                    </a>
                </li>
                <?php } ?>
                
                <?php if($usuario->hasRole('Global')){ ?>
                    <li>
                        <a href="{{ route('admin.logs') }}" target="_blank" class="waves-effect waves-primary">
                            <i class="fa fa-exclamation-triangle"></i><span> Error logs </span>
                        </a>
                    </li>
                <?php } ?>
                
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
    </div>

    <div class="user-detail">
        <div class="dropup">
            <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true">
                <img src="{{ asset('a/css/lib/images/profile_small.png') }}" alt="user-img" class="img-circle">
                <span class="user-info-span">
                    <h5 class="m-t-0 m-b-0">{{ isset($usuario->usuario) ? $usuario->usuario : "" }}</h5>
                    <p class="text-muted m-b-0">
                        <small><i class="fa fa-circle text-success"></i> <span>Online</span></small>
                    </p>
                </span>
            </a>
            <ul class="dropdown-menu">
                {{-- <li><a href="javascript:void(0)"><i class="md md-face-unlock"></i> Profile</a></li>
                <li><a href="javascript:void(0)"><i class="md md-settings"></i> Settings</a></li>
                <li><a href="javascript:void(0)"><i class="md md-lock"></i> Lock screen</a></li> --}}
                <li><a href="{{ route('admin.auth.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="md md-settings-power"></i> Cerrar sesión</a></li>
            </ul>
        </div>
    </div>

    <form id="logout-form" action="{{ route('admin.auth.logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    
</div>
<!-- Left Sidebar End --> 