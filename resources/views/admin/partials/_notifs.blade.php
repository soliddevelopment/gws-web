@if(Session::has('success'))

	<script>
		$(document).ready(function(){
			$.Notification.notify('success','top right', '{{ Session::get('success') }}');
		});
	</script>

@endif

@if(count($errors) > 0)
	
	<script>
		$(document).ready(function(){
			var strErrores = '';
			
			@if(count($errors->all())>0)
				@foreach($errors->all() as $error)
					strErrores+= '{{ $error }} <br>';
				@endforeach
			@endif
			

				

			$.Notification.notify('error','top right', strErrores);
		});
	</script>
	<div class="mensaje mensaje-error" style="display:none;">
		<p>
			<strong>Errors:</strong>
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</p>
	</div>

@endif
