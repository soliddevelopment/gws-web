@foreach($mappings as $item)
    
    @if($item->campo == 'subida')
        <div class="form-group">
            {{ Form::label('subida', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::text('subida', null, ['class' => 'form-control max-char-length', 'placeholder' => $item->placeholder, 'maxlength'=> $item->max_char_length]) }}
            @if($item->max_char_length > 0)
                <div class="max-txt">
                    <span>Caracteres disponibles: </span><strong>{{$item->max_char_length}}</strong>
                </div>
            @endif
        </div>
    @endif

    @if($item->campo == 'titulo')
        <div class="form-group">
            {{ Form::label('titulo', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::text('titulo', null, ['class' => 'form-control max-char-length', 'placeholder' => $item->placeholder, 'maxlength'=> $item->max_char_length]) }}
            @if($item->max_char_length > 0)
                <div class="max-txt">
                    <span>Caracteres disponibles: </span><strong>{{$item->max_char_length}}</strong>
                </div>
            @endif
        </div>
    @endif
    
    @if($item->campo == 'slug')
        <div class="form-group">
            {{ Form::label('slug', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::text('slug', null, ['class' => 'form-control max-char-length', 'placeholder' => $item->placeholder, 'maxlength'=> $item->max_char_length]) }}
            @if($item->max_char_length > 0)
                <div class="max-txt">
                    <span>Caracteres disponibles: </span><strong>{{$item->max_char_length}}</strong>
                </div>
            @endif
        </div>
    @endif

    @if($item->campo == 'titulo_corto')
        <div class="form-group">
            {{ Form::label('titulo_corto', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::text('titulo_corto', null, ['class' => 'form-control max-char-length', 'placeholder' => $item->placeholder, 'maxlength'=> $item->max_char_length]) }}
            @if($item->max_char_length > 0)
                <div class="max-txt">
                    <span>Caracteres disponibles: </span><strong>{{$item->max_char_length}}</strong>
                </div>
            @endif
        </div>
    @endif

    @if($item->campo == 'bajada')
        <div class="form-group">
            {{ Form::label('bajada', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::textarea('bajada', null, ['class' => 'form-control max-char-length', 'rows'=>'2', 'placeholder' => $item->placeholder, 'maxlength'=> $item->max_char_length]) }}
            @if($item->max_char_length > 0)
                <div class="max-txt">
                    <span>Caracteres disponibles: </span><strong>{{$item->max_char_length}}</strong>
                </div>
            @endif
        </div>
    @endif

    @if($item->campo == 'destacado')
        <div class="form-group">
            {{ Form::label('destacado', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::checkbox('destacado', 1) }}
        </div>
    @endif

    @if($item->campo == 'categoria_id')
        <div class="form-group">
            {{ Form::label('categoria_id', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            <select name="categoria_id" id="categoria_id" class="form-control">
                <option value="0">- Ninguna -</option>
                @foreach($categorias as $item)
                    <option value="{{ $item->id }}" {{ $categoria != null && $categoria->id == $item->id ? 'selected':'' }} >
                        {{ $item->titulo }}
                    </option>
                @endforeach
            </select>
        </div>
    @endif
    
    @if($item->campo == 'subcategoria_id')
        <div class="form-group">
            {{ Form::label('subcategoria_id', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            <select name="subcategoria_id" id="subcategoria_id" class="form-control">
                <option value="0" data-cat="0">- Ninguna -</option>
                @foreach($subcategorias as $item)
                    <option value="{{ $item->id }}" data-cat="{{ $item->categoria_id }}" style="{{ $post != null && $post->subcategoria_id == $item->id ? 'display:block':'display:none' }}" {{ $post != null && $post->subcategoria_id == $item->id ? 'selected':'' }}>{{ $item->titulo }}</option>
                @endforeach
            </select>
        </div>
    @endif

    @if($item->campo == 'img')
        <div class="form-group">
            <div class="img-preview clearfix">
                {{ Form::label('img', $item->titulo.':') }}
                @if(strlen($item->helptext) > 0)
                    &nbsp; <small class="text-muted"><b>Peso máximo 3 mb. {{ $item->helptext }}</b></small>
                @endif
                <div class="cont">
                    @if($post!=null)
                        <div class="img" style="background-image:url({{ thumb($post->img, 100, 100) }})"></div>
                    @endif
                    @if($post==null)
                        <div class="img" style="background-image:url(/images/upload.png)"></div>
                    @endif
                    {{ Form::file('img') }}
                </div>
            </div>
        </div>
    @endif

    @if($item->campo == 'img2')
        <div class="form-group">
            <div class="img-preview clearfix">
                {{ Form::label('img2', $item->titulo.':') }}
                @if(strlen($item->helptext) > 0)
                    &nbsp; <small class="text-muted"><b>Peso máximo 3 mb. {{ $item->helptext }}</b></small>
                @endif
                <div class="cont">
                    @if($post!=null)
                        <div class="img" style="background-image:url({{ thumb($post->img2, 100, 100) }})"></div>
                    @endif
                    @if($post==null)
                        <div class="img" style="background-image:url(/images/upload.png)"></div>
                    @endif
                    {{ Form::file('img2') }}
                </div>
            </div>
        </div>
    @endif

    @if($item->campo == 'img3')
        <div class="form-group">
            <div class="img-preview clearfix">
                {{ Form::label('img3', $item->titulo.':') }}
                @if(strlen($item->helptext) > 0)
                    &nbsp; <small class="text-muted"><b>Peso máximo 3 mb, Formatos: jpg, png. {{ $item->helptext }}</b></small>
                @endif
                <div class="cont">
                    @if($post!=null)
                        <div class="img" style="background-image:url({{ thumb($post->img3, 100, 100) }})"></div>
                    @endif
                    @if($post==null)
                        <div class="img" style="background-image:url(/images/upload.png)"></div>
                    @endif
                    {{ Form::file('img3') }}
                </div>
            </div>
        </div>
    @endif

    @if($item->campo == 'body')
        <div class="form-group">
            {{ Form::label('body', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::textarea('body', null, ['class' => 'summernote']) }}
        </div>
    @endif
    
    @if($item->campo == 'url_externo')
        <div class="form-group">
            {{ Form::label('url_externo', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::text('url_externo', null, ['class' => 'form-control', 'placeholder' => $item->placeholder]) }}
        </div>
    @endif

    @if($item->campo == 'simple_tags')
        <div class="form-group">
            {{ Form::label('simple_tags', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>Ingresar los tags separados por comas. {{ $item->helptext }}</b></small>
            @endif
            {{ Form::textarea('simple_tags', null, [
                'class' => 'form-control', 
                'rows' => '4', 
                'placeholder' => $item->placeholder
            ]) }}
        </div>
    @endif

    @if($item->campo == 'fecha_custom')
        <div class="form-group">
            {{ Form::label('fecha_custom', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::text('fecha_custom', null, ['class' => 'form-control', 'placeholder' => $item->placeholder]) }}
        </div>
    @endif

    {{-- @if($item->campo == 'order')
        <div class="form-group">
            {{ Form::label('order', $item->titulo.':') }}
            @if(strlen($item->helptext) > 0)
                &nbsp; <small class="text-muted"><b>{{ $item->helptext }}</b></small>
            @endif
            {{ Form::number('order', null, ['class' => 'form-control', 'placeholder' => $item->placeholder, 'min' => '0']) }}
        </div>
    @endif --}}
        
@endforeach

<hr>

<div class="btn-cont">
    
    {{ Form::hidden('grupo_id', $grupo->id) }}
    {{-- {{ Form::hidden('metas_nuevos', $post->grupo_id, ['id'=>'metas_nuevos']) }} --}}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>