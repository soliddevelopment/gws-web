@extends('admin.main')

@section('title', $title)

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.posts.index', ['grupo_id' => $grupo->id]) }}">{{ $grupo->titulo }}</a></li>
                <li class="active">{{ $title }}</li>
            </ol>
            <h4 class="page-title">{{ $title }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div id="posts_form_page" class="card-box">
            {{-- <div class="pull-right">
                <a href="" class="btn btn-primary">+ Nuevo</a>
            </div> --}}
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de usuarios</b>
			</h4> --}}
			
			@if($post != null)
				{!! Form::model($post, ['route'=>['admin.posts.update', $post->id], 'method'=>'PUT', 'files'=>true, 'data-edit'=> 'true' ]) !!}
					@include('admin.posts._form')
				{!! Form::close() !!}
			@endif

			@if($post == null)
				{!! Form::open(['route'=>['admin.posts.store', null], 'method'=>'post', 'files'=>true, 'data-edit'=> 'false' ]) !!}
					@include('admin.posts._form')
				{!! Form::close() !!}
			@endif

		</div>
	</div>
</div>

@section('view_scripts')
    <script type="text/javascript" src="{{mix('a/js/view/grupos-mappings-posts.js')}}"></script>
@endsection

@endsection