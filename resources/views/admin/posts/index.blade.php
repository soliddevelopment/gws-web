@extends('admin.main')

@section('title', 'Contenido de '.$grupo->titulo)

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li class="active">{{ $grupo->titulo }}</li>
            </ol>
            <h4 class="page-title">{{ $grupo->titulo }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="align-right">
                
                @if($grupo->nombre === 'productos')
                    <div style="display:inline-block;width: 240px;margin-right:20px;">
                        <form action="" method="GET">
                            <select id="filtro_categorias" name="cat" class="form-control filtro-categorias">
                                <option {{ $selected_cat === null ? 'selected':'' }}>[ Todas las categorías ]</option>
                                @foreach($categorias as $item)
                                    <option value="{{$item->id}}" {{ $selected_cat !== null && $selected_cat->id === $item->id ? 'selected':'' }}>{{$item->titulo}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                    <a href="{{ route('admin.categorias.index', ['grupo'=> $grupo->id]) }}" class="btn btn-primary">Editar categorías</a>
                @endif

                @if($grupo->nombre !== 'galerias')
                    <a href="{{ route('admin.posts.create', ['grupo_id'=> $grupo->id]) }}" class="btn btn-primary">+ Nuevo</a>
                @endif
            </div>
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de {{ $grupo->titulo }}</b>
            </h4> --}}
            
            <hr>
            
            <div class="row">
                <div class="col-md-12">
                	<table class="table table-striped heySort" data-url="{{ route('admin.posts.sort', $grupo->id) }}">
                		<thead>
                			<tr>
                                <th></th>
                                
                                <?php if($usuario->hasRole('Global')){ ?>
                                    <th>id</th>
                                <?php } ?>
                                
                                <?php if($grupo->nombre=='productos'){ ?>
                				    <th>categoría</th>
                                <?php } ?>

                                <th>título</th>
                                
                                @if($grupo->nombre !== 'galerias')
                                    <th>creado</th>
                                @endif
                                
                                @if($grupo->nombre !== 'galerias')
                                    <th style="text-align:center;">habilitado</th>
                                @endif
                                
                                <?php // ESTO ES UNA MEDIDA TEMPORAL
                                if($grupo->nombre=='galerias'){ ?>
                                    <th></th>
                                <?php } ?>
                                

                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($posts as $item)
	                			<tr id="elem_{{$item->id}}">
                                    <td>
                                        @if($grupo->nombre !== 'galerias')
                                            @if($grupo->nombre === 'productos')
                                                <a class="fancybox" href="{{ asset('/storage/'.$item->img) }}"><img width="50" src="{{ asset('/storage/'.$item->img) }}"></a>
                                            @else
                                                <a class="fancybox" href="{{ thumb($item->img, 800, 600) }}"><img src="{{ thumb($item->img, 50, 50) }}"></a>
                                            @endif
                                        @endif
                                    </td>
                                    
                                    <?php if($usuario->hasRole('Global')){ ?>
                                        <td>{{ $item->id }}</td>
                                    <?php } ?>

                                    <?php if($grupo->nombre=='productos'){ ?>
                                        <td>
                                            @if($item->categoria != null)
                                                {{-- {{ $item->categoria->id }} -  --}}
                                                {{ $item->categoria->titulo }}
                                            @endif
                                            @if($item->categoria == null)
                                                Sin categoría
                                            @endif
                                        </td>
                                    <?php } ?>

                                    <td>{{ $item->titulo }}</td>

                                    @if($grupo->nombre !== 'galerias')
                                        <td>
                                            {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                        </td>
                                    @endif
                                    
                                    @if($grupo->nombre !== 'galerias')
                                        <td class="align-center" style="text-align:center;">
                                            <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.posts.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->st == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                        </td>
                                    @endif

                                    <?php // ESTO ES UNA MEDIDA TEMPORAL
                                     if($grupo->nombre=='galerias'){ ?>
                                        <td>
                                            <a href="{{ route('admin.imgs.post', ['id'=>$item->id]) }}">Editar imágenes ({{ count($item->imgs) }})</a>
                                        </td>
                                    <?php } ?>

                                    <td style="text-align:right;">
                                        <?php if($grupo->nombre != 'galerias') { // MEDIDA TEMPORAL ?>
                                            <a href="{{ route('admin.posts.edit', [ 'grupo_id' => $grupo->id, 'id' => $item->id  ]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                                Editar
                                            </a>
                                            &nbsp;

                                            {!! Form::open(['route'=>['admin.posts.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}

                                            <a href="#" 
                                                data-method="delete" 
                                                data-title="¡Precaución!" 
                                                data-text="¿Estás seguro que quieres borrar esto?" 
                                                data-type="warning" 
                                                data-id-form="form_{{ $item->id }}"
                                                class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                                Borrar
                                            </a>
                                        <?php } ?>
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>

                    {{-- {!! $posts->links() !!} --}}

                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection