<div class="form-group">
    {{ Form::label('destacado', 'Destacado:') }}
    {{ Form::checkbox('destacado', 1) }}
</div>
<div class="form-group">
    {{ Form::label('cliente', 'Cliente:') }}
    {{ Form::text('cliente', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('titulo', 'Título:') }}
    {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('slug', 'Slug:') }}
    {{ Form::text('slug', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('bajada', 'Bajada:') }}
    {{ Form::text('bajada', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('descripcion', 'Descripción:') }}
    {{ Form::textarea('descripcion', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<hr>
{{-- <div class="form-group">
    <div class="img-preview clearfix">
        {{ Form::label('img_logo', 'Imagen logo:') }}
        <div class="cont">
            @if($proyecto!=null && strlen($proyecto->img_logo)>0)
                <div class="img" style="{{ "background-image:url('" . asset('storage/'.$proyecto->img_logo) . "'); background-size: 85% auto; background-color: #4c5667" }}"></div>
            @endif
            @if($proyecto==null || strlen($proyecto->img_logo)==0)
                <div class="img" style="background-image:url(/images/upload.png)"></div>
            @endif
            {{ Form::file('img_logo') }}
        </div>
    </div>
</div> --}}
<div class="form-group">
    <div class="img-preview clearfix">
        {{ Form::label('img_thumb', 'Imagen thumb:') }}
        <div class="cont">
            @if($proyecto!=null)
                {{-- <div class="img" style="{{ "background-image:url('" . thumb($proyecto->img_thumb, 100, 100) . "')" }}"></div> --}}
                <div class="img" style="{{ bgImgStyle($proyecto->img_thumb, 100, 100) }}"></div>
            @endif
            @if($proyecto==null)
                <div class="img" style="background-image:url(/images/upload.png)"></div>
            @endif
            {{ Form::file('img_thumb') }}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="img-preview clearfix">
        {{ Form::label('img_intro', 'Imagen intro:') }}
        <div class="cont">
            @if($proyecto!=null)
                <div class="img" style="{{ bgImgStyle($proyecto->img_intro, 100, 100) }}"></div>
            @endif
            @if($proyecto==null)
                <div class="img" style="background-image:url(/images/upload.png)"></div>
            @endif
            {{ Form::file('img_intro') }}
        </div>
    </div>
</div>
<hr>
<div class="form-group">
    <p><label>Servicios:</label></p>
    <div class="form-group lista-servicios checkboxes-list">
        <ul>
            @foreach($lista_servicios as $item)
                <li>
                    @if($item->checked)
                        {{ Form::checkbox('serv_'.$item->id, 1, true) }} {{ $item->titulo }}
                    @else
                        {{ Form::checkbox('serv_'.$item->id, 1, false) }} {{ $item->titulo }}
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>
<div class="form-group">
    {{ Form::label('fecha', 'Fecha personalizada:') }}
    {{ Form::text('fecha', null, ['class' => 'form-control', 'placeholder' => '']) }}
</div>

<hr>

<div class="btn-cont">
    {{-- {{ Form::hidden('grupo_id', $grupo->id) }} --}}
    {{-- {{ Form::hidden('metas_nuevos', $post->grupo_id, ['id'=>'metas_nuevos']) }} --}}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>