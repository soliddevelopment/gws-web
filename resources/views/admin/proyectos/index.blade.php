@extends('admin.main')

@section('title', 'Lista de proyectos')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="/">Administrador</a></li>
                <li class="active">Proyectos</li>
            </ol>
            <h4 class="page-title">Proyectos</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="pull-right">
                <a href="{{ route('admin.proyectos.create') }}" class="btn btn-primary">+ Nuevo</a>
            </div>
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de {{ $grupo->titulo }}</b>
            </h4> --}}
            
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped heySort lista-work" data-url="{{ route('admin.proyectos.sort') }}">
                		<thead>
                			<tr>
                                <th></th>
                				<th>id</th>
                				<th>título</th>
                				<th>creado</th>
                				<th class="align-center">habilitado</th>
                				<th class="align-center">destacado</th>
                				<th></th>
                				<th></th>
                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($proyectos as $item)
	                			<tr id="elem_{{$item->id}}">
                                    <td>
                                        <a class="fancybox" href="{{ thumb($item->img_thumb, 800, 600) }}"><img src="{{ thumb($item->img_thumb, 60, 44) }}"></a>
                                    </td>
	                				<td>{{ $item->id }}</td>
                                    <td>{{ $item->titulo }}</td>
	                				<td>
                                        {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                    </td>
                                    <td class="align-center">
                                        <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.proyectos.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->st == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                    </td>
                                    <td class="align-center">
                                        <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.proyectos.destacado', ['id' => $item->id]) }}" type="checkbox" {{ $item->destacado == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#1CCC3F" data-size="small" />
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.proyectos.imgs', ['id'=>$item->id]) }}">Editar imágenes ({{ count($item->imgs) }})</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.proyectos.links.index', ['id'=>$item->id]) }}">Editar links ({{ count($item->links) }})</a>
                                    </td>
                                    <td style="text-align:right;">
                                        <a href="{{ route('admin.proyectos.edit', [ 'id' => $item->id  ]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                            Editar
                                        </a>
                                        &nbsp;

                                        {!! Form::open(['route'=>['admin.proyectos.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                        {!! Form::close() !!}

                                        <a href="#" 
                                            data-method="delete" 
                                            data-title="¡Precaución!" 
                                            data-text="¿Estás seguro que quieres borrar esto?" 
                                            data-type="warning" 
                                            data-id-form="form_{{ $item->id }}"
                                            class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                            Borrar
                                        </a>
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>

                    {{-- {!! $proyectos->links() !!} --}}

                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection