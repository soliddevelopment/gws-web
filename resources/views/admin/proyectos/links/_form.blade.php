<div class="form-group">
    {{ Form::label('tipo', 'Tipo:') }}
    <select name="tipo" id="" class="form-control">
        <option value="WEB">Sitio web</option>
        <option value="IOS">iOS app</option>
        <option value="ANDROID">Android app</option>
    </select>
</div>
<div class="form-group">
    {{ Form::label('titulo', 'Título:') }}
    {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('url', 'Url:') }}
    {{ Form::text('url', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('descripcion', 'Descripción:') }}
    {{ Form::textarea('descripcion', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<hr>

<div class="btn-cont">
    {{ Form::hidden('proyecto_id', $proyecto->id) }}
    {{-- {{ Form::hidden('metas_nuevos', $post->grupo_id, ['id'=>'metas_nuevos']) }} --}}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>