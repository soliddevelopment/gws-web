@extends('admin.main')

@section('title', $title)

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.proyectos.index') }}">Proyectos</a></li>
                <li><a href="{{ route('admin.proyectos.edit', ['id'=>$proyecto->id]) }}">{{ $proyecto->titulo }}</a></li>
                <li class="active">{{ $title }}</li>
            </ol>
            <h4 class="page-title">{{ $title }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div class="card-box">
            {{-- <div class="pull-right">
                <a href="" class="btn btn-primary">+ Nuevo</a>
            </div> --}}
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de usuarios</b>
			</h4> --}}
			
			@if($link != null)
				{!! Form::model($link, ['route'=>['admin.proyectos.links.update', $link->id], 'method'=>'PUT', 'files'=>true ]) !!}
					@include('admin.proyectos.links._form')
				{!! Form::close() !!}
			@endif

			@if($link == null)
				{!! Form::open(['route'=>['admin.proyectos.links.store'], 'method'=>'post', 'files'=>true ]) !!}
					@include('admin.proyectos.links._form')
				{!! Form::close() !!}
			@endif

		</div>
	</div>
</div>

@section('scripts')
	{{-- <script type="text/javascript" src="{{ mix('a/js/custom/editarPost.js') }}"></script> --}}
@endsection

@endsection