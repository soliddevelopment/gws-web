@extends('admin.main')

@section('title', 'Lista de links')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.proyectos.index', ['id'=>$proyecto->id]) }}">Proyectos</a></li>
                <li><a href="{{ route('admin.proyectos.edit', ['id'=>$proyecto->id]) }}">{{ $proyecto->titulo }}</a></li>
                <li class="active">Links</li>
            </ol>
            <h4 class="page-title">Links</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="pull-right">
                <a href="{{ route('admin.proyectos.links.create', ['proyectoId'=> $proyecto->id ]) }}" class="btn btn-primary">+ Nuevo</a>
            </div>
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de {{ $grupo->titulo }}</b>
            </h4> --}}
            
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped heySort lista-work" data-url="{{ route('admin.proyectos.links.sort', ['id'=> $proyecto->id]) }}">
                		<thead>
                			<tr>
                				<th>id</th>
                				<th>tipo</th>
                				<th>título</th>
                				<th>url</th>
                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($links as $item)
	                			<tr id="elem_{{$item->id}}">
	                				<td>{{ $item->id }}</td>
	                				<td>{{ $item->tipo }}</td>
                                    <td>{{ $item->titulo }}</td>
                                    <td><a href="{{ $item->url }}" target="_blank">{{ $item->url }}</a></td>
                                    <td style="text-align:right;">
                                        <a href="{{ route('admin.proyectos.links.edit', [ 'linkId' => $item->id  ]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                            Editar
                                        </a>
                                        &nbsp;

                                        {!! Form::open(['route'=>['admin.proyectos.links.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                        {!! Form::close() !!}

                                        <a href="#" 
                                            data-method="delete" 
                                            data-title="¡Precaución!" 
                                            data-text="¿Estás seguro que quieres borrar esto?" 
                                            data-type="warning" 
                                            data-id-form="form_{{ $item->id }}"
                                            class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                            Borrar
                                        </a>
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>

                    {{-- {!! $proyectos->links() !!} --}}

                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection