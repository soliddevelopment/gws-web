<div class="form-group">
    {{ Form::label('titulo', 'Título:') }}
    {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<hr>
<div class="btn-cont">
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>