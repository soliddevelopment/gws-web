@extends('admin.main')

@section('title', 'Lista de servicios')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li class="active">Servicios</li>
            </ol>
            <h4 class="page-title">Servicios</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="pull-right">
                <a href="{{ route('admin.servicios.create') }}" class="btn btn-primary">+ Nuevo</a>
            </div>
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de {{ $grupo->titulo }}</b>
            </h4> --}}
            
            <div class="row">
                <div class="col-md-12">
                	<table class="table table-striped">
                		<thead>
                			<tr>
                				<th>título</th>
                				<th>creado</th>
                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($servicios as $item)
	                			<tr>
                                    <td>{{ $item->titulo }}</td>
	                				<td>
                                        {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                    </td>
                                    <td style="text-align:right;">
                                        <a href="{{ route('admin.servicios.edit', [ 'id' => $item->id  ]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                            Editar
                                        </a>
                                        &nbsp;

                                        {!! Form::open(['route'=>['admin.servicios.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                        {!! Form::close() !!}

                                        <a href="#" 
                                            data-method="delete" 
                                            data-title="¡Precaución!" 
                                            data-text="¿Estás seguro que quieres borrar esto?" 
                                            data-type="warning" 
                                            data-id-form="form_{{ $item->id }}"
                                            class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                            Borrar
                                        </a>
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>

                    {{-- {!! $servicios->links() !!} --}}

                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection