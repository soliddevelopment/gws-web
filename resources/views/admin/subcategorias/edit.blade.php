@extends('admin.main')

@section('title', 'Editar categoría')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.posts.index', ['grupo_id'=>$grupo->id]) }}">{{ $grupo->titulo }}</a></li>
                <li><a href="{{ route('admin.categorias.index', ['grupo'=>$grupo->id]) }}">Categorías</a></li>
                <li><a href="{{ route('admin.subcategorias.index', ['categoria'=>$categoria->id]) }}">Subcategorías</a></li>
                <li class="active">Editar subcategoría</li>
            </ol>
            <h4 class="page-title">Editar subcategoría de {{ $categoria->titulo }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div class="card-box">
            {{-- <div class="pull-right">
                <a href="" class="btn btn-primary">+ Nuevo</a>
            </div> --}}
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de usuarios</b>
            </h4> --}}

			{!! Form::model($subcategoria, ['route'=>['admin.subcategorias.update', $subcategoria->id], 'method'=>'PUT', 'files'=>true ]) !!}

				<div class="form-group">
					{{ Form::label('nombre', 'Nombre:') }}
					{{ Form::text('nombre', null, ['class' => 'form-control']) }}
				</div>
				<div class="form-group">
					{{ Form::label('titulo', 'Título:') }}
					{{ Form::text('titulo', null, ['class' => 'form-control']) }}
				</div>
				<div class="form-group">
					<div class="img-preview clearfix">
						{{ Form::label('img', 'Imagen principal:') }}
						<small class="text-muted">Peso máximo 3 mb, Formatos: jpg, png</small>
						<div class="cont">
							<div class="img" style="background-image:url({{ thumb($subcategoria->img, 100, 100) }})"></div>
							{{ Form::file('img') }}
						</div>
					</div>
				</div>

				<div class="btn-cont">
					{{ Form::hidden('categoria_id', $categoria->id) }}
					{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
				</div>

			{!! Form::close() !!}

		</div>
	</div>
</div>

@section('scripts')
	{{-- <script type="text/javascript" src="{{ mix('a/js/custom/editarPost.js') }}"></script> --}}
@endsection

@endsection