<div class="form-group">
    {{ Form::label('nombre', 'Nombre:') }}
    {{ Form::text('nombre', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('apellido', 'Apellido:') }}
    {{ Form::text('apellido', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('usuario', 'Usuario:') }}
    {{ Form::text('usuario', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('email', 'Email:') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
</div>

<?php if($user == null) { ?>
    <div class="form-group">
        {{ Form::label('password', 'Password:') }}
        {{ Form::password('password', ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('password_confirmation', 'Confirmar password:') }}
        {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
    </div>
<?php } ?>

<?php if($usuario->hasRole('Global') || ( $usuario->hasRole('Admin') )){ ?>
    <?php if( $user != null && !$user->hasRole('Global') ){ ?>
        <div class="form-group">
            <a href="#" data-toggle="modal" data-target="#cambiarContrasenaModal" class="btn btn-sm btn-primary">Cambiar contraseña</a>
        </div>
        <div class="form-group">
            {{ Form::label('role', 'Roles:') }}
            <div class="checkboxes">
                @foreach($roles as $role)
                    @if($role->nombre != 'Global')
                        <p>
                            {{-- <input type="checkbox" name="roles[]" value="{{ $role->id }}" {{ $user->hasRole($role->nombre) ? 'checked':'' }}> --}}
                            <input type="radio" name="role" value="{{ $role->id }}" {{ $user->hasRole($role->nombre) ? 'checked':'' }}>
                            <span>
                                {{ $role->nombre }} 
                                @if(strlen($role->descripcion)>0)
                                    ({{ $role->descripcion }})
                                @endif
                            </span>
                        </p>
                    @endif
                @endforeach
            </div>
        </div>
    <?php } else if($user === null){ ?>
        <div class="form-group">
            {{ Form::label('role', 'Roles:') }}
            <div class="checkboxes">
                @foreach($roles as $role)
                    @if($role->nombre != 'Global')
                        <p>
                            <input type="radio" name="role" value="{{ $role->id }}">
                            <span>
                                {{ $role->nombre }} 
                                @if(strlen($role->descripcion)>0)
                                    ({{ $role->descripcion }})
                                @endif
                            </span>
                        </p>
                    @endif
                @endforeach
            </div>
        </div>
    <?php } ?>
<?php } ?>

<div class="btn-cont">
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>

{{-- {!! Form::open(['route'=>['usuarios.destroy', $usuario->id], 'method'=>'DELETE' ]) !!}
    {{ Form::submit('Borrar', ['class' => 'btn btn-primary btn-lg']) }}
{!! Form::close() !!} --}}