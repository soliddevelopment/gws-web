@extends('admin.main')

@section('title', $title)

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.usuarios.index') }}">Usuarios</a></li>
                <li class="active">Editar</li>
            </ol>
            <h4 class="page-title">Usuarios</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div class="card-box">
			
			@if($user != null)
				{!! Form::model($user, ['route'=>['admin.usuarios.update', $user->id], 'method'=>'PUT' ]) !!}
					@include('admin.usuarios._form')
				{!! Form::close() !!}
			@endif

			@if($user == null)
				{!! Form::open(['route'=>['admin.usuarios.store', null], 'method'=>'post', 'files'=>true ]) !!}
					@include('admin.usuarios._form')
				{!! Form::close() !!}
			@endif

		</div>
	</div>
</div>

@if($user != null)
	<div id="cambiarContrasenaModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="cambiarContrasenaModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="cambiarContrasenaModalLabel">Cambiar contraseña</h4>
				</div>
				<div class="modal-body">
					<div class="modal-form">
						{!! Form::model($user, ['route'=>['admin.usuarios.updatepassword', $user->id], 'method'=>'PUT' ]) !!}
							<div class="form-group">
								{{ Form::label('password', 'Contraseña:') }}
								{{ Form::password('password', ['class' => 'form-control']) }}
							</div>
							<div class="form-group">
								{{ Form::label('password_confirmation', 'Confirmar contraseña:') }}
								{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
							</div>
							<div class="btn-cont">
								{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@endif

@endsection