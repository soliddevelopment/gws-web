@extends('admin.main')

@section('title', 'Lista de usuarios')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li class="active">Usuarios</li>
            </ol>
            <h4 class="page-title">Usuarios</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="card-box">
            <div class="pull-right">
                <a href="{{ route('admin.usuarios.create') }}" class="btn btn-primary">+ Nuevo</a>
            </div>
            <h4 class="m-t-0 header-title">
                <b>Lista de usuarios</b>
            </h4>
            
            <div class="row">
                <div class="col-md-12">
                	<table class="table table-striped">
                		<thead>
                			<tr>
                				<th>id</th>
                				<th>usuario</th>
                				<th>nombre</th>
                				<th>email</th>
                				<th>creado</th>
                                <th>roles</th>
                				<th></th>
                				<th></th>
                			</tr>
                		</thead>
                		<tbody>
                			@foreach($usuarios->all() as $item)
	                			<tr>
	                				<td>{{ $item->id }}</td>
	                				<td>{{ $item->usuario }}</td>
	                				<td>{{ $item->nombre.' '.$item->apellido }}</td>
	                				<td>{{ $item->email }}</td>
	                				<td>{{ $item->created_at }}</td>
                                    <td>
                                        <?php $x=0; ?>
                                        @foreach($item->roles as $role) 
                                            <?php $x++; ?>
                                            {{ $role->nombre }}{{ $x<count($item->roles) ? ',':''  }}
                                        @endforeach
                                    </td>
                                    <td class="align-center">
                                        <?php /*<input class="st-switch" data-id="1" data-url="st" type="checkbox" <?php if($elem->st == 1) echo "checked";  data-plugin="switchery" data-color="#3bafda" data-size="small" /> */ ?>
                                        <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.usuarios.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->st == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                    </td>
                                    <td style="text-align:right;">
                                        <a href="{{ route('admin.usuarios.edit', ['id' => $item->id]) }} " class="btn btn-primary btn-sm waves-effect waves-light">
                                            Editar
                                        </a>
                                        &nbsp;

                                        @if($usuario->hasRole('Admin'))
                                            {!! Form::open(['route'=>['admin.usuarios.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}
                                            <a href="#" 
                                                data-method="delete" 
                                                data-title="¡Precaución!" 
                                                data-text="¿Estás seguro que quieres borrar esto?" 
                                                data-type="warning" 
                                                data-id-form="form_{{ $item->id }}"
                                                class="hey-confirm btn btn-danger btn-sm waves-effect waves-light">
                                                Borrar
                                            </a>
                                        @endif
                                    </td>
	                			</tr>
                			@endforeach
                		</tbody>
                	</table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection