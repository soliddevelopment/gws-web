@extends('publico.main')

@section('title', 'Registro')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<h1>REGISTRO</h1>

{!! Form::open(['action'=>'Publico\Auth\RegisterController@register', 'method'=>'post']) !!}
    
    <div class="form-group">
        {{ Form::label('usuario', 'Usuario:') }}
        {{ Form::text('usuario', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('email', 'Email:') }}
        {{ Form::email('email', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('nombre', 'Nombre:') }}
        {{ Form::text('nombre', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('apellido', 'Apellido:') }}
        {{ Form::text('apellido', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('password', 'Contraseña:') }}
        {{ Form::text('password', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('password_confirmation', 'Confirmar contraseña:') }}
        {{ Form::text('password_confirmation', null, ['class' => 'form-control']) }}
    </div>
    <div class="checkbox checkbox-primary">
        <label>
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
        </label>
    </div>

    <div class="btn-cont">
        {{ Form::submit('Ingresar', ['class' => 'btn btn-primary btn-lg']) }}
    </div>

{!! Form::close() !!}


<a href="{{ route('publico.home.home') }}">Home</a>
<a href="{{ route('publico.auth.login') }}">Login</a>
<a href="{{ route('publico.auth.register') }}">Registro</a>


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection