@extends('publico.main')
@section('title', $post->titulo)
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/logo.png') }}'
    ];
</script>

<div id="page_detalle" class="page">
    {{-- <a href="{{ route('publico.software.home') }}" class="sitio software"> --}}
    <div class="m">
        <div class="detalle">

            @if(strlen($post->img)>0)
                <div class="img-cont">
                    <img src="{{ asset('storage/'.$post->img) }}" alt="" class="img">
                </div>
            @endif
            
            <div class="head">
                <div class="regresar-cont">
                    <a href="{{ route('publico.blog.index') }}" class="ajax-link" data-page-title="CENEFRO | Blog">
                        Regresar al blog
                    </a>
                </div>
                <h1 class="titulo tit1">{{ $post->titulo }}</h1>
                <div class="fecha-cont">
                    <p>{{ Carbon\Carbon::parse($post->created_at)->format('d/m/Y h:i A') }}</p>
                </div>
                @if(strlen($post->bajada)>0)
                    <h2 class="bajada txt2 rojo">{{ $post->bajada }}</h2>
                @endif
                @if(strlen($post->titulo_corto)>0)
                    <p class="autor">
                        <i class="fa fa-user"></i>
                        {{ $post->titulo_corto }}
                    </p>
                @endif
            </div>
            <div class="body">
                {!! $post->body !!}
            </div>

        </div>
    </div>

</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection