@extends('publico.main')
@section('title', 'Noticias')
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/logo.png') }}'
    ];
</script>

<div id="page_blog" class="page">
    <section>
        <div id="intro2">
            <h1 class="tit1 heywow">BLOG</h1>
            <p class="bajada heywow">
                Aquí publicamos notas y eventos de la empresa.
            </p>
        </div>
    </section>

    <div class="m">
        <div class="lista-blog">
            @foreach($posts as $item)
                <?php if(strlen($item->url_externo)>0){
                    $url = $item->url_externo;
                    $txt_blank = 'target="_blank"';
                    $txt_ajax_link = '';
                } else {
                    $url = route('publico.blog.detalle', ['id'=>$item->id]);
                    $txt_blank = '';
                    $txt_ajax_link = 'ajax-link';
                } ?>
                <a href="{{ $url }}" {{ $txt_blank }} class="elem {{ $txt_ajax_link }} wow fadeInUp" data-page-title="CENEFRO | {{ $item->titulo }}">
                    <span class="img" style="background-image: url({{ thumb($item->img, 500, 500) }})"></span>
                    <span class="info">
                        <span class="titulo tit2">{{ $item->titulo }}</span>
                        <span class="fecha">
                            {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y') }}
                        </span>
                        <span class="bajada">{{ $item->bajada }}</span>
                    </span>
                </a>
            @endforeach
        </div>
    </div>
    
    {!! $posts->links() !!}

</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection