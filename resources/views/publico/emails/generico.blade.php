@component('mail::layout')

{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.angular_url')])
    <div class="header">
        @if(strlen($logo_url) > 0)
            <img style="width:144px;" width="144" src="{{ $logo_url }}">
        @endif
    </div>
@endcomponent
@endslot

{{--  ---------------------------------------------------------------------------------  --}}
{{-- Body --}}

# {{ $titulo }}
{!! $body !!}

@component('mail::button', ['url' => $url_boton, 'color' => $color ?? 'default-color'])
{{ $txt_boton }}
@endcomponent

Gracias,<br>
{{ config('app.name') }}

{{--  ---------------------------------------------------------------------------------  --}}

{{-- Subcopy --}}
@slot('subcopy')
@component('mail::subcopy')
    <!-- subcopy here -->
@endcomponent
@endslot


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    <!-- footer here -->
@endcomponent
@endslot


@endcomponent
