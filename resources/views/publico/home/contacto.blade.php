@extends('publico.main')
@section('title', 'Contacto')
@section('description', 'Global Wines & Spirits nace en mayo de 2008, con el propósito de importar y distribuir marcas únicas de alta calidad para gustos exclusivos.')
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/bgs/4.jpg') }}'
    ];
</script>

<div id="page_contacto" class="page">
    <section id="contacto" style="background-image: url('/p/img/bgs/4.jpg');">
        <div class="m">

            <div class="cols">

                <div class="col formulario">
                    <h1 class="tit1 wow fadeInDown">¿Quieres contactarnos?</h1>
                    <div class="datos wow fadeInDown">
                        <strong><a href="#" class="hidden-email-1"></a></strong>
                        <a href="tel:25231727">2523-1727</a>
                        <a href="https://wa.me/50360625288" target="_blank">
                            <img width="26" src="/p/img/whatsapp.png" alt="Whatsapp">
                            <span style="margin-left:3px;">6062-5288</span>
                        </a>
                    </div>
                    <div class="dir wow fadeInDown">
                        <i class="fa fa-map-marker"></i> 89 Av. Norte y 7 Calle Poniente, No. 515 Colonia Escalón. San Salvador, El Salvador
                    </div>
                    <div class="form">
                        <div class="solid-form">
                            {!! Form::open(['route'=>['publico.home.contactoPost'], 'method'=>'post', 'files'=>true,
                                'class'=>'solid-validate',
                                'data-loading-val'=>'Enviando...'
                            ]) !!}

                                {{ Form::text('issue', null, ['class'=>'hp-field']) }}
                                {{ Form::text('inquiry', null, ['class'=>'hp-field']) }}
                                {{ Form::text('email_address', null, ['class'=>'hp-field']) }}

                                <div class="elem wow fadeInUp">
                                    {{-- {{ Form::label('nombre', 'Name', ['class'=>'lbl']) }} --}}
                                    {{ Form::text('nombre', null, ['class'=>'field', 'placeholder'=>'Nombre...', 'required'=>true]) }}
                                </div>
                                <div class="elem wow fadeInUp">
                                    {{ Form::email('email', null, ['class'=>'field', 'placeholder'=>'Email...', 'required'=>true]) }}
                                </div>
                                <div class="elem wow fadeInUp">
                                    {{ Form::textarea('mensaje', null, ['class'=>'field', 'placeholder'=>'Message...', 'rows'=>'5', 'required'=>true]) }}
                                </div>
                                <div class="recaptcha-cont  wow fadeInUp">
                                    <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="{{ config('misc.GOOGLE_RECAPTCHA_KEY') }}"></div>
                                </div>
                                <div class="btn-cont wow fadeInUp">
                                    {{ Form::submit('Enviar', ['class' => 'btn celeste', 'id'=>'submitBtn', 'disabled']) }}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                {{-- <div class="col mapa">
                    <div class="mapa-cont heywow">
                        <div id="mapaContacto"></div>
                    </div>
                    <div class="datos-contacto">

                    </div>
                </div> --}}
            </div>

        </div>
    </section>

</div>

<script>
    function recaptchaCallback() {
        $('#submitBtn').removeAttr('disabled');
    };
</script>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection
