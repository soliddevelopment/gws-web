@extends('publico.main')
@section('title', 'GWS')
@section('description', 'Global Wines & Spirits nace en mayo de 2008, con el propósito de importar y distribuir marcas únicas de alta calidad para gustos exclusivos.')
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/logo.png') }}'
    ];
</script>

@foreach($imgs_dinamicas as $item)
    <script>
        imgArray.push('{!!$item!!}');
    </script>
@endforeach

<div id="page_home" class="page">
    
    <section id="sec_intro" class="sec">
        <div class="home-slider-cont heywow">
            <div class="home-slider">
                @foreach($slides as $item)
                    <div class="slide">
                        <div class="bg" style="{{bgImgStyle($item->img)}}"></div>
                        <div class="bg-overlay"></div>
                        <div class="cont">
                            <div class="va">
                                <h1 class="tit-huge">{{ $item->titulo }}</h1>
                                <p class="bajada txt1">
                                    {{ $item->bajada }}
                                </p>
                                <div class="btn-cont">
                                    <a href="{{route('publico.home.contacto')}}" class="btn naranja ajax-link" data-page-title="Global Wines and Spirits | Contacto">
                                        Contáctanos
                                    </a>
                                </div>
                                @include('publico.partials._datosContacto')
                                <a href="#valores" class="fle-abajo scroll-to hvr-grow" data-ms="400">
                                    <img src="{{asset('p/img/fle-abajo.png')}}" alt="Seguir hacia abajo">
                                </a>
                            </div>
                        </div>
                        <img src="{{asset('p/img/home-slider-adorno.png')}}" alt="..." class="adorno">
                    </div>
                @endforeach
            </div>
        </div>
        <img id="valores" src="{{asset('p/img/home-slider-adorno2.png')}}" alt="..." class="adorno2">
        
    </section>

    <section id="sec_valores" class="sec">
        <div class="m">
            <div class="valores valores-home">
                @include('publico.partials._valores')
            </div>
        </div>
    </section>

    <section id="sec_destacados" class="sec">
        <h1 class="tit1 naranja">Productos destacados</h1>
        <div class="destacados-slider-cont">
            <div class="destacados-slider fle-cafe">
                @foreach($destacados as $item)
                <div class="slide">
                    <img src="{{ asset('storage/'.$item->img) }}" alt="{{ $item->titulo }}" class="img">
                    <div class="cuadro">
                        <div class="info">
                            <h1 class="tit2">{{ $item->titulo }}</h1>
                            <p class="bajada">{{ $item->bajada }}</p>
                            {{-- <div class="img"></div> --}}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{-- <div class="bg-rectangulo"></div> --}}
        </div>
        <div class="btn-cont">
            <a href="{{route('publico.productos.index')}}" class="btn naranja" data-page-title="Global Wines and Spirits | Productos">
                Ir a productos
            </a>
        </div>
    </section>

    <section id="sec_videos" class="sec">
        <div class="m">
            <div class="lista">
                @foreach($videos as $item)
                    <div class="elem">
                        <div class="vid-cont wow fadeInLeft">
                            <a href="#" data-id="video1" data-video-id="{{$item['video_id']}}" class="vid" style="background-image:url('p/img/videos/{{$item['img']}}')">
                                <img src="{{asset('p/img/btn-play.png')}}" alt="">
                            </a>
                        </div>
                        <div class="info wow fadeInUp" data-wow-delay="0.44s">
                            <h2 class="titulo tit2">{{$item['titulo']}}</h2>
                            {{-- <p class="bajada txt2">{{$item['descripcion']}}</p> --}}
                        </div>
                        <div class="adorno wow zoomIn"></div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    
</div>

<div id="videoModal" class="video-modal">
    <a href="#" class="cerrar"><i class="fa fa-remove"></i></a>
    <div class="bg"></div>
    <div class="cuadro">
        <div class="cont">
            <div id="player"></div>
        </div>
    </div>
</div>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection