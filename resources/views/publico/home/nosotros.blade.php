@extends('publico.main')
@section('title', 'Sobre nosotros')
@section('description', 'Global Wines & Spirits nace en mayo de 2008, con el propósito de importar y distribuir marcas únicas de alta calidad para gustos exclusivos.')
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/bgs/1.jpg') }}',
    ];
</script>

<section>
    <div id="page_nosotros" class="page">
        <section>
            <div id="intro" class="heywow" style="background-image: url('p/img/bgs/1.jpg');">
                <div class="m">
                    <div class="info">
                        <h1 class="tit1 heywow">Sobre nosotros</h1>
                        <p class="bajada txt1 heywow">
                            Somos una compañía distribuidora de bebidas. Nuestros productos ayudan a crear momentos únicos y experiencias inolvidables,  forjando amistades y celebrando la vida.
                        </p>
                    </div>
                    <div class="datos-cont heywow">
                        @include('publico.partials._datosContacto')
                    </div>
                </div>
            </div>
        </section>
        <section id="nosotros_txt" class="sec">
            <div class="cont">
                <p class="txt1">
                    Global Wines & Spirits nace en mayo de 2008, con el proposito de importar y distribuir marcas unicas de alta calidad para gustos exclusivos.
                </p>
                <hr>
                <div class="valores2">
                    <div class="row clearfix">
                        <div class="col-sm-4">
                            <p class="tit"><strong>Colaboración</strong></p>
                            <p>Impulsamos el talento colectivo y el trabajo en equipo.</p>
                        </div>
                        <div class="col-sm-4">
                            <p class="tit"><strong>Integridad</strong></p>
                            <p>Somos transparentes.</p>
                        </div>
                        <div class="col-sm-4">
                            <p class="tit"><strong>Compromiso</strong></p>
                            <p>comprometidos en satisfacer a nuestros clientes y establecer una sólida relación comercial.</p>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-4">
                            <p class="tit"><strong>Pasión</strong></p>
                            <p>Estamos comprometidos de corazón con nuestro trabajo y con la gente que lo hace posible.</p>
                        </div>
                        <div class="col-sm-4">
                            <p class="tit"><strong>Diversidad</strong></p>
                            <p>Contamos con una gran referencias de marcas de excelencia mundial.</p>
                        </div>
                        <div class="col-sm-4">
                            <p class="tit"><strong>Calidad</strong></p>
                            <p>Vamos tras la excelencia cada día.</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section id="empresa" class="sec">
            <div class="m2">
                <h1 class="tit1">Empresa</h1>
                <div id="mision_vision">
                    <div class="col">
                        <h2 class="tit2">Nuestra misión</h2>
                        <p class="bajada">
                            Satisfacer los gustos y preferencias de nuestros consumidores y clientes ofreciendo un excelente servicio y productos exclusivos de calidad.
                        </p>
                    </div>
                    <div class="col">
                        <h2 class="tit2">Nuestra visión</h2>
                        <p class="bajada">
                            Ser la empresa lider en el mercado de bebidas alchololicas que permita el crecimiento sostenible de las utilidades.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section id="valoresImg" class="sec">
            <div class="img-cont">
                <div class="imgs">
                    <div id="img_valor_1" class="img sel" style="background-image:url('p/img/valores_nosotros/1.jpg')">
                        <div class="va">
                            <p>Ofrecemos productos de excelencia y calidad, reconocidos a nivel mundial.</p>
                        </div>
                    </div>
                    <div id="img_valor_2" class="img" style="background-image:url('p/img/valores_nosotros/2.jpg')">
                        <div class="va">
                            <p>Contamos con un amplio portafolio de marcas premium de licores y vinos  para gustos exclusivos.</p>
                        </div>
                    </div>
                    <div id="img_valor_3" class="img" style="background-image:url('p/img/valores_nosotros/3.jpg')">
                        <div class="va">
                            <p>Ofrecemos nuestros servicios de calidad y contamos con cobertura en todo el país.</p>
                        </div>
                    </div>
                    <div id="img_valor_4" class="img" style="background-image:url('p/img/valores_nosotros/4.jpg')">
                        <div class="va">
                            <p>Nuestra prioridad es brindar el mejor servicio y atención a la altura de nuestros productos de excelencia.</p>
                        </div>
                    </div>
                </div>
                <div class="cuadro"></div>
            </div>
            <div class="valores-cont">
                <div class="valores">
                    @include('publico.partials._valores')
                </div>
            </div>
            <div class="datos-contacto-snippet">
                @include('publico.partials._datosContacto')
            </div>
        </section>

    </div>
</section>


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection