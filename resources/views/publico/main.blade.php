<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>

    @include('publico.partials._analytics')

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <meta name="description" content="@yield('description', 'Global Wines & Spirits nace en mayo de 2008, con el proposito de importar y distribuir marcas unicas de alta calidad para gustos exclusivos.')">
    <meta name="author" content="solid.com.sv">

    {{--@stack('metafb')--}}

    @include('publico.partials._meta')

    {{-- Esto me sirve para ajax calls --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Global Wines and Spirits | @yield('title')</title>

     <link rel="shortcut icon" href="{{ asset('p/img/favicon.ico')}}" type="image/x-icon">
	 <link rel="icon" href="{{ asset('p/img/favicon.ico')}}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('p/css/fonts/font-awesome.css')}} " type="text/css">
    <link rel="stylesheet" href="{{ asset('p/css/fonts/fonts.css')}} " type="text/css">
    {{--<link rel="stylesheet" href="{{ mix('p/css/lib/lib.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ mix('p/css/custom.css') }}" type="text/css">--}}

    <link rel="stylesheet" href="{{ mix('p/css/app.css') }}" type="text/css">

    <script type="text/javascript" src="{{ mix('p/js/header.js') }}"></script>

    @yield('stylesheets')

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    @yield('header_scripts')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    {{-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-68678734-9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-68678734-9');
    </script> --}}


</head>

<body>
{{--<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=116889388404773&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>--}}

    <div id="todo">
        @include('publico.partials._loading')
        @include('publico.partials._header')
        @include('publico.partials._analytics')

        <div id="content">
            @yield('content')
        </div>

        @include('publico.partials._footer')

    </div>

    <div class="whatsapp-btn">
        <a href="https://wa.me/50360625288" target="_blank" class="link">
            <img src="/p/img/whatsapp.png" alt="Whatsapp">
        </a>
    </div>

    {{--  Para hacer logout  --}}
    <form id="included-logout-form" action="{{ route('publico.auth.logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

    {{--  @include('publico.partials._notifs')  --}}

    <script type="text/javascript" src="{{ asset('p/js/lib/history.js') }}"></script>
    <script type="text/javascript" src="{{ asset('p/js/lib/history.adapter.js') }}"></script>
    <script type="text/javascript" src="{{ mix('p/js/footer.js') }}"></script>

    @yield('scripts')

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXEbIrDqoZG3DmHWgYktdF3vkl0sZLFKs&callback=initMap" type="text/javascript"></script>

</body>
</html>
