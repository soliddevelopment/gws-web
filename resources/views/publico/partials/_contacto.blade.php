<div class="contacto">
    <div class="main">
        <div class="heading1 text-right clearfix">
            <h1 class=" wow fadeInUp">CONTACTO</h1>
        </div>
        
        {!! Form::open(['action'=>'Publico\HomeController@contacto', 'method'=>'post', 'files'=>false, 'class'=>'solid-validate' ]) !!}
            {{--  {{ Form::label('titulo', 'Título:') }}  --}}

            <div class="form">
                <div class="row">
                    <div class="col-sm-6">
                        {{ Form::text('nombre', null, [
                            'class' => 'form-control wow fadeInUp',
                            'placeholder' => 'Nombre...',
                        ]) }}
                    </div>
                    <div class="col-sm-6">
                            {{ Form::text('empresa', null, [
                                'class' => 'form-control wow fadeInUp',
                                'placeholder' => 'Empresa...',
                            ]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        {{ Form::email('email', null, [
                            'class' => 'form-control wow fadeInUp',
                            'placeholder' => 'Email...',
                        ]) }}
                    </div>
                    <div class="col-sm-6">
                        {{ Form::text('telefono', null, [
                            'class' => 'form-control wow fadeInUp',
                            'pattern=' => '[0-9]*',
                            'placeholder' => 'Teléfono...',
                        ]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{ Form::textarea('mensaje', null, [
                            'class' => 'form-control wow fadeInUp', 
                            'rows' => '4', 
                            'placeholder' => 'Mensaje'
                        ]) }}
                    </div>
                </div>
                <div class="botones text-center p-t-40">
                    {{ Form::hidden('tipo', $seccion) }}
                    {{ Form::submit('Enviar', [
                        'data-wow-delay' => '0.4s',
                        'class' => 'btn-linea negro wow fadeInUp',
                        'id' => 'enviar',
                    ]) }}
                </div>
            </div>
        
        {!! Form::close() !!}
        
    </div>
</div>