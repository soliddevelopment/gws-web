<section id="sec_mapa" class="sec">
    <div class="head">
        <div class="m">
            <div class="izq">
                <h1 class="tit1 naranja wow fadeInUp" data-wow-delay="0.1s">Distribuimos <strong>a más de 400 comercios</strong> en el país</h1>
                <p class="bajada wow fadeInUp" data-wow-delay="0.3s">
                    Contamos con cobertura de nuestros servicios y productos de calidad en los 14 departamentos del país.
                </p>
            </div>
            {{--<div class="datos">
                <div class="elem wow fadeInUp" data-wow-delay="0.6s">
                    <p class="num">{{ $locations && count($locations)>0 ? count($locations): '--' }}</p>
                    <p class="txt">comercios</p>
                </div>
                <div class="elem wow fadeInUp" data-wow-delay="0.8s">
                    <p class="num">14</p>
                    <p class="txt">departamentos</p>
                </div>
            </div>--}}
        </div>
    </div>
    <div class="mapa-cont">
        <div id="mapa-comercios"></div>
    </div>
    <script>
        var locationsArray = [];
    </script>
    @foreach($locations as $item)
        <script>
            locationsArray.push({
                nombre: '{{ $item->nombre }}',
                descripcion: '{{ $item->descripcion }}',
                lat: {{ $item->latitude }},
                lng: {{ $item->longitude }}
            });
        </script>
    @endforeach
</section>

<section id="sec_comercios" class="sec">
    <p class="txt1 cafe">Puedes encontrar nuestros productos aquí:</p>
    <div class="comercios-slider">
        @if(!empty($comercios))
            @foreach($comercios as $item)
                <div class="slide">
                    <img src="{{asset('storage/'.$item->img)}}" alt="asdf">
                </div>
            @endforeach
        @endif
    </div>
</section>

<footer id="footer" class="sec">
    <div class="m">
        <div class="row">
            <div id="footerLinkCols" class="col-sm-3">
                <div class="link-cols row">
                    <div class="col-sm-6">
                        <h2 class="tit2">Navega</h2>
                        <nav>
                            <ul>
                                <li><a href="{{route('publico.home.home')}}" class="ajax-link" data-page-title="Global Wines and Spirits" title="Ir a Inicio">Inicio</a></li>
                                <li><a href="{{route('publico.productos.index')}}" class="ajax-link" data-page-title="Global Wines and Spirits | Productos" title="Ir a Productos">Productos</a></li>
                                <li><a href="{{route('publico.home.nosotros')}}" class="ajax-link" data-page-title="Global Wines and Spirits | Sobre nosotros" title="Ir a Nosotros">Nosotros</a></li>
                                <li><a href="{{route('publico.home.contacto')}}" class="ajax-link" data-page-title="Global Wines and Spirits | Contacto" title="Ir a Contacto">Contacto</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-sm-6">
                        {{-- <h2 class="tit2">Navega</h2> --}}
                        <nav>
                            {{-- <ul>
                                <li><a href="{{route('publico.home.home')}}" class="ajax-link" data-page-title="Global Wines and Spirits">Inicio</a></li>
                                <li><a href="{{route('publico.productos.index')}}" class="ajax-link" data-page-title="Global Wines and Spirits | Productos">Productos</a></li>
                                <li><a href="{{route('publico.home.nosotros')}}" class="ajax-link" data-page-title="Global Wines and Spirits | Sobre nosotros">Nosotros</a></li>
                                <li><a href="{{route('publico.home.contacto')}}" class="ajax-link" data-page-title="Global Wines and Spirits | Contacto">Contacto</a></li>
                            </ul> --}}
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="centro">
                    <h1 class="tit2">¿TIENES ALGUNA CONSULTA?</h1>
                    <div class="btn-cont">
                        <a href="{{route('publico.home.contacto')}}" data-page-title="Global Wines and Spirits | Contacto" class="btn naranja" title="Ir a Contacto">Contáctanos</a>
                    </div>
                    {{--<div class="redes">
                        @include('publico.partials._redes')
                    </div>--}}
                    <div class="derechos">
                        Todos los derechos reservados
                        <br>
                        <a href="http://solid.com.sv" target="_blank">Web design &amp; development</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="datos-contacto">
                    <p>
                        <span class="ic"><i class="fa fa-phone"></i></span>
                        <span class="val">
                            <a href="tel:25231727">(503) 2523-1727</a>
                        </span>
                    </p>
                    <p>
                        <span class="ic"><i class="fa fa-envelope"></i></span>
                        <span class="val">
                            <a href="mailto:acomercial@gws.com.sv">acomercial@gws.com.sv</a>
                        </span>
                    </p>
                    <p>
                        <span class="ic"><i class="fa fa-map-marker"></i></span>
                        <span class="val">
                            89 Av. Norte y 7 Calle Poniente, No. 515 Colonia Escalón. San Salvador, El Salvador
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
