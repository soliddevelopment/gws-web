<header id="header">
    <div class="m">
        <a href="{{ route('publico.home.home') }}" class="heywow-once logo">
            <img src="{{ asset('p/img/logo.png') }}" alt="Global Wines &amp; Spirits">
        </a>
        <div class="desktop-nav">
            @include('publico.partials._nav')
        </div>
    </div>
</header>

<div id="sidemenu_btn" class="heywow-once">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
</div>
<div id="sidemenu_overlay"></div>
<div id="sidemenu">
    <div id="sidemenu_panel">
        <div class="sidemenu-nav">
            @include('publico.partials._nav')
        </div>
    </div>
</div>
