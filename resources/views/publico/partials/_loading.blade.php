<div id="loading">
    <div class="cont">
        <div class="va">
            <div class="loader">
                <img src="{{ asset('p/img/logo-peq.png') }}" alt="Global Wines and Spirits" class="logo">
                
                {{-- <img src="{{ asset('p/img/ajax-loader.gif') }}" alt="Loading..." class="gif"> --}}
                {{-- <img src="{{ asset('p/img/loader.svg') }}" alt="Loading..." class="gif"> --}}
                <div class="sk-wave">
                    <div class="sk-rect sk-rect1"></div>
                    <div class="sk-rect sk-rect2"></div>
                    <div class="sk-rect sk-rect3"></div>
                    <div class="sk-rect sk-rect4"></div>
                    <div class="sk-rect sk-rect5"></div>
                </div>

                {{--  <p class="bajada">Cargando...</p>  --}}
            </div>
        </div>
    </div>
</div>
