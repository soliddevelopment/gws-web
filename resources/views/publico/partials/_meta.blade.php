<meta property="og:title" content="@yield('title', 'Global Wines & Spirits')">
<meta property="og:description" content="@yield('description', 'Global Wines & Spirits nace en mayo de 2008, con el propósito de importar y distribuir marcas únicas de alta calidad para gustos exclusivos.')">
<meta property="og:url" content="https://www.gws.com.sv">
<meta property="fb:app_id" content="" />
<meta property="og:type" content="website" />

<meta property="og:image" content="@yield('share_img', asset('/p/img/sharing.jpg'))">
<meta property="og:image:secure_url" content="@yield('share_img', asset('/p/img/sharing.jpg'))" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="630" />
<meta property="og:image:alt" content="Global Wines & Spirits" />
