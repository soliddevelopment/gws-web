<ul>
    <li class="{{ $route_name == 'publico.home.home' ? 'active':'' }}">
        <a href="{{ route('publico.home.home') }}" class="pri ajax-link" title="Go to homepage" data-page-title="Global Wines and Spirits">
            home
        </a>
    </li>
    <li class="{{ $route_name == 'publico.home.nosotros' ? 'active':'' }}">
        <a href="{{ route('publico.home.nosotros') }}" class="pri ajax-link" title="Ir a nosotros" data-page-title="Global Wines and Spirits | Nosotros">
            nosotros
        </a>
    </li>
    <li class="{{ $route_name == 'publico.productos.index' ? 'active':'' }}">
        <a href="{{ route('publico.productos.index') }}" class="pri" title="Ir a productos" data-page-title="Global Wines and Spirits | Productos">
            productos
        </a>
    </li>
    <li class="{{ $route_name == 'publico.home.contacto' ? 'active':'' }}">
        <a href="{{ route('publico.home.contacto') }}" class="pri ajax-link" title="Ir a contacto" data-page-title="Global Wines and Spirits | Contacto">
            contacto
        </a>
    </li>
    {{--<li class="redes-li">
        @include('publico.partials._redes')
    </li>--}}
</ul>
