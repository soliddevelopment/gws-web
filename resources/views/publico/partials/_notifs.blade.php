@if(Session::has('success'))

	<script>
		$(document).ready(function(){
			Solid.AbrirModal(null, '{{ Session::get('success') }}', "¡Gracias!");

			setTimeout(function(){
				$('.alerta').fadeOut(244, function(){
					$('.alerta').remove();
				});
			}, 4000);
		});
	</script>

@endif

@if(count($errors) > 0)
	
	<script>
		$(document).ready(function(){
			var strErrores = '';
			@if(count($errors->all())>0)
				@foreach($errors->all() as $error)
					strErrores+= '{{ $error }} <br>';
				@endforeach
			@endif
			
			Solid.AbrirModal(null, strErrores, "¡Error!");
		});
	</script>
	{{--  <div class="mensaje mensaje-error" style="display:none;">
		<p>
			<strong>Errors:</strong>
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</p>
	</div>  --}}

@endif
