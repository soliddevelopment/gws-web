<a href="{{ route('publico.work.detalle', [ 'slug' => $item->slug ]) }}" class="elem heywow ajax-link" title="Go to project detail" data-page-title="SOLID | {{ $item->titulo }}">
    <span class="img" style="{{ bgImgStyle($item->img_thumb, 600, 400) }}">
        <span class="imgoverlay"></span>
    </span>
    <span class="info">
        <span class="va">
            <span class="titulo">{{ $item->titulo }}</span>
            <span class="bajada">{{ $item->bajada }}</span>
        </span>
    </span>
    {{-- <img src="{{ asset('storage/'.$item->img_logo) }}" alt="{{ $item->cliente }}" class="logo"> --}}
</a>