@extends('publico.main')
@section('title', $proyecto->titulo)
@section('description', $proyecto->descripcion)
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/logo.png') }}'
    ];
</script>

<section>
    <div id="page_detalle_proyecto" class="page">
        <section>
            <div id="intro_proyecto" style="{{ bgImgStyle($proyecto->img_intro) }}"></div>
            <div class="detalle">
                <div class="m">
                    <div class="info">
                        <div class="ops wow fadeInUp">
                            <a href="{{ route('publico.work.index') }}" class="regresar ajax-link afx" data-page-title="SOLID | Work" title="Go to work"><i class="fa fa-chevron-left"></i> Back to work</a>
                        </div>
                        <h1 class="tit1 wow fadeInUp">{{ $proyecto->titulo }}</h1>
                        <p class="bajada wow fadeInUp">{{ $proyecto->bajada }}</p>
                        <p class="descripcion wow fadeInUp">{{ $proyecto->descripcion }}</p>
                        @if(count($proyecto->servicios) > 0)
                            <div class="servicios">
                                <ul>
                                    @foreach($proyecto->servicios as $item)
                                        <li class="wow fadeInUp">{{ $item->titulo }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="links wow fadeInUp" data-wow-delay="0.4s">
                        @foreach($proyecto->links as $item)
                            <a href="{{$item->url}}" target="_blank" class="elem btn linea negro peq {{$item->tipo}}" title="{{$item->titulo}}">
                                {{$item->titulo}}
                                @if($item->tipo == 'IOS')
                                    <img class="store btn-icon" src="{{asset('p/img/appstore-negro.png')}}" alt="Appstore" />
                                @elseif($item->tipo == 'ANDROID')
                                    <img class="store btn-icon" src="{{asset('p/img/playstore-negro.png')}}" alt="Play store" />
                                @endif
                            </a>
                        @endforeach
                    </div>
                    <div class="imgs">
                        <div class="cont">
                            @foreach($imgs as $item)
                                <div class="img">
                                    <img class="wow fadeInUp" src="{{ asset('storage/'.$item->img) }}" alt="{{ $proyecto->titulo }}">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </section>
    </div>
</section>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection