@extends('publico.main')
@section('title', 'Our work')
@section('description', 'We specialize in web and app development. Take a look at our portfolio.')
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/logo.png') }}'
    ];
</script>

<section>
    <div id="page_work" class="page">
        {{-- <a href="{{ route('publico.software.home') }}" class="sitio software"> --}}
        <section id="work" class="sec">
            <div class="m">
                <div class="lista-work">
                    @foreach($proyectos as $item)
                        @include('publico.partials._workElem')
                    @endforeach
                </div>
                <div class="btn-cont">
                    <a href="{{ route('publico.home.contact') }}" title="Go to contact page" class="btn negro ajax-link" data-page-title="SOLID | Contact us">Contact us</a>
                </div>
            </div>
        </section>
        <hr>
    </div>
</section>


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection