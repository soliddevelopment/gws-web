@extends('publico.main')
@section('title', $producto->titulo)
@section('description', '')
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/bgs/4.jpg') }}',
        '{{ asset('storage/'.$producto->img) }}'
    ];
</script>

<div id="page_detalle" class="page">
    <section id="intro" class="intro-detalle heywow" style="background-image: url('/p/img/bgs/4.jpg');">
        <div class="m">
            <div class="info">
                <div class="regresar-cont heywow">
                    <a href="{{route('publico.productos.index')}}" class="ajax-link" data-page-title="Global Wines and Spirits | Nuestros productos">
                        <i class="fa fa-arrow-left"></i> Regresar a productos
                    </a>
                </div>
                <h1 class="tit1 heywow">{{ $producto->titulo }}</h1>
                <p class="bajada heywow">
                    {{$producto->bajada}}
                </p>
                @if($producto->subida !== null && strlen($producto->subida)>0)
                    <p class="variedades heywow">
                        {{$producto->subida}}
                    </p>
                @endif
                @if(strlen($producto->titulo_corto)>0)
                    <p class="presentacion heywow">
                        {{$producto->titulo_corto}}
                    </p>
                @endif
            </div>
            <img class="img-prod heywow" src="{{ asset('storage/'.$producto->img) }}" alt="{{ $producto->titulo }}">
        </div>
    </section>
    <div class="barra-gris bg-gris">
        <img class="img-mobile" src="{{ asset('storage/'.$producto->img) }}" alt="{{ $producto->titulo }}">
    </div>
    <aside id="otros" class="sec">
        <p class="txt1 cafe">Otros productos:</p>
        <div class="otros-slider fle-cafe">
            @foreach($otros as $item)
                <div class="slide">
                    <a class="ajax-link" href="{{route('publico.productos.detalle', ['slug'=>$item->slug])}}">
                        <img src="{{asset('storage/'.$item->img)}}" alt="{{$item->titulo}}">
                    </a>
                </div>
            @endforeach
            @foreach($otros as $item)
                <div class="slide">
                    <a class="ajax-link" href="{{route('publico.productos.detalle', ['slug'=>$item->slug])}}">
                        <img src="{{asset('storage/'.$item->img)}}" alt="{{$item->titulo}}">
                    </a>
                </div>
            @endforeach
        </div>
    </aside>
</div>


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection