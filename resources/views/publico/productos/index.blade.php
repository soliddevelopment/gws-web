@extends('publico.main')
@section('title', 'Nuestros productos')
@section('description', 'Global Wines & Spirits nace en mayo de 2008, con el propósito de importar y distribuir marcas únicas de alta calidad para gustos exclusivos.')
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<script>
    var imgArray = [
        '{{ asset('p/img/bgs/2.jpg') }}'
    ];
</script>

<section>
    <div id="page_productos" class="page">
        <section>
            <div id="intro" class="heywow" style="background-image: url('p/img/bgs/2.jpg');">
                <div class="m">
                    <div class="info">
                        <h1 class="tit1 heywow">Nuestros productos</h1>
                        <p class="bajada txt1 heywow">
                            Contamos con una amplia selección de licores, vinos y otras bebidas para gustos exclusivos, en los cuales se destacan marcas y productos de calidad en diferentes categorías.
                        </p>
                    </div>
                    <div class="datos-cont heywow">
                        @include('publico.partials._datosContacto')
                    </div>
                </div>
            </div>
        </section>
        <section id="filtros">
            <div class="buscar-cont">
                <input id="buscarField" type="text" class="field" placeholder="Buscar productos...">
                <i class="fa fa-search"></i>
            </div>
        </section>
        <section id="listaResultados">
            <div class="m">
                <div id="resultados">

                </div>
            </div>
        </section>
        <section id="noHay">
            <div class="m">
                <p>No se encontraron resultados</p>
            </div>
        </section>
        <section id="listaProductos">
            <div class="lista-productos">
                @foreach($categorias as $cat)
                    @if(count($cat->productos)>0)
                        <div class="cat">
                            <div class="m">
                                <h2 class="tit2">{{ $cat->titulo }}</h2>
                            </div>
                            <div class="prod-slider fle-cafe">
                                @foreach($cat->productos as $prod)
                                    <div class="slide">
                                        <a href="{{route('publico.productos.detalle', $prod->slug)}}" class="ajax-link" data-page-title="Global Wines and Spirits | {{$prod->titulo}}" title="{{ $prod->titulo }}" data-nombre="{{ $prod->titulo }}" data-categoria="{{ $cat->titulo }}">
                                            <img src="{{ asset('storage/'.$prod->img) }}" alt="{{ $prod->titulo }}">
                                            <span class="nombre">{{ $prod->titulo }}</span>
                                            <span class="categ">{{ $cat->titulo }}</span>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </section>
        
    </div>
</section>


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection