<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {

    Route::get('/', [
        'uses' => 'HomeController@index',
        'as' => 'admin.home.home',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor', 'Basic']
    ]);

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin.auth.login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('admin.auth.logout');
            
        
    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // POSTS
    
    Route::get('/posts/grupo/{grupo_id}', [
        'uses' => 'PostsController@index',
        'as' => 'admin.posts.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/posts/edit/{grupo_id}/{id}', [
        'uses' => 'PostsController@edit',
        'as' => 'admin.posts.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/posts/create/{grupo_id}', [
        'uses' => 'PostsController@edit',
        'as' => 'admin.posts.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/posts/store', [
        'uses' => 'PostsController@store',
        'as' => 'admin.posts.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::put('/posts/{id}/update', [
        'uses' => 'PostsController@store',
        'as' => 'admin.posts.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/posts/{id}/destroy', [
        'uses' => 'PostsController@destroy',
        'as' => 'admin.posts.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/posts/{id}/st', [
        'uses' => 'PostsController@st',
        'as' => 'admin.posts.st',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    
    Route::post('/contenidos/grupo/{grupo_id}/sort', [
        'uses' => 'PostsController@sort',
        'as' => 'admin.posts.sort',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // USUARIOS

    Route::get('/usuarios', [
        'uses' => 'UsuariosController@index',
        'as' => 'admin.usuarios.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor', 'Basic']
    ]);
    Route::get('/usuarios/{id}/edit', [
        'uses' => 'UsuariosController@edit',
        'as' => 'admin.usuarios.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor', 'Basic']
    ]);
    Route::get('/usuarios/create', [
        'uses' => 'UsuariosController@edit',
        'as' => 'admin.usuarios.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin']
    ]);
    Route::post('/usuarios/store', [
        // 'uses' => 'UsuariosController@store',
        'uses' => 'UsuariosController@store',
        //'as' => 'admin.usuarios.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin']
    ]);
    Route::put('/usuarios/{id}/update', [
        'uses' => 'UsuariosController@store',
        'as' => 'admin.usuarios.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor', 'Basic']
    ]);
    Route::put('/usuarios/{id}/updatepassword', [
        'uses' => 'UsuariosController@updatePassword',
        'as' => 'admin.usuarios.updatepassword',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor', 'Basic']
    ]);
    Route::delete('/usuarios/{id}/destroy', [
        'uses' => 'UsuariosController@destroy',
        'as' => 'admin.usuarios.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin']
    ]);
    Route::post('/usuarios/{id}/st', [
        'uses' => 'UsuariosController@st',
        'as' => 'admin.usuarios.st',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // GRUPOS

    Route::get('/grupos', [
        'uses' => 'GruposController@index',
        'as' => 'admin.grupos.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::get('/grupos/{id}/edit', [
        'uses' => 'GruposController@edit',
        'as' => 'admin.grupos.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::get('/grupos/create', [
        'uses' => 'GruposController@edit',
        'as' => 'admin.grupos.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::post('/grupos/store', [
        'uses' => 'GruposController@store',
        'as' => 'admin.grupos.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::put('/grupos/{id}/update', [
        'uses' => 'GruposController@store',
        'as' => 'admin.grupos.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::delete('/grupos/{id}/destroy', [
        'uses' => 'GruposController@destroy',
        'as' => 'admin.grupos.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::post('/grupos/{id}/st', [
        'uses' => 'GruposController@st',
        'as' => 'admin.grupos.st',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // EXCEPCIONES DE GRUPOS
    
    Route::get('/grupos/{id}/mappings', [
        'uses' => 'MappingsController@index',
        'as' => 'admin.grupos.mappings.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::get('/grupos/{grupo_id}/mappings/{id}/edit', [
        'uses' => 'MappingsController@edit',
        'as' => 'admin.grupos.mappings.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::get('/grupos/{grupo_id}/mappings/edit', [
        'uses' => 'MappingsController@edit',
        'as' => 'admin.grupos.mappings.edit2',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::get('/grupos/{grupo_id}/mappings/create', [
        'uses' => 'MappingsController@edit',
        'as' => 'admin.grupos.mappings.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::post('/grupos/{id}/mappings/store', [
        'uses' => 'MappingsController@store',
        'as' => 'admin.grupos.mappings.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::put('/grupos/{grupo_id}/mappings/{id}/update', [
        'uses' => 'MappingsController@store',
        'as' => 'admin.grupos.mappings.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::delete('/grupos/{id}/mappings/destroy', [
        'uses' => 'MappingsController@destroy',
        'as' => 'admin.grupos.mappings.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    
    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // IMGS
    
    Route::get('/imgs/post/{postId}/index', [
        'uses' => 'ImgsController@postIndex',
        'as' => 'admin.imgs.post',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/imgs/producto/{productoId}/index', [
        'uses' => 'ImgsController@productoIndex',
        'as' => 'admin.imgs.producto',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    Route::post('/imgs/post/{postId}/postUpload', [
        'uses' => 'ImgsController@postUpload',
        'as' => 'admin.imgs.postupload',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/imgs/post/{postId}/productoUpload', [
        'uses' => 'ImgsController@productoUpload',
        'as' => 'admin.imgs.productoupload',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    Route::post('/imgs/destroy', [
        'uses' => 'ImgsController@destroy',
        'as' => 'admin.imgs.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/imgs/sort', [
        'uses' => 'ImgsController@sort',
        'as' => 'admin.imgs.sort',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // PRODUCTOS
    
    Route::get('/productos/grupo/{grupo}', [
        'uses' => 'ProductosController@index',
        'as' => 'admin.productos.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/productos/{id}/edit', [
        'uses' => 'ProductosController@edit',
        'as' => 'admin.productos.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/productos/create/grupo/{grupo}', [
        'uses' => 'ProductosController@create',
        'as' => 'admin.productos.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/productos/store', [
        'uses' => 'ProductosController@store',
        'as' => 'admin.productos.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::put('/productos/{id}/update', [
        'uses' => 'ProductosController@update',
        'as' => 'admin.productos.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/productos/{id}/destroy', [
        'uses' => 'ProductosController@destroy',
        'as' => 'admin.productos.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/productos/{id}/st', [
        'uses' => 'ProductosController@st',
        'as' => 'admin.productos.st',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    
    

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // CATEGORIAS

    Route::get('/categorias/grupo/{grupo}', [
        'uses' => 'CategoriasController@index',
        'as' => 'admin.categorias.index',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/categorias/{id}/edit', [
        'uses' => 'CategoriasController@edit',
        'as' => 'admin.categorias.edit',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/categorias/create/grupo/{grupo}', [
        'uses' => 'CategoriasController@create',
        'as' => 'admin.categorias.create',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/categorias/store', [
        'uses' => 'CategoriasController@store',
        'as' => 'admin.categorias.store',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::put('/categorias/{id}/update', [
        'uses' => 'CategoriasController@update',
        'as' => 'admin.categorias.update',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/categorias/{id}/destroy', [
        'uses' => 'CategoriasController@destroy',
        'as' => 'admin.categorias.destroy',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/categorias/{id}/st', [
        'uses' => 'CategoriasController@st',
        'as' => 'admin.categorias.st',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // SUBCATEGORIAS

    Route::get('/subcategorias/categoria/{categoria}', [
        'uses' => 'SubcategoriasController@index',
        'as' => 'admin.subcategorias.index',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/subcategorias/{id}/edit', [
        'uses' => 'SubcategoriasController@edit',
        'as' => 'admin.subcategorias.edit',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/subcategorias/create/categoria/{categoria}', [
        'uses' => 'SubcategoriasController@create',
        'as' => 'admin.subcategorias.create',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/subcategorias/store', [
        'uses' => 'SubcategoriasController@store',
        'as' => 'admin.subcategorias.store',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::put('/subcategorias/{id}/update', [
        'uses' => 'SubcategoriasController@update',
        'as' => 'admin.subcategorias.update',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/subcategorias/{id}/destroy', [
        'uses' => 'SubcategoriasController@destroy',
        'as' => 'admin.subcategorias.destroy',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/subcategorias/{id}/st', [
        'uses' => 'SubcategoriasController@st',
        'as' => 'admin.subcategorias.st',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // CONTACTO

    Route::get('/contactos', [
        'uses' => 'ContactosController@index',
        'as' => 'admin.contactos.index',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    
    Route::get('/contactos/{id}', [
        'uses' => 'ContactosController@detalle',
        'as' => 'admin.contactos.detalle',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/contactos/{id}/destroy', [
        'uses' => 'ContactosController@destroy',
        'as' => 'admin.contactos.destroy',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // ERROR LOG VIEWER
        
    Route::get('/logs', [
        'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
        'as' => 'admin.logs',
        'middleware' => 'admin_roles', 'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // PROYECTOS
    
    Route::get('/proyectos', [
        'uses' => 'ProyectosController@index',
        'as' => 'admin.proyectos.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/proyectos/edit/{id}', [
        'uses' => 'ProyectosController@edit',
        'as' => 'admin.proyectos.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/proyectos/create', [
        'uses' => 'ProyectosController@edit',
        'as' => 'admin.proyectos.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/proyectos/store', [
        'uses' => 'ProyectosController@store',
        'as' => 'admin.proyectos.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::put('/proyectos/{id}/update', [
        'uses' => 'ProyectosController@store',
        'as' => 'admin.proyectos.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/proyectos/{id}/destroy', [
        'uses' => 'ProyectosController@destroy',
        'as' => 'admin.proyectos.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    // Route::post('/proyectos/{id}/st', [
    //     'uses' => 'ProyectosController@st',
    //     'as' => 'admin.proyectos.st',
    //     'middleware' => 'admin_roles',
    //     'roles' => ['Global', 'Admin', 'Supervisor']
    // ]);
    Route::post('/proyectos/{id}/destacado', [
        'uses' => 'ProyectosController@destacado',
        'as' => 'admin.proyectos.destacado',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/proyectos/sort', [
        'uses' => 'ProyectosController@sort',
        'as' => 'admin.proyectos.sort',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // IMGS
    
    Route::get('/imgs/proyecto/{proyectoId}', [
        'uses' => 'ProyectoImgsController@index',
        'as' => 'admin.proyectos.imgs',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/imgs/proyecto/{postId}/upload', [
        'uses' => 'ProyectoImgsController@upload',
        'as' => 'admin.proyectos.imgs.upload',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/imgs/proyectos/destroy', [
        'uses' => 'ProyectoImgsController@destroy',
        'as' => 'admin.proyectos.imgs.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/imgs/proyecto/sort', [
        'uses' => 'ProyectoImgsController@sort',
        'as' => 'admin.proyectos.imgs.sort',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // SERVICIOS
    
    Route::get('/servicios', [
        'uses' => 'ServiciosController@index',
        'as' => 'admin.servicios.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/servicios/edit/{id}', [
        'uses' => 'ServiciosController@edit',
        'as' => 'admin.servicios.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/servicios/create', [
        'uses' => 'ServiciosController@edit',
        'as' => 'admin.servicios.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/servicios/store', [
        'uses' => 'ServiciosController@store',
        'as' => 'admin.servicios.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::put('/servicios/{id}/update', [
        'uses' => 'ServiciosController@store',
        'as' => 'admin.servicios.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/servicios/{id}/destroy', [
        'uses' => 'ServiciosController@destroy',
        'as' => 'admin.servicios.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/servicios/{id}/st', [
        'uses' => 'ServiciosController@st',
        'as' => 'admin.servicios.st',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/servicios/{id}/destacado', [
        'uses' => 'ServiciosController@destacado',
        'as' => 'admin.servicios.destacado',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // LINKS PROYECTOS

    Route::get('/proyectos/{id}/links', [
        'uses' => 'LinksController@index',
        'as' => 'admin.proyectos.links.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/proyectos/links/edit/{linkId}', [
        'uses' => 'LinksController@edit',
        'as' => 'admin.proyectos.links.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/proyectos/{proyectoId}/links/create', [
        'uses' => 'LinksController@create',
        'as' => 'admin.proyectos.links.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/proyectos/links/store', [
        'uses' => 'LinksController@store',
        'as' => 'admin.proyectos.links.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::put('/proyectos/links/{linkId}/update', [
        'uses' => 'LinksController@store',
        'as' => 'admin.proyectos.links.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/proyectos/links/{id}/destroy', [
        'uses' => 'LinksController@destroy',
        'as' => 'admin.proyectos.links.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/proyectos/links/{id}/sort', [
        'uses' => 'LinksController@sort',
        'as' => 'admin.proyectos.links.sort',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    
    // **********************************************************************************************************************************
    // **********************************************************************************************************************************
    // CONFIG
    
    Route::get('/config', [
        'uses' => 'ConfigController@index',
        'as' => 'admin.config.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin']
    ]);
    Route::get('/config/edit/{id}', [
        'uses' => 'ConfigController@edit',
        'as' => 'admin.config.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin']
    ]);
    Route::get('/config/create', [
        'uses' => 'ConfigController@edit',
        'as' => 'admin.config.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::post('/config/store', [
        'uses' => 'ConfigController@store',
        'as' => 'admin.config.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::put('/config/{id}/update', [
        'uses' => 'ConfigController@store',
        'as' => 'admin.config.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin']
    ]);
    Route::delete('/config/{id}/destroy', [
        'uses' => 'ConfigController@destroy',
        'as' => 'admin.config.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global']
    ]);
    Route::post('/config/{id}/st', [
        'uses' => 'ConfigController@st',
        'as' => 'admin.config.st',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin']
    ]);

    


});


