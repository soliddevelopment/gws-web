<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {

    Route::get('/locations', [
        'uses' => 'LocationsController@index',
        'as' => 'admin.locations.index',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    
    Route::get('/locations/edit/{id}', [
        'uses' => 'LocationsController@edit',
        'as' => 'admin.locations.edit',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::get('/locations/create', [
        'uses' => 'LocationsController@edit',
        'as' => 'admin.locations.create',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/locations/store', [
        'uses' => 'LocationsController@store',
        'as' => 'admin.locations.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::put('/locations/{id}/update', [
        'uses' => 'LocationsController@store',
        'as' => 'admin.locations.update',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::delete('/locations/{id}/destroy', [
        'uses' => 'LocationsController@destroy',
        'as' => 'admin.locations.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/locations/{id}/st', [
        'uses' => 'LocationsController@st',
        'as' => 'admin.locations.st',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

    Route::get('/locations/script', [
        'uses' => 'LocationsController@scriptsEdit',
        'as' => 'admin.locations.script',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);
    Route::post('/locations/script/store', [
        'uses' => 'LocationsController@scriptsStore',
        'as' => 'admin.locations.script.store',
        'middleware' => 'admin_roles',
        'roles' => ['Global', 'Admin', 'Supervisor']
    ]);

});


