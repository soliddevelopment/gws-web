<?php
// RUTAS DEL SITIO PUBLICO

// RUTAS DE LOGIN
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('publico.auth.login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('publico.auth.register');
Route::post('/register', 'Auth\RegisterController@register');
Route::post('/logout', 'Auth\LoginController@logout')->name('publico.auth.logout');

// RUTAS COMUNES
Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'publico.home.home'
]);

// RUTAS CUSTOM
Route::get('/nosotros', [
    'uses' => 'HomeController@nosotros',
    'as' => 'publico.home.nosotros'
]);

Route::get('/contacto', [
    'uses' => 'HomeController@contacto',
    'as' => 'publico.home.contacto'
]);
Route::post('/contacto/store', [
    'uses' => 'HomeController@contactoPost',
    'as' => 'publico.home.contactoPost'
]);

/* 
Route::get('/blog', [
    'uses' => 'BlogController@index',
    'as' => 'publico.blog.index'
]);
Route::get('/blog/{slug}', [
    'uses' => 'BlogController@detalle',
    'as' => 'publico.blog.detalle'
]); */

Route::get('/productos', [
    'uses' => 'ProductosController@index',
    'as' => 'publico.productos.index'
]);

Route::get('/productos/{slug}', [
    'uses' => 'ProductosController@detalle',
    'as' => 'publico.productos.detalle'
]);


// EJEMPLOS::::::::::::::::::

// RUTAS DE SITIO SOFTWARE
/* Route::prefix('software')->group(function () {

    Route::get('/', [
        'uses' => 'Software\HomeController@index',
        'as' => 'publico.software.home'
    ]);
    Route::get('/servicios', [
        'uses' => 'Software\HomeController@servicios',
        'as' => 'publico.software.servicios'
    ]);
    Route::get('/equipo', [
        'uses' => 'Software\HomeController@equipo',
        'as' => 'publico.software.equipo'
    ]);

});
 */