let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


/*************************************************************************************
***************************************************************************************
***************************************************************************************
ADMIN
*/

// Estilos
mix.styles([
	'resources/assets/admin/css/lib/icons.css',
	'resources/assets/admin/css/lib/switchery.min.css',
	'resources/assets/admin/css/lib/sweet-alert.css',
	'resources/assets/admin/css/lib/jquery-ui.css',
	'resources/assets/admin/css/lib/dropzone.css',
	'resources/assets/admin/css/lib/summernote.css',
	'resources/assets/admin/css/lib/summernote-bs3.css',
	'resources/assets/admin/css/lib/jquery.fancybox.css',
	'resources/assets/admin/css/lib/bootstrap-timepicker.min.css',
	'resources/assets/admin/css/lib/bootstrap.min.css',
	'resources/assets/admin/css/lib/croppie.css',
	'resources/assets/admin/css/lib/responsive.css'
], 'public/a/css/lib/all.css');

mix.styles([
    'resources/assets/admin/css/pages/core.css',
    'resources/assets/admin/css/pages/components.css',
    'resources/assets/admin/css/pages/pages.css',
    'resources/assets/admin/css/pages/menu.css',
    'resources/assets/admin/css/pages/responsive.css'
], 'public/a/css/pages/all.css');

mix.styles([
    'resources/assets/admin/sass/ajustes.css',
    'resources/assets/admin/sass/custom.css',
    'resources/assets/admin/sass/login.css',
], 'public/a/css/custom/custom-admin.css');

// mix.sass('resources/assets/admin/sass/ajustes.scss', 'public/a/css/custom/ajustes.css');
// mix.sass('resources/assets/admin/sass/custom.scss', 'public/a/css/custom/custom.css');
// mix.sass('resources/assets/admin/sass/login.scss', 'public/a/css/custom/login.css');

// Scripts
mix.scripts([
	'resources/assets/admin/js/lib/modernizr.min.js',
	'resources/assets/admin/js/lib/jquery.min.js'
], 'public/a/js/header.js');

mix.scripts([
    'resources/assets/admin/js/lib/jquery.dataTables.js',
    'resources/assets/admin/js/lib/dataTables.bootstrap.js',
    'resources/assets/admin/js/lib/dataTables.responsive.js',
    'resources/assets/admin/js/lib/dataTables.tableTools.min.js'
],'public/a/js/datatables.js');

mix.scripts([
	'resources/assets/admin/js/lib/jquery-ui.min.js',
    'resources/assets/admin/js/lib/switchery.min.js',
    'resources/assets/admin/js/lib/skycons.min.js',
    'resources/assets/admin/js/lib/notify.min.js',
    'resources/assets/admin/js/lib/notify-metro.js',
    'resources/assets/admin/js/lib/sweet-alert.js',
    'resources/assets/admin/js/lib/bootstrap-timepicker.min.js',
    'resources/assets/admin/js/lib/bootstrap.min.js',
    'resources/assets/admin/js/lib/detect.js',
    'resources/assets/admin/js/lib/fastclick.js',
    'resources/assets/admin/js/lib/jquery.slimscroll.js',
    'resources/assets/admin/js/lib/jquery.blockUI.js',
    'resources/assets/admin/js/lib/waves.js',
    'resources/assets/admin/js/lib/wow.min.js',
    'resources/assets/admin/js/lib/jquery.nicescroll.js',
    'resources/assets/admin/js/lib/jquery.scrollTo.min.js',
    'resources/assets/admin/js/lib/jquery.fancybox.js',
    'resources/assets/admin/js/lib/toastr.min.js',
    'resources/assets/admin/js/lib/bootstrap-datepicker.js',
    'resources/assets/admin/js/lib/jquery-ui.min.js',
    //'resources/assets/admin/js/lib/croppie.js',
    //'resources/assets/admin/js/lib/croppie.init.js',
    'resources/assets/admin/js/lib/summernote.min.js',
    'resources/assets/admin/js/lib/dropzone.js',
    'resources/assets/admin/js/lib/jquery.validate.js',
    //'resources/assets/admin/js/lib/jquery.validate.unobtrusive.js',
    // 'resources/assets/admin/js/lib/exif.js', // lo pase pa fuera

], 'public/a/js/footer.js');

mix.scripts([
    'resources/assets/admin/js/pages/jquery.core.js',
    'resources/assets/admin/js/pages/jquery.app.js',
    'resources/assets/admin/js/custom/ajustes.js',
    'resources/assets/admin/js/custom/custom.js'
],'public/a/js/custom.js');

mix.js('resources/assets/admin/js/view/grupos-mappings-posts.js', 'public/a/js/view/grupos-mappings-posts.js');

// mix.sass('resources/assets/sass/app.scss', 'public/css');


/*************************************************************************************
***************************************************************************************
***************************************************************************************
PUBLIC
*/

// incluir font-awesome y fonts directamente alla, ya los meti en el public
/*mix.styles([
    'resources/assets/public/css/lib/normalize.css',
    'resources/assets/public/css/lib/reset.css',
    'resources/assets/public/css/lib/animate.css',
    'resources/assets/public/css/lib/bootstrap.css',
    // 'resources/assets/public/css/lib/jquery.fancybox.css',
    'resources/assets/public/css/lib/slick.css',
    'resources/assets/public/css/lib/slick-theme.css',
    'resources/assets/public/css/lib/hover-min.css',
    'resources/assets/public/css/lib/animated-menu-icon.css',
    'resources/assets/public/css/lib/jquery.simplyscroll.css',
    'resources/assets/public/css/lib/jquery-ui.css',
    'resources/assets/public/css/lib/jquery-ui.theme.css',
], 'public/p/css/lib/lib.css');


mix.styles([
    'resources/assets/public/css/sass/fonts.css',
    'resources/assets/public/css/sass/snippets.css',
    'resources/assets/public/css/sass/styles.css',
    'resources/assets/public/css/sass/media-queries.css',
], 'public/p/css/custom.css');*/

mix.sass('resources/assets/public/css/sass/app.scss', 'public/p/css/app.css').version();

// Scripts
mix.scripts([
	'resources/assets/public/js/lib/modernizr-2.8.3.min.js',
	'resources/assets/public/js/lib/jquery.js',
	'resources/assets/public/js/lib/jquery-ui.js',
	'resources/assets/public/js/lib/jquery.ui.touch-punch.min.js',
	'resources/assets/public/js/lib/datepicker-es.js'
], 'public/p/js/header.js');

mix.scripts([
    // estos dos de history que no vayan minificados
	// 'resources/assets/public/js/lib/history.js',
    // 'resources/assets/public/js/lib/history.adapter.js',

	'resources/assets/public/js/lib/selectivizr-min.js',
	'resources/assets/public/js/lib/jquery.easing.1.3.js',
	'resources/assets/public/js/lib/jquery.validate.min.js',
	'resources/assets/public/js/lib/jquery.validate.custom.js',
	// 'resources/assets/public/js/lib/parallax.js',
	'resources/assets/public/js/lib/PxLoader.js',
	'resources/assets/public/js/lib/PxLoaderImage.js',
	'resources/assets/public/js/lib/hey.ajaxnav.js',
	'resources/assets/public/js/lib/slick.js',
	'resources/assets/public/js/lib/jquery.fancybox.js',
	'resources/assets/public/js/lib/wow.min.js',
	'resources/assets/public/js/lib/isInViewport.min.js',
	'resources/assets/public/js/lib/jquery.simplyscroll.min.js',

    'resources/assets/public/js/custom/utils.js',
    //'resources/assets/public/js/custom/snippets.js',
    'resources/assets/public/js/custom/general.js'

], 'public/p/js/footer.js');
